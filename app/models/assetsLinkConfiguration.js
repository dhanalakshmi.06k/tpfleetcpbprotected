/**
 * Created by Suhas on 8/18/2016.
 */

var mongoose = require('mongoose'),
        Schema = mongoose.Schema;
var assetsLinkConfigurationSchema = new Schema({
        'assetType':String,
        "assetsTypeLinkable":Array
},{collection: "assetsLinkConfiguration"})
module.exports = mongoose.model('assetsLinkConfigurationModel',assetsLinkConfigurationSchema);