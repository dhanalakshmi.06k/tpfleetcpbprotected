/**
 * Created by Suhas on 6/18/2016.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var sensorSchema = new mongoose.Schema({
                                                sensorData:{},
                                                created_at:{ type: Date},
                                                updated_at:{ type: Date}
                                        },{
                                        collection:"sensors"
                                        });
sensorSchema.pre('save', function(next){
        var now = new Date();
        this.updated_at = now;
        if ( !this.created_at ) {
                this.created_at = now;
        }
        next();
});
sensorSchema.post('save', function(doc) {
        console.log('%s asset of type %s has been saved', doc.sensorData.assetName,doc.sensorData.sensorType);
});
module.exports = mongoose.model('sensors',sensorSchema);




