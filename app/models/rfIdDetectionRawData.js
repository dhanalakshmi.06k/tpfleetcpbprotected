/**
 * Created by Suhas on 9/6/2016.
 */
var mongoose = require('mongoose'),
        Schema = mongoose.Schema;
var sensorModel = require('./sensorModel.js');
var rfIdDetectedRawDataSchema = new Schema({
        rawData:String,
        created_at:{ type: Date},
        updated_at:{ type: Date}
},{collection: "rfIdDetectedRawDataDoc"})

module.exports = mongoose.model('rfIdDetectedRawDataModel',rfIdDetectedRawDataSchema);

rfIdDetectedRawDataSchema.pre('save', function(next){
        console.log("Rf Id Detection Data Saved")
        var now = Date.now();
        this.updated_at = now;
        if ( !this.created_at ) {
                this.created_at = now;
        }
        next();
});
rfIdDetectedRawDataSchema.post('save', function(doc){
        console.log(doc)
});
