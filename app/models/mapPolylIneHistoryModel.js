/**
 * Created by zendynamix on 25/8/16.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var historyDateSettingSchema = new Schema({
    "dateConfiguration":{
        "id":Number,
        "fromDate":Date,
        "fromTime":Number,
        "toDate":Date,
        "toTime":Number

    }
},{collection: "historyDateSettingModel"})
module.exports = mongoose.model('historyDateSettingModel',historyDateSettingSchema);

