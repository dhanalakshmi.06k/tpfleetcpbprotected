/**
 * Created by Suhas on 9/15/2016.
 */
var rfIdDetectionRawDataModel = require('../models/rfIdDetectionRawData');
var save = function(data){
        var rfIdDetectionRawDataObj = new rfIdDetectionRawDataModel;
        rfIdDetectionRawDataObj.rawData = data;
        rfIdDetectionRawDataObj.save(function(err,data){
                if(err){console.log(err.stack)}else{console.log("RF ID Detection Raw Data Saved")}
        })

}
module.exports={
        save:save
}