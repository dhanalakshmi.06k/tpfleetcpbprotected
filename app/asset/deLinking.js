/**
 * Created by Suhas on 8/18/2016.
 */

var mongoose = require('mongoose');
var assetDataManagement = require('./assetDataManagement');
var assetModel = mongoose.model('sensors');
var deLinkingAssets = function(assetOne,assetTwo){
        return new Promise(function(resolve,reject){
                var checker = 0;
                deLink(assetOne,assetTwo)
                .then(function(){
                        checker =  checker+1
                        resolve()
                })
                deLink(assetTwo,assetOne)
                .then(function(){
                        checker =  checker+1
                        if(checker==2){
                                resolve()
                        }
                })
        })
}
function deLink(asset,assetToBeDeLinked){
        return new Promise(function(resolve,reject){
                assetDataManagement.retrieve(asset)
                .then(function(data){
                        if(data.sensorData && data.sensorData.assetsLinked && data.sensorData.assetsLinked.length>0){
                                var assetsLinkedArr = data.sensorData.assetsLinked;
                                var assetIdObj = mongoose.Types.ObjectId(assetToBeDeLinked)
                                for(var i= 0;i<assetsLinkedArr.length;i++){
                                        var assetLinkedObj = assetsLinkedArr[i];
                                        /*console.log(typeof(assetLinkedObj.ref))*/
                                        if(typeof(assetLinkedObj.ref)==='string'){
                                                assetLinkedObj.ref=mongoose.Types.ObjectId(assetLinkedObj.ref)
                                        }
                                       /* console.log(assetLinkedObj.ref.id)
                                        console.log(assetIdObj.id)*/
                                        if(assetLinkedObj.ref.id===assetIdObj.id){
                                                data.sensorData.assetsLinked.splice(i,1);
                                                if(data.sensorData.assetsLinked.length == 0){
                                                        data.sensorData.isLinked = false;
                                                }
                                                assetDataManagement.update(data)
                                                .then(function(data){
                                                        resolve();
                                                })
                                                break;
                                        }

                                }
                        }
                })
        })
}
/*function getAssetDetails(id){
        return new Promise(function(resolve,reject){
                assetModel.findOne({'_id':id},function(err,result){
                        if(err){
                                reject(err)
                        }else{
                                resolve(result)
                        }
                })
        })
}
function updateAssetDetails(assetObj){
        return new Promise(function(resolve,reject){
                assetModel.findOneAndUpdate({_id: assetObj._id},assetObj, function (err, response){
                        if(err){reject(err)}else{resolve(response)}
                })
        })
}*/
module.exports = {
        deLinkingAssets:deLinkingAssets
}