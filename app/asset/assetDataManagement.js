/**
 * Created by Suhas on 8/23/2016.
 */
var mongoose = require('mongoose');
var assetModel = mongoose.model('sensors');
var sensorsConfiguration =  mongoose.model('sensorsConfiguration');
var assetConfigManagement  = require('./assetConfigurationManagement');
var notificationSaveAndPush  = require('../dataManagement').notification;
var odbAssetManagement=require('../obdAssetController')
var save = function(assetData){

        return new Promise(function(resolve,reject){
                try{
                        var assetModelObj = new assetModel();
                        assetModelObj.createdDate = new Date();
                        assetModelObj.sensorData = assetData;
                        assetConfigManagement.getAssetNameFieldByAssetType(assetModelObj.sensorData.sensorType)
                        .then(function(assetNameField){
                                if(assetModelObj.sensorData[assetNameField]){
                                        assetModelObj.sensorData['assetName']=assetModelObj.sensorData[assetNameField];
                                }
                                assetModelObj.save(function(err, result){
                                        if (err) {console.log(err.stack)}
                                        else {
                                                if(result.sensorData.sensorType==='obdDevice'){
                                                        console.log("*&&&&&&&&&&&saveOdbAsset&&&&&&&&&&&")
                                                    console.log(assetData)
                                                    console.log("*&&&&&&&&&&&saveOdbAsset&&&&&&&&&&&")
                                                        odbAssetManagement.obdAssets.saveOdbAsset(assetData,function(err){
                                                                resolve(result);
                                                        })
                                                 }else{
                                                        resolve(result);
                                                }

                                                var addedMsg={
                                                        "level":"INFO",
                                                        "message": "Asset Configuration "+assetModelObj.sensorData.assetName+" Created",
                                                        "timeStamp":new Date(),
                                                        "notificationType":"Asset"
                                                }
                                                notificationSaveAndPush.save(addedMsg);
                                        }
                                })
                        })
                }
                catch(e){
                        reject(e.stack)
                }
        })
}
var update = function(assetDetails){
        return new Promise(function(resolve,reject) {
                try {
                        var type = assetDetails.sensorData.sensorType
                        assetConfigManagement.getAssetNameFieldByAssetType(type)
                        .then(function (assetNameField) {
                                if (assetDetails.sensorData[assetNameField]) {
                                        assetDetails.sensorData['assetName'] = assetDetails.sensorData[assetNameField];
                                }
                                assetDetails.updated_at = new Date();
                                assetModel.findOneAndUpdate({_id: assetDetails._id}, assetDetails
                                        , function (err, response) {
                                                if (err) {
                                                        reject(err.stack)
                                                }
                                                else {
                                                        if(response.sensorData.sensorType==='obdDevice'){
                                                                odbAssetManagement.obdAssets.updateObdDevice(assetDetails,function(err){
                                                                        resolve(assetDetails)
                                                                })
                                                        }else{
                                                                resolve(assetDetails)
                                                        }
                                                        var updateMsg = {
                                                                "level": "INFO",
                                                                "message": "Asset Configuration " + assetDetails.sensorData.assetName + " Updated",
                                                                "timeStamp": new Date(),
                                                                "notificationType": "Asset"
                                                        }
                                                        notificationSaveAndPush.save(updateMsg);
                                                }
                                        })
                        })
                }
                catch (e) {
                        reject(e.stack)
                }
        })
}
var retrieve = function(id){
        return new Promise(function(resolve,reject) {
                assetModel.findOne({'_id': id}, function (err, result) {
                        if (err) {
                                reject(err)
                        } else {
                                resolve(result)
                        }
                })
        })
}
var remove = function(id){
        return new Promise(function(resolve,reject) {
                assetModel.findOne({'_id': id},function(err,response){
                if(err){
                        reject(err)
                }else{
                        assetModel.remove({'_id': id}, function (err, result) {
                                if (err) {
                                        reject(err)
                                } else {
                                        var deleteMsg={
                                                "level":"WARNING",
                                                "message": "Asset Configuration '"+response.sensorData.assetName+"'  Deleted",
                                                "timeStamp":new Date(),
                                                "notificationType":"Asset"
                                        }
                                        notificationSaveAndPush.save(deleteMsg);
                                        resolve(result)
                                }
                        })
                }
        })
        })
}

var getRelatedAssetDetailsByRfIdNumber= function(rfIdNumber){
        return new Promise(function(resolve,reject){
                assetModel.findOne({'sensorData.rfIdNo':rfIdNumber})
                        .populate('sensorData.assetsLinked.ref')
                        .exec(function(err,result){
                                if(err)
                                {
                                        reject(err)
                                }else{
                                        resolve(result)
                                }
                        })
        })

}

var getAssetsLinkedToAssetTypeProvided = function(rootAssetType,assetTypeLinked){
        return new Promise(function(resolve,reject){
                assetModel.aggregate([
                                {$match: {"sensorData.sensorType":rootAssetType}},
                                {$unwind: "$sensorData.assetsLinked"},
                                {$match: {"sensorData.assetsLinked.type":assetTypeLinked}},
                                {$project: {'carId':"$_id",'carName':"$sensorData.assetName",'_id':0}}
                        ]
                        ,function(err,data){
                                if(err){reject(err)}else{resolve(data)}
                        });
        })

}

module.exports = {
        save:save,
        update:update,
        retrieve:retrieve,
        remove:remove,
        getRelatedAssetDetailsByRfIdNumber:getRelatedAssetDetailsByRfIdNumber,
        getAssetsLinkedToAssetTypeProvided:getAssetsLinkedToAssetTypeProvided
}