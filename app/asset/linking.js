/**
 * Created by Suhas on 8/18/2016.
 */

var mongoose = require('mongoose');
var AssetModel = mongoose.model('sensors');
var assetDataManagement = require('./assetDataManagement');
var AssetLinkConfigurationModel = mongoose.model('assetsLinkConfigurationModel');
var linkingAssets = function(rootAssetToBeLinked,assetToBeLinked){
        return new Promise(function(resolve,reject){
                verifyAssetCanBeLinked(rootAssetToBeLinked.type,assetToBeLinked.type)
                .then(function(isLinkable){
                        var counter = 0;
                        if(isLinkable){
                                linkAssets(rootAssetToBeLinked,assetToBeLinked,true)
                                .then(function(){
                                        counter = counter+1;
                                        if(counter==2){
                                                resolve()
                                        }
                                })
                                linkAssets(assetToBeLinked,rootAssetToBeLinked,false)
                                .then(function(){
                                        counter = counter+1;
                                        if(counter==2){
                                                resolve()
                                        }

                                })
                        }else{
                                resolve();
                        }
                })
                .catch(function(error){
                        reject(error)
                })
        })
}
function linkAssets(assetsToBeLinkedTo,assetsToBeLinked,isRoot){
        return new Promise(function(resolve,reject){
                assetDataManagement.retrieve(assetsToBeLinkedTo.id)
                .then(function(result){
                                var assetDetails = result;
                                checkAndLinkAssets(assetDetails,assetsToBeLinked,isRoot)
                                .then(function(assetObj){
                                                resolve();
                                        })
                                .catch(function(e){
                                        reject(e)
                                })
                        })
        })
}
function checkAndLinkAssets(assetsLinkedObj,assetToBeLinked,isRoot){
        return new Promise(function(resolve,reject){
                if(assetsLinkedObj.sensorData.assetsLinked && assetsLinkedObj.sensorData.assetsLinked.length>0){
                        var assetsLinked= assetsLinkedObj.sensorData.assetsLinked;
                        var isAssetsLinked = false;
                        assetsLinked.forEach(function(linkedObjects,index){
                                var assetToBeLinkedObjectId = mongoose.Types.ObjectId(assetToBeLinked.id)
                                if(linkedObjects.ref === assetToBeLinkedObjectId){
                                        isAssetsLinked = true;
                                }
                                if(index == assetsLinked.length-1 && ! isAssetsLinked){
                                        var assetLinkObj = {
                                                type:assetToBeLinked.type,
                                                ref:assetToBeLinkedObjectId
                                        };
                                        assetsLinkedObj.sensorData.isLinked = true;
                                        assetsLinkedObj.sensorData.assetsLinked.push(assetLinkObj)
                                        assetDataManagement.update(assetsLinkedObj)
                                        .then(function(result){
                                                resolve(result);
                                        })
                                }else{
                                        resolve(assetsLinkedObj);
                                }
                        })
                }
                else{
                        assetsLinkedObj.sensorData.assetsLinked=[];
                        var assetLinkObj = {
                                type:assetToBeLinked.type,
                                ref:mongoose.Types.ObjectId(assetToBeLinked.id)
                        }
                        assetsLinkedObj.sensorData.assetsLinked.push(assetLinkObj)
                        assetsLinkedObj.sensorData.isLinked = true;
                        assetDataManagement.update(assetsLinkedObj)
                        .then(function(result){
                                resolve(result);
                        })
                }

        })
}
function verifyAssetCanBeLinked(rootAssetType,typeOfAssetToBeLinked){
        return new Promise(function(resolve,reject){
                AssetLinkConfigurationModel.findOne({'assetType':rootAssetType},function(err,result){
                        if(err){reject(err)}
                        else{
                                if(result.assetsTypeLinkable.indexOf(typeOfAssetToBeLinked)>=0){resolve(true)}
                                else{reject(false)}
                        }
                })
        })
}
module.exports = {
        linkingAssets:linkingAssets
}