/**
 * Created by Suhas on 8/18/2016.
 */

var linking= require('./linking');
var deLinking= require('./deLinking');
var assetDataManagement= require('./assetDataManagement');
var assetConfigurationManagement= require('./assetConfigurationManagement');
var rfIdDetectedAssetManagement= require('./rfIdDetectedAssetManagement');
var rfIdDetectedRawDataManagement= require('./rfIdDetectionRawData');
module.exports={
        linking:linking,
        deLinking:deLinking,
        assetDataManagement:assetDataManagement,
        assetConfigurationManagement:assetConfigurationManagement,
        rfIdDetectedAssetManagement:rfIdDetectedAssetManagement,
        rfIdDetectedRawDataManagement:rfIdDetectedRawDataManagement
}
