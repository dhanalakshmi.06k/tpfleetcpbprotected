/**
 * Created by Suhas on 8/25/2016.
 */

var mongoose = require('mongoose');
var assetsConfiguration =  mongoose.model('sensorsConfiguration');
var getAssetNameFieldByAssetType = function(type){
        return new Promise(function(resolve,reject){
                assetsConfiguration.findOne({'sensorConfiguration.sensorType.typeId':type}
                ,function(err,result){
                        if(err){
                                console.log(err.stack)
                        }else{
                        getAssetNameRequiredByType(result.sensorConfiguration.configuration)
                        .then(function(data){
                                resolve(data)
                        })
                        }
                })
        })
}
function getAssetNameRequiredByType(data){
        return new Promise(function(resolve,reject){
                var k = Object.keys(data);
                k.forEach(function (objKey, index) {
                        if (data[k[index]].type) {
                                if(data[k[index]].assetName){
                                        resolve(k[index])
                                }
                        }
                });
        })
}

module.exports={
        getAssetNameFieldByAssetType:getAssetNameFieldByAssetType
}