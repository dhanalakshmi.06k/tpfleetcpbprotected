/**
 * Created by Suhas on 9/6/2016.
 */
var rfIdDetectionModel = require('../models/rfIdDetection');
var moment = require('moment');
var getTotalCount = function(){
        return new Promise(function(resolve,reject){
                rfIdDetectionModel.count({},function(err,rfIdDetectionCount){
                        if(err){
                                reject(err)
                        }
                        else{
                                var data={
                                        count:rfIdDetectionCount
                                }
                                resolve(data)
                        }
                })
        })
}
var getDataBySkipAndLimit = function(skip,limit){
        return new Promise(function(resolve,reject){
                rfIdDetectionModel.find({})
                .sort({'created_at':-1})
                .skip(parseInt(skip))
                .limit(parseInt(limit))
                .exec(function(err,rfIdDetectionCount){
                        if(err){
                                reject(err)
                        }
                        else{
                                resolve(rfIdDetectionCount)
                        }
                })
        })
}
var rfIdDetectionDataCountByDate = function(date){
        console.log("***************datedatedatedate************************")
        console.log(date)
        return new Promise(function(resolve,reject){
                var startDate=moment(date).startOf('day');
                var endDate=moment(date).set('hour', 23).set('minute', 59).set('second', 59);
                console.log(startDate)
                console.log(endDate)
                rfIdDetectionModel.count({created_at:{$gt:startDate,$lt:endDate}},function(err,rfIdDetectionCount){
                        if(err){reject(err)}
                        else{
                                var data={
                                        count:rfIdDetectionCount
                                }
                                resolve(data)
                        }
                })
        })
}
var rfIdDetectionDataByDate = function(date,skip,limit){
        return new Promise(function(resolve,reject){
                var startDate=moment(date).startOf('day');
                var endDate=moment(date).set('hour', 23).set('minute', 59).set('second', 59);
                rfIdDetectionModel.find({created_at: { $gt: startDate, $lt: endDate }})
                        .sort({'created_at':-1})
                        .skip(parseInt(skip))
                        .limit(parseInt(limit))
                        .exec(function(err,rfIdDetectionCount){
                                if(err){
                                        reject(err)
                                }
                                else{
                                        resolve(rfIdDetectionCount)
                                }
                        })
        })
}


var rfIdHistoryDetectionDataCountByDate = function(fromDate,toDate,fromTime,toTime){
        return new Promise(function(resolve,reject){
                var startDate=moment(fromDate).set('hour', fromTime);
                var endDate=moment(toDate).set('hour', toTime);
                console.log(startDate)
                console.log(endDate)
                rfIdDetectionModel.count({created_at:{$gt:startDate,$lt:endDate}},function(err,rfIdDetectionCount){
                        if(err){reject(err)}
                        else{
                                var data={
                                        count:rfIdDetectionCount
                                }
                                resolve(data)
                        }
                })
        })
}
var rfIdDetectionAllDataByDateRange = function(startDate,endDate){
        return new Promise(function(resolve,reject){
                var startDate=moment(startDate).startOf('day');
                var endDate=moment(endDate).set('hour', 23).set('minute', 59).set('second', 59);
                rfIdDetectionModel.find({created_at: { $gt: startDate, $lt: endDate }})
                    .sort({'created_at':-1})
                    .exec(function(err,rfIdDetectionCount){
                            if(err){
                                    reject(err)
                            }
                            else{
                                    resolve(rfIdDetectionCount)
                            }
                    })
        })
}


var rfIdDetectionAllDataByDateRangeWithDwellingTime = function(start,end){
        return new Promise(function(resolve,reject){
                var startDate=moment(start).startOf('day');
                var endDate=moment(end).set('hour', 23).set('minute', 59).set('second', 59);
                rfIdDetectionModel.find({created_at: { $gt: startDate, $lt: endDate }})
                        .sort({'carLinked.carNo':-1})
                        .sort({'created_at':-1})
                        .exec(function(err,rfIdDetectionCount){
                                if(err){
                                        reject(err)
                                }
                                else{
                                        if(rfIdDetectionCount.length>0){
                                                calculateDwellingTime(rfIdDetectionCount)
                                                        .then(function(modifiedArray){
                                                                resolve(modifiedArray);
                                                        })
                                        }else{
                                                resolve([]);
                                        }
                                }
                        })
        })
}

function calculateDwellingTime(array){
        return new Promise(function(resolve,reject){
                try{
                        var allRfIdDetectedArray = array;
                        var allRfIdDetectedArrayWithDwellingTime = [];
                        for(var i=0;i<allRfIdDetectedArray.length;i++){
                                var obj = {
                                        carNo:'',
                                        ingress:'',
                                        outgress:'',
                                        dwellingTime:'',
                                        driverName:''
                                }
                                var rfIdDetectedObj = allRfIdDetectedArray[i];
                                var nextRfIdDetectedObj = null;
                                obj.carNo = rfIdDetectedObj.carLinked.carNo;
                                nextRfIdDetectedObj = allRfIdDetectedArray[i+1];
                                var inTime;
                                var outTime;
                                var difference;
                                if(allRfIdDetectedArray[i+1]){
                                        nextRfIdDetectedObj = allRfIdDetectedArray[i+1];
                                         inTime = moment(rfIdDetectedObj.created_at);
                                         outTime = moment(nextRfIdDetectedObj.created_at);
                                         difference = inTime.diff(outTime,"seconds")
                                }
                                if(rfIdDetectedObj.driverLinked && rfIdDetectedObj.driverLinked.driverName){
                                        obj.driverName = rfIdDetectedObj.driverLinked.driverName;
                                }
                                if(nextRfIdDetectedObj && rfIdDetectedObj.carLinked.carNo===nextRfIdDetectedObj.carLinked.carNo && difference<2400){
                                        obj.outgress=inTime;
                                        obj.ingress=outTime;
                                        obj.dwellingTime=difference;
                                        if(i<allRfIdDetectedArray.length-1){
                                                i++;
                                        }
                                }else{
                                        if(rfIdDetectedObj.direction==='entry'){
                                                obj.ingress=rfIdDetectedObj.created_at;
                                                obj.dwellingTime='-';
                                        }
                                        else{
                                                obj.outgress=rfIdDetectedObj.created_at;
                                                obj.dwellingTime='-';
                                        }
                                }
                                allRfIdDetectedArrayWithDwellingTime.unshift(obj)
                                if(i==allRfIdDetectedArray.length-1){
                                        resolve(allRfIdDetectedArrayWithDwellingTime)
                                }
                        }
                }catch(e){
                        console.log(e.stack);
                        reject(e)
                }
        })

}


module.exports={
        getTotalCount:getTotalCount,
        getDataBySkipAndLimit:getDataBySkipAndLimit,
        rfIdDetectionDataByDate:rfIdDetectionDataByDate,
        rfIdDetectionDataCountByDate:rfIdDetectionDataCountByDate,
        rfIdHistoryDetectionDataCountByDate:rfIdHistoryDetectionDataCountByDate,
        rfIdDetectionAllDataByDateRange:rfIdDetectionAllDataByDateRange,
        rfIdDetectionAllDataByDateRangeWithDwellingTime:rfIdDetectionAllDataByDateRangeWithDwellingTime
}