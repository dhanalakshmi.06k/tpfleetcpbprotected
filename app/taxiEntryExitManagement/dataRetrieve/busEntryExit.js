/**
 * Created by Dhanalakshmi on 2/23/2016.
 */
var mongoose = require('mongoose'),
    socketIo = require('../../../config/socketIo').getSocketIoServer();
var pushTaxiData = function(data){
        socketIo.emit('busEntryExitData',data);

}
module.exports={
        pushTaxiData:pushTaxiData
}