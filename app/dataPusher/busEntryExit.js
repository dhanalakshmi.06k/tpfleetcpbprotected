/**
 * Created by Suhas on 2/23/2016.
 */
var mongoose = require('mongoose');
var socketConn = require('../../config/socketIo');
var pushData = function(data){
    let MS_PER_MINUTE = 60000;
    data.departureTimeStamp = new Date();
    data.arrivalTimeStamp = new Date(data.departureTimeStamp - getRandomArbitrary(5, 125) * MS_PER_MINUTE);
    console.log(data.departureTimeStamp);
    console.log(data.arrivalTimeStamp);

    // socketIo.emit('busEntryExitData',data);
    socketConn.getSocketIoServer().emit('busEntryExitData',data);
    // var socketConn = require('../../config/socketIo');
        /*console.log("data pusher bus entry exit")*/
}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}
module.exports={
        pushData:pushData
}