/**
 * Created by Suhas on 8/10/2016.
 */

var fs = require('fs');
var path  =require('path');
var dirPath = fs.realpathSync(__dirname);
var sensorDataModel = require('../models/sensorModel');
var mongoose = require('mongoose'),
config = require('../../config/config');
mongoose.connect(config.db);
var generateTaxiFleetCarDataCounter = 0;
function generateTaxiFleetCarData(){
        return new Promise(function(resolve,reject) {
                var carJsonPath = path.join(dirPath, "../", "data/carData.json");
                var sensorConfiguration = JSON.parse(fs.readFileSync(carJsonPath));
                if (sensorConfiguration && sensorConfiguration.length > 0) {
                        for (var i = 0; i < sensorConfiguration.length; i++) {
                                var sensorObj = sensorConfiguration[i]
                                var sensorConfigObj = new sensorDataModel();
                                sensorConfigObj.sensorData = {};
                                sensorConfigObj.sensorData = {
                                        "sensorLabel": "Car",
                                        "sensorType": "car",
                                        "assetName": sensorObj["Car #"],
                                        "dutyType": sensorObj["Duty Type"],
                                        "vinNo": sensorObj["VIN"],
                                        "modelNo": sensorObj["Model"],
                                        "make": sensorObj["Make"],
                                        "carNo": sensorObj["Car #"],
                                        "fleetNo": sensorObj["Car #"]
                                };
                                sensorConfigObj.save(function (err,result) {
                                        generateTaxiFleetCarDataCounter++
                                        if (err) {
                                                console.log(err.stack)
                                        } else {
                                               /* console.log("result Saved")*/
                                        }
                                        if (sensorConfiguration.length == generateTaxiFleetCarDataCounter) {
                                                resolve();
                                        }
                                })/**/

                        }
                }
        })
}
if(process.argv.indexOf("--force")>0){
        sensorDataModel.remove({'sensorData.sensorType':'car'},function(){
                console.log("All Car Assets Previous Data  Saved Successfully");
                generateTaxiFleetCarData().then(function(){
                        console.log("All Car Assets Saved Successfully");
                        process.exit(0);
                })
        })
}else{
        generateTaxiFleetCarData().then(function(){
                console.log("All Car Assets Saved Successfully");
                process.exit(0);
        })
}