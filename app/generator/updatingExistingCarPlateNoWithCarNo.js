/**
 * Created by Suhas on 9/13/2016.
 */
/**
 * Created by Suhas on 8/10/2016.
 */

var sensorDataModel = require('../models/sensorModel');
var mongoose = require('mongoose'),
        config = require('../../config/config');
mongoose.connect(config.db);
var generateTaxiFleetCarDataCounter = 0;
function updateCarRegistrationNo(){
        return new Promise(function(resolve,reject) {
                sensorDataModel.find({'sensorData.sensorType':'car'},function(err,carAssetDataArray){
                        if(err){console.log(err.stack)}else{
                                if (carAssetDataArray && carAssetDataArray.length > 0) {
                                        for (var i = 0; i < carAssetDataArray.length; i++) {
                                                var sensorConfigObj = carAssetDataArray[i];
                                                /*sensorConfigObj.sensorData = {};*/
                                                if(sensorConfigObj.sensorData.carNo){
                                                        sensorConfigObj.sensorData.registrationNumber = sensorConfigObj.sensorData.carNo;
                                                }
                                                sensorDataModel.findOneAndUpdate({_id:sensorConfigObj._id},sensorConfigObj,function (err,result) {
                                                        generateTaxiFleetCarDataCounter++
                                                        if (err) {
                                                                console.log(err.stack)
                                                        } else {
                                                                /* console.log("result Saved")*/
                                                        }
                                                        if (carAssetDataArray.length == generateTaxiFleetCarDataCounter) {
                                                                resolve();
                                                        }
                                                })

                                        }
                                }
                        }
                })
        })
}
function run(){
        updateCarRegistrationNo().then(function(){
                console.log("updated")
                process.exit(0)
        })
}
run();