/**
 * Created by zendynamix on 9/13/2016.
 */


var moment =require('moment')
var rfIdDetectionModel = require('../models/rfIdDetection');
var mongoose = require('mongoose'),
        config = require('../../config/config');
mongoose.connect(config.db);
var data = [
        cd {
                "direction" : "exit",
                "detectionTime" : "1999-12-31T18:30:00.000+0000",
                "count" :  1,
                "antennaNo" : 1,
                "strength" : -64,
                "carLinked" : {
                        "dutyType" : "Public Taxi",
                        "carNo" : "DA007",
                        "registrationNumber" : "DA007"
                }
        },
        {
                "direction" : "exit",
                "detectionTime" : "1999-12-31T18:30:00.000+0000",
                "count" :  1,
                "antennaNo" : 1,
                "strength" : -64,
                "carLinked" : {
                        "dutyType" : "Public Taxi",
                        "carNo" : "DA003",
                        "registrationNumber" : "DA003"
                }
        },
        {
                "direction" : "exit",
                "detectionTime" : "1999-12-31T18:30:00.000+0000",
                "count" :  1,
                "antennaNo" : 1,
                "strength" : -64,
                "carLinked" : {
                        "dutyType" : "Public Taxi",
                        "carNo" : "DA002",
                        "registrationNumber" : "DA002"
                }
        },
        {
                "direction" : "exit",
                "detectionTime" : "1999-12-31T18:30:00.000+0000",
                "count" :  1,
                "antennaNo" : 1,
                "strength" : -64,
                "carLinked" : {
                        "dutyType" : "Public Taxi",
                        "carNo" : "DA032",
                        "registrationNumber" : "DA032"
                }
        },
        {
                "direction" : "exit",
                "detectionTime" : "1999-12-31T18:30:00.000+0000",
                "count" :  1,
                "antennaNo" : 1,
                "strength" : -64,
                "carLinked" : {
                        "dutyType" : "Public Taxi",
                        "carNo" : "DA015",
                        "registrationNumber" : "DA015"
                }
        },
        {
                "direction" : "exit",
                "detectionTime" : "1999-12-31T18:30:00.000+0000",
                "count" :  1,
                "antennaNo" : 1,
                "strength" : -64,
                "carLinked" : {
                        "dutyType" : "Public Taxi",
                        "carNo" : "DA010",
                        "registrationNumber" : "DA010"
                }
        },
        {
                "direction" : "exit",
                "detectionTime" : "1999-12-31T18:30:00.000+0000",
                "count" :  1,
                "antennaNo" : 1,
                "strength" : -64,
                "carLinked" : {
                        "dutyType" : "Public Taxi",
                        "carNo" : "DA010",
                        "registrationNumber" : "DA010"
                }
        }
]
var DATES = [
        {
                "H": 2,
                'M':21,
                'MON':8,
                'DAY':11
        },
        {
                "H": 1,
                'M':55,
                'MON':8,
                'DAY':11
        },
        {
                "H": 1,
                'M':13,
                'MON':8,
                'DAY':11
        },
        {
                "H": 4,
                'M':29,
                'MON':8,
                'DAY':11
        },
        {
                "H": 4,
                'M':47,
                'MON':8,
                'DAY':11
        },
        {
                "H": 2,
                'M':11,
                'MON':8,
                'DAY':11
        },
        {
                "H": 23,
                'M':30,
                'MON':8,
                'DAY':11
        }
]

function generateSampleRfIddetectionData(){
        for(var i=0;i<data.length;i++){
                var rfIdDetectionData = data[i];
                var rfIdDetectionObj = new rfIdDetectionModel();
                var date = new Date();
                date.setUTCDate(DATES[i]['DAY'])
                date.setUTCHours(DATES[i]['H'])
                date.setUTCMinutes(DATES[i]['M'])
                date.setUTCMonth(DATES[i]['MON'])
                rfIdDetectionObj.detectionTime=date;
                rfIdDetectionObj.rfIdNumber=rfIdDetectionData.rfIdNumber;
                rfIdDetectionObj.direction=rfIdDetectionData.direction;
                rfIdDetectionObj.strength=rfIdDetectionData.strength;
                rfIdDetectionObj.antennaNo=rfIdDetectionData.antennaNo;
                rfIdDetectionObj.count=rfIdDetectionData.count;
                rfIdDetectionObj.carLinked=rfIdDetectionData.carLinked;
                rfIdDetectionObj.created_at=moment.utc(date);
                rfIdDetectionObj.updated_at=moment.utc(date);
                rfIdDetectionObj.save(function(err,dataSaved){
                        console.log("data Saved")
                })
        }
}

function run(){
        rfIdDetectionModel.remove({},function(){

                /*generateSampleRfIddetectionData();*/
        })
}run();
