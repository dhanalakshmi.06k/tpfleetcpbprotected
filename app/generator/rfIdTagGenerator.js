/**
 * Created by Suhas on 9/13/2016.
 */

var AssetConfigurationModel = require('../models/sensorModel');
var mongoose = require('mongoose'),
        config = require('../../config/config');
mongoose.connect(config.db);
var generateRfIdTagDataCounter = 0;

var rfIdArrayDetailsArray = [
        {
                "": "",
                "rFIdTagId": "082A2",
                "FLEET NUMBER": "DA007",
                "vinNo": "6T1BF9FK3FX583667"
        },
        {
                "": "",
                "rFIdTagId": "08249",
                "FLEET NUMBER": "DA003",
                "vinNo": "6T1BF9FK9FX582944"
        },
        {
                "": "",
                "rFIdTagId": "08244",
                "FLEET NUMBER": "DA008",
                "vinNo": "6T1BF9FK2FX5845460"
        },
        {
                "": "",
                "rFIdTagId": "082AC",
                "FLEET NUMBER": "DA041",
                "vinNo": "6T1BF9FK2FX585037"
        },
        {
                "": "",
                "rFIdTagId": "08201",
                "FLEET NUMBER": "DA002",
                "vinNo": "6T1BF9FK3FX584205"
        },
        {
                "": "",
                "rFIdTagId": "08024",
                "FLEET NUMBER": "DA016",
                "vinNo": "6T1BF9FK9FX584774"
        },
        {
                "": "",
                "rFIdTagId": "08253",
                "FLEET NUMBER": "DA001",
                "vinNo": "6T1BF9FK3FX582258"
        },
        {
                "": "",
                "rFIdTagId": "08019",
                "FLEET NUMBER": "DA050",
                "vinNo": "6T1BF9FK1FX584882"
        },
        {
                "": "",
                "rFIdTagId": "0801C",
                "FLEET NUMBER": "DA012",
                "vinNo": "6T1BF9FK6FX585283"
        },
        {
                "": "",
                "rFIdTagId": "807FFB",
                "FLEET NUMBER": "DA034",
                "vinNo": "6T1BF9FK0FX583691"
        },
        {
                "": "",
                "rFIdTagId": "082D2",
                "FLEET NUMBER": "DA005",
                "vinNo": "6T1BF9FK5FX583654"
        },
        {
                "": "",
                "rFIdTagId": "0828B",
                "FLEET NUMBER": "DA043",
                "vinNo": "6T1BF9FK8FX583485"
        },
        {
                "": "",
                "rFIdTagId": "08012",
                "FLEET NUMBER": "DA021",
                "vinNo": "6T1BF9FK8FX583759"
        },
        {
                "": "",
                "rFIdTagId": "0800A",
                "FLEET NUMBER": "DA037",
                "vinNo": "6T1BF9FK0FX583707"
        },
        {
                "": "",
                "rFIdTagId": "082A9",
                "FLEET NUMBER": "DA039",
                "vinNo": "6T1BF9FK0FX584176"
        },
        {
                "": "",
                "rFIdTagId": "082B4",
                "FLEET NUMBER": "DA046",
                "vinNo": "6T1BF9FKXFX585335"
        },
        {
                "": "",
                "rFIdTagId": "082B1",
                "FLEET NUMBER": "DA038",
                "vinNo": "6T1BF9FK7FX585096"
        },
        {
                "": "",
                "rFIdTagId": "082CA",
                "FLEET NUMBER": "DA040",
                "vinNo": "6T1BF9FK1FX583506"
        },
        {
                "": "",
                "rFIdTagId": "082BB",
                "FLEET NUMBER": "DA048",
                "vinNo": "6T1BF9FK1FX583506"
        },
        {
                "": "BLUETOOTH",
                "rFIdTagId": "082C3",
                "FLEET NUMBER": "DA013",
                "vinNo": "6T1BF9FK4FX584309"
        },
        {
                "": "BLUETOOTH",
                "rFIdTagId": "0829A",
                "FLEET NUMBER": "DA019",
                "vinNo": "6T1BF9FK6FX583646"
        },
        {
                "": "BLUETOOTH",
                "rFIdTagId": "0803A",
                "FLEET NUMBER": "DA017",
                "vinNo": "6T1BF9FK56FX583856"
        },
        {
                "": "BLUETOOTH",
                "rFIdTagId": "0824C",
                "FLEET NUMBER": "DA049",
                "vinNo": "6T1BF9FK5FX583931"
        },
        {
                "": "BLUETOOTH",
                "rFIdTagId": "0826A",
                "FLEET NUMBER": "DA006",
                "vinNo": "6T1BF9FK7FX584322"
        },
        {
                "": "BLUETOOTH",
                "rFIdTagId": "08272",
                "FLEET NUMBER": "DA035",
                "vinNo": "6T1BF9FX1FX584154"
        },
        {
                "": "BLUETOOTH / GPS 359710048984422",
                "rFIdTagId": "08263",
                "FLEET NUMBER": "DA024",
                "vinNo": "6T1BF9FK3FX583751"
        },
        {
                "": "BLUETOOTH",
                "rFIdTagId": "08279",
                "FLEET NUMBER": "DA009",
                "vinNo": "6T1BF9FK4FX583838"
        },
        {
                "": "BLUETOOTH",
                "rFIdTagId": "0825B",
                "FLEET NUMBER": "DA014",
                "vinNo": "6T1BF9FK7FX584059"
        },
        {
                "": "BLUETOOTH",
                "rFIdTagId": "0827C",
                "FLEET NUMBER": "DA004",
                "vinNo": "6T1BF9FK4FX585475"
        },
        {
                "": "",
                "rFIdTagId": "08281",
                "FLEET NUMBER": "DA010",
                "vinNo": "6T1BF9FK4FX584802"
        },
        {
                "": "",
                "rFIdTagId": "08293",
                "FLEET NUMBER": "DA015",
                "vinNo": "6T1BF9FK4FX583628"
        },
        {
                "": "",
                "rFIdTagId": "08002",
                "FLEET NUMBER": "DA022",
                "vinNo": "6T1BF9FK1FX585353"
        },
        {
                "": "",
                "rFIdTagId": "08054",
                "FLEET NUMBER": "DA032",
                "vinNo": "6T1BF9FK4FX584214"
        },
        {
                "": "GPS 359710048989447",
                "rFIdTagId": "08051",
                "FLEET NUMBER": "DA018",
                "vinNo": "6T1BF9FK5FX584951"
        },
        {
                "": "GPS 359710048990635",
                "rFIdTagId": "08064",
                "FLEET NUMBER": "DA042",
                "vinNo": "6T1BF9FK3FX583474"
        },
        {
                "": "",
                "rFIdTagId": "08059",
                "FLEET NUMBER": "DA029",
                "vinNo": "6T1BF9FK3FX583779"
        },
        {
                "": "GPS 359710048988266",
                "rFIdTagId": "08082",
                "FLEET NUMBER": "DA026",
                "vinNo": "6T1BF9FK4FX585198"
        },
        {
                "": "",
                "rFIdTagId": "0805C",
                "FLEET NUMBER": "DA011",
                "vinNo": "6T1BF9FK7FX584515"
        }
]
function generateRfIdTagDetails(){
        return new Promise(function(resolve,reject) {
                var rfIdDetailsConfigurationArray = rfIdArrayDetailsArray;
                if (rfIdDetailsConfigurationArray && rfIdDetailsConfigurationArray.length > 0) {
                        for (var i = 0; i < rfIdDetailsConfigurationArray.length; i++) {
                                var rfIdConfigurationObj = rfIdDetailsConfigurationArray[i]
                                var assetConfigurationModelObj = new AssetConfigurationModel();
                                assetConfigurationModelObj.sensorData = {};
                                assetConfigurationModelObj.sensorData.sensorType = 'rfIdTag';
                                assetConfigurationModelObj.sensorData.sensorLabel = 'RFID Tag';
                                assetConfigurationModelObj.sensorData.type = 'Active';
                                        assetConfigurationModelObj.sensorData.uniqueIdentifier = rfIdConfigurationObj.rFIdTagId;
                                assetConfigurationModelObj.sensorData.assetName = rfIdConfigurationObj.rFIdTagId;
                                assetConfigurationModelObj.save(function (err,result) {
                                        generateRfIdTagDataCounter++;
                                        if (err) {
                                                console.log(err.stack)
                                        } else {
                                                /* console.log("result Saved")*/
                                        }
                                        if (rfIdDetailsConfigurationArray.length == generateRfIdTagDataCounter) {
                                                console.log("No Of RfId Tags Generated  "+generateRfIdTagDataCounter)
                                                resolve();
                                        }
                                })

                        }
                }
        })
}

if(process.argv.indexOf("--force")>=0){
        console.log("All Previous Drivers Details Removed Successfully");
        AssetConfigurationModel.remove({'sensorData.sensorType':'driver'},function(){
                generateRfIdTagDetails().then(function(){
                        console.log("All Drivers Details Saved Successfully");
                        process.exit(0);
                })
        })
}
else{
        generateRfIdTagDetails().then(function(){
                console.log("All Drivers Details Saved Successfully");
                process.exit(0);
        })
}