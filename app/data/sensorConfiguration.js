var formConfigDetails = {
    configDetails:[
        {
            "sensorConfiguration" : {

                "instanceDataMapper" : {
                },
                "sensorType" :{
                    "typeId":"car",
                    "label":"Car"
                },
                "configuration" : {
                    "make" : {
                        "type" : "dropDown",
                        "description" : "Enter Manufacturer Name",
                        "label":"Make",
                        "required":false,
                        "autoComplete":true,
                        "value":{
                            "dropDownValues":["Kia","Lexus","Nissan","Toyota","Volvo"],
                            "defaultValue":"Hatta Taxi"
                        }
                    },/**/
                    "modelNo" : {
                        "type" : "dropDown",
                        "description" : "Enter Model Number",
                        "label":"Model Number",
                        "required":false,
                        "autoComplete":true,
                        "value":{
                            "dropDownValues":["4Runner","Avalon","Camry","Civilian","ES",
                                "GS","Land Cruiser","LS","Maxima","Patrol","Sentra",
                                "Sequoia","Sienna","URvan","V40"],
                            "defaultValue":"4Runner"
                        }
                    },
                    "registrationNumber" : {
                        "type" : "text",
                        "description" : "Enter Registration Number",
                        "label":"Registration Number",
                        "required":false,
                        "autoComplete":false,
                        "assetName":true
                    },
                    "vinNo" : {
                        "type" : "text",
                        "description" : "Enter Vin Number",
                        "label":"Vin Number",
                        "required":false,
                        "autoComplete":false
                    },
                    "purchased year" : {
                        "type" : "Number",
                        "description" : "Enter purchased Year",
                        "label":"Year",
                        "required":false,
                        "autoComplete":false
                    },
                    "manufactured year" : {
                        "type" : "Number",
                        "description" : "Enter Manufactured Year",
                        "label":"Year",
                        "required":false,
                        "autoComplete":false
                    },
                    "uniqueIdentifier" : {
                        "type" : "text",
                        "description" : "Enter Unique Identifier",
                        "label":"Unique Identifier",
                        "required":false,
                        "autoComplete":false,
                        "assetName":true
                    },
                    "isVechicle" : {
                        "type" : "dropDown",
                        "description" : "Enter vechicle Name",
                        "label":"Is Vechicle",
                        "required":false,
                        "readOnly":true,
                        "autoComplete":true,
                        "value":{
                            "dropDownValues":[true,false],
                            "defaultValue":true
                        }
                    }
                }
            }
        },
        {
            "sensorConfiguration" : {
                "sensorType" :{
                    "typeId":"driver",
                    "label":"Driver"
                },
                "configuration" : {
                    "driverId" : {
                        "type" : "text",
                        "assetName":true,
                        "description" : "Enter Driver Id",
                        "label":"Driver Id",
                        "required":false,
                        "autoComplete":true
                    },
                    "nationality" : {
                        "type" : "text",
                        "description" : "Enter Nationality",
                        "label":"Nationality",
                        "required":false,
                        "autoComplete":true
                    },
                    "gender" : {
                        "type" : "dropDown",
                        "description" : "Enter Driver Name",
                        "label":"Driver Name",
                        "required":false,
                        "autoComplete":true,
                        "value":{
                            "dropDownValues": ["Male","Female"],
                            "defaultValue":"Male"
                        }
                    },
                    "department" : {
                        "type" : "text",
                        "description" : "Enter Department",
                        "label":"Department",
                        "required":false,
                        "autoComplete":true
                    },
                    "dateOfJoin" : {
                        "type" : "date",
                        "description" : "Enter Date Of Join",
                        "label":"Date Of Join",
                        "required":false,
                        "autoComplete":true
                    },
                    "accommodation" : {
                        "type" : "date",
                        "description" : "Enter Your Accommodation",
                        "label":"Accommodation",
                        "required":false,
                        "autoComplete":true
                    },
                    "licenceNo" : {
                        "type" : "text",
                        "description" : "Enter Licence No",
                        "label":"Licence No",
                        "required":false,
                        "autoComplete":true
                    },
                    "mobileNo" : {
                        "type" : "number",
                        "description" : "Enter Mobile No",
                        "label":"Mobile No",
                        "required":false,
                        "autoComplete":true
                    },
                    "dutyType" : {
                        "type" : "dropDown",
                        "description" : "Enter Duty Type",
                        "label":"Duty Type",
                        "required":false,
                        "value":{
                            "dropDownValues": ["Hatta Taxi","Ladies and Family","Limousine","Off-road","Public Taxi","Special Needs"],
                            "defaultValue":"Hatta Taxi"
                        },
                        "autoComplete":false
                    },
                    "isVechicle" : {
                        "type" : "dropDown",
                        "description" : "Enter vechicle Name",
                        "label":"Is Vechicle",
                        "required":false,
                        "readOnly":true,
                        "autoComplete":false,
                        "value":{
                            "dropDownValues":[true,false],
                            "defaultValue":false
                        }
                    }
                },
                "instanceDataMapper":{
                }

            }
        },
        {
            "sensorConfiguration" : {
                "sensorType" :{
                    "typeId":"rfIdTag",
                    "label":"RFID Tag"
                },
                "configuration" : {
                    "make" : {
                        "type" : "text",
                        "description" : "Enter Manufacturer Name",
                        "label":"Make",
                        "required":false,
                        "autoComplete":true
                    },
                    "modelNo" : {
                        "type" : "text",
                        "description" : "Enter Model Number",
                        "label":"Model Number",
                        "required":false,
                        "autoComplete":true
                    },
                    "type" : {
                        "type" : "dropDown",
                        "description" : "Enter Model Number",
                        "label":"Model Number",
                        "required":false,
                        "value":{
                            "dropDownValues":["Active","Passive"],
                            "defaultValue":"Active"
                        },
                        "autoComplete":false
                    },
                    "uniqueIdentifier" : {
                        "type" : "text",
                        "description" : "Enter Unique Identifier",
                        "label":"Unique Identifier",
                        "required":false,
                        "autoComplete":false,
                        "assetName":true
                    },
                    "isVechicle" : {
                        "type" : "dropDown",
                        "description" : "Enter vechicle Name",
                        "label":"Is Vechicle",
                        "required":false,
                        "readOnly":true,
                        "autoComplete":false,
                        "value":{
                            "dropDownValues":[true,false],
                            "defaultValue":false
                        }
                    }
                },
                "instanceDataMapper":{
                }


            }
        },
        {
            "sensorConfiguration" : {
                "sensorType" :{
                    "typeId":"obdDevice",
                    "label":"Obd Device"
                },
                "configuration" : {
                    "make" : {
                        "type" : "text",
                        "description" : "Enter Manufacturer Name",
                        "label":"Make",
                        "required":false,
                        "autoComplete":true
                    },
                    "modelNo" : {
                        "type" : "text",
                        "description" : "Enter Model Number",
                        "label":"Model Number",
                        "required":false,
                        "autoComplete":true
                    },
                    "uniqueIdentifier" : {
                        "type" : "text",
                        "description" : "Unique Identifier",
                        "label":"IMEI Number / UId",
                        "required":false,
                        "autoComplete":false
                    },
                    "gsmId" : {
                        "type" : "text",
                        "description" : "Enter GSM Id",
                        "label":"GSM Id",
                        "required":false,
                        "autoComplete":false,
                        "assetName":true
                    },
                    "isVechicle" : {
                        "type" : "dropDown",
                        "description" : "Enter vechicle Name",
                        "label":"Is Vechicle",
                        "required":false,
                        "readOnly":true,
                        "autoComplete":false,
                        "value":{
                            "dropDownValues":[true,false],
                            "defaultValue":false
                        }
                    }
                },
                "instanceDataMapper":{
                }

            }
        },
        {
            "sensorConfiguration" : {

                "instanceDataMapper" : {
                },
                "sensorType" :{
                    "typeId":"truck",
                    "label":"truck"
                },
                "configuration" : {
                    "make" : {
                        "type" : "dropDown",
                        "description" : "Enter Manufacturer Name",
                        "label":"Make",
                        "required":false,
                        "autoComplete":true,
                        "value":{
                            "dropDownValues":["ACMAT","AEC","Alexander Dennis","Armstrong","Argyle"],
                            "defaultValue":"Argyle"
                        }

                    },/**/
                    "modelNo" : {
                        "type" : "dropDown",
                        "description" : "Enter Model Number",
                        "label":"Model Number",
                        "required":false,
                        "autoComplete":true,
                        "value":{
                            "dropDownValues":["VLRA 2","Astra HD","BAZ-6306"],
                            "defaultValue":"VLRA 2"
                        }
                    },
                    "registrationNumber" : {
                        "type" : "text",
                        "description" : "Enter Registration Number",
                        "label":"Registration Number",
                        "required":false,
                        "autoComplete":false,
                        "assetName":true
                    },
                    "vinNo" : {
                        "type" : "text",
                        "description" : "Enter Vin Number",
                        "label":"Vin Number",
                        "required":false,
                        "autoComplete":false
                    },
                    "Manufactured year" : {
                        "type" : "Number",
                        "description" : "Enter Manufactured Year",
                        "label":"Year",
                        "required":false,
                        "autoComplete":false
                    },
                    "PurchaseDate year" : {
                        "type" : "Number",
                        "description" : "Enter PurchaseDate Year",
                        "label":"Year",
                        "required":false,
                        "autoComplete":false
                    },
                    "uniqueIdentifier" : {
                        "type" : "text",
                        "description" : "Enter Unique Identifier",
                        "label":"Unique Identifier",
                        "required":false,
                        "autoComplete":false,
                        "assetName":true
                    },
                    "isVechicle" : {
                        "type" : "dropDown",
                        "description" : "Enter vechicle Name",
                        "label":"Is Vechicle",
                        "required":false,
                        "readOnly":true,
                        "autoComplete":false,
                        "value":{
                            "dropDownValues":[true,false],
                            "defaultValue":true
                        }
                    }
                }
            }
        },
        {
            "sensorConfiguration" : {

                "instanceDataMapper" : {
                },
                "sensorType" :{
                    "typeId":"scooter",
                    "label":"scooter"
                },
                "configuration" : {
                    "make" : {
                        "type" : "dropDown",
                        "description" : "Enter Manufacturer Name",
                        "label":"Make",
                        "required":false,
                        "autoComplete":true,
                        "value":{
                            "dropDownValues":["Adly","Aeon","Bajaj","Armstrong","BMW"],
                            "defaultValue":"Bajaj"
                        }

                    },/**/
                    "modelNo" : {
                        "type" : "dropDown",
                        "description" : "Enter Model Number",
                        "label":"Model Number",
                        "required":false,
                        "autoComplete":true,
                        "value":{
                            "dropDownValues":["Honda Activa 5G","TVS Jupiter","Honda Grazia"],
                            "defaultValue":"Honda Grazia"
                        }
                    },
                    "registrationNumber" : {
                        "type" : "text",
                        "description" : "Enter Registration Number",
                        "label":"Registration Number",
                        "required":false,
                        "autoComplete":false,
                        "assetName":true
                    },
                    "vinNo" : {
                        "type" : "text",
                        "description" : "Enter Vin Number",
                        "label":"Vin Number",
                        "required":false,
                        "autoComplete":false
                    },
                    "Manufactured year" : {
                        "type" : "Number",
                        "description" : "Enter Manufactured Year",
                        "label":"Year",
                        "required":false,
                        "autoComplete":false
                    },
                    "PurchaseDate year" : {
                        "type" : "Number",
                        "description" : "Enter PurchaseDate Year",
                        "label":"Year",
                        "required":false,
                        "autoComplete":false
                    },
                    "isVechicle" : {
                        "type" : "dropDown",
                        "description" : "Enter vechicle Name",
                        "label":"Is Vechicle",
                        "required":false,
                        "readOnly":true,
                        "autoComplete":false,
                        "value":{
                            "dropDownValues":[true,false],
                            "defaultValue":true
                        }
                    }
                }
            }
        },
        {
            "sensorConfiguration" : {
                "sensorType" :{
                    "typeId":"Air fuel ratio meter",
                    "label":"Air fuel ratio meter"
                },
                "configuration" : {
                    "make" : {
                        "type" : "text",
                        "description" : "Enter Manufacturer Name",
                        "label":"Make",
                        "required":false,
                        "autoComplete":true
                    },
                    "modelNo" : {
                        "type" : "text",
                        "description" : "Enter Model Number",
                        "label":"Model Number",
                        "required":false,
                        "autoComplete":true
                    },
                    "type" : {
                        "type" : "dropDown",
                        "description" : "Enter Model Number",
                        "label":"Model Number",
                        "required":false,
                        "value":{
                            "dropDownValues":["Active","Passive"],
                            "defaultValue":"Active"
                        },
                        "autoComplete":false
                    },
                    "uniqueIdentifier" : {
                        "type" : "text",
                        "description" : "Enter Unique Identifier",
                        "label":"Unique Identifier",
                        "required":false,
                        "autoComplete":false,
                        "assetName":true
                    },
                    "isVechicle" : {
                        "type" : "dropDown",
                        "description" : "Enter vechicle Name",
                        "label":"Is Vechicle",
                        "required":false,
                        "readOnly":true,
                        "autoComplete":false,
                        "value":{
                            "dropDownValues":[true,false],
                            "defaultValue":false
                        }
                    }
                },
                "instanceDataMapper":{
                }

            }
        }
    ]

}

module.exports = formConfigDetails;