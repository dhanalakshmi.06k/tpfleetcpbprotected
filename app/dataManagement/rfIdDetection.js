/**
 * Created by Suhas on 9/6/2016.
 */
var moment = require('moment');
var mongoose = require('mongoose');
var assetManagementModule  = require('../asset');
var rfIdDetectionModel = require('../models/rfIdDetection');
var socketConn = require('../../config/socketIo');
var constants = require('../../config/constants').constants;
var assetModel = mongoose.model('sensors');
var save=function(data){
        var rfIdNo = data[0].split(':');
        rfIdNo = rfIdNo[1];
        var strength = data[1].split(':');
        strength = parseInt(strength[1]);
        var antennaNo = data[2].split(':');
        antennaNo = parseInt(antennaNo[1]);
        var count = data[3].split(':');
        count = parseInt(count[1]);
        var detectionTime = data[4].substring(5,data[4].length);
        console.log(detectionTime)
        var timeOfDetectionTime = moment(detectionTime);

        var rfIdDetectionObj = rfIdDetectionModel();
        console.log("rfIdNo  :"+rfIdNo);
        console.log("strength  :"+strength);
        console.log("antennaNo  :"+antennaNo);
        console.log("count  :"+count);
        console.log("detectionTime  :"+timeOfDetectionTime);
        rfIdDetectionObj.rfIdNo=rfIdNo;
        rfIdDetectionObj.strength=strength;
        rfIdDetectionObj.antennaNo=antennaNo;
        rfIdDetectionObj.count=count;
        rfIdDetectionObj.detectionTime=timeOfDetectionTime;
        rfIdDetectionObj.created_at=timeOfDetectionTime;
        rfIdDetectionObj.updated_at=timeOfDetectionTime;
        rfIdDetectionObj.direction=constants.RFID_DETECTION.DIRECTION.EXIT;
        if(strength<constants.RFID_DETECTION.RANGE_FOR_EXIT){
                rfIdDetectionObj.direction=constants.RFID_DETECTION.DIRECTION.ENTRY;
        }
        var dataToBePushed={
                carLinked:{},
                rfIdDetectionDetails:rfIdDetectionObj,
                driverLinked:{},
                rfIdNo:rfIdNo,
                strength:strength,
                antennaNo:antennaNo,
                count:count,
                detectionTime:detectionTime,
                direction:rfIdDetectionObj.direction,
                created_at:timeOfDetectionTime
        }
        getRelatedAssetDetailsByRfIdNumber(rfIdNo)
                .then(function(rfIdData){
                        if(rfIdData.sensorData.assetsLinked && rfIdData.sensorData.assetsLinked.length>0){
                                checkAndGetAssetLinked(rfIdData.sensorData.assetsLinked,constants.ASSET_DETAILS.ASSET_NAME_CAR)
                                        .then(function(carLinked){
                                                console.log(rfIdDetectionObj)
                                                rfIdDetectionObj.save(function(err,res){
                                                        if(err){
                                                                console.log(err.stack)
                                                        }
                                                        else{
                                                                dataToBePushed.carLinked.registrationNumber = carLinked.ref.sensorData.registrationNumber;
                                                                dataToBePushed.carLinked.carNo = carLinked.ref.sensorData.carNo;
                                                                dataToBePushed.carLinked.dutyType = carLinked.ref.sensorData.dutyType;
                                                                res._doc.carLinked = dataToBePushed.carLinked;
                                                                updateRfIdDetectionInfo(res._id,res);
                                                                if(carLinked){
                                                                        getAssetsDetailsWithRefAssetsLinked(carLinked.ref._id)
                                                                                .then(function(assetDetails){
                                                                                        if(assetDetails && assetDetails.sensorData
                                                                                                && assetDetails.sensorData.assetsLinked &&
                                                                                                assetDetails.sensorData.assetsLinked.length>1){
                                                                                                checkAndGetAssetLinked(assetDetails.sensorData.assetsLinked,constants.ASSET_DETAILS.ASSET_NAME_DRIVER)
                                                                                                        .then(function(driverLinked){
                                                                                                                if(driverLinked){
                                                                                                                        dataToBePushed.driverLinked.driverName = driverLinked.ref.sensorData.driverName;
                                                                                                                        dataToBePushed.driverLinked.driverId = driverLinked.ref.sensorData.driverId;
                                                                                                                        dataToBePushed.driverLinked.badgeNo = driverLinked.ref.sensorData.badgeNo;
                                                                                                                        dataToBePushed.driverLinked.driverId = driverLinked.ref.sensorData.driverId;
                                                                                                                        res._doc.driverLinked = dataToBePushed.driverLinked;
                                                                                                                        updateRfIdDetectionInfo(res._id,res)
                                                                                                                        pushToSocket(dataToBePushed)
                                                                                                                }else{
                                                                                                                        console.warn("Driver has not Been linked To This Taxi " +
                                                                                                                                ""+dataToBePushed.carLinked.carNo+" connected to RF Id "+rfIdNo);
                                                                                                                        pushToSocket(dataToBePushed)
                                                                                                                }
                                                                                                        })
                                                                                        }else{
                                                                                                console.warn("Driver has not Been linked To This Taxi " +
                                                                                                        ""+dataToBePushed.carLinked.carNo+" connected to RF Id "+rfIdNo);
                                                                                                pushToSocket(dataToBePushed)
                                                                                        }
                                                                                })
                                                                }else{

                                                                        console.warn("Taxi has not Been linked To This Rf Id "+rfIdNo);
                                                                }
                                                        }
                                                });
                                        })
                        }
                }).catch(function(err){
                console.log(err.stack)
        });
};

function getRelatedAssetDetailsByRfIdNumber(rfIdNumber){
        return new Promise(function(resolve,reject){
                var length = rfIdNumber.length;
                var alternateRfIdNumber1 = rfIdNumber.substr(19,rfIdNumber.length)
                var alternateRfIdNumber2 = rfIdNumber.substr(18,rfIdNumber.length)
                assetModel.findOne({'sensorData.uniqueIdentifier':{ $in: [rfIdNumber,alternateRfIdNumber1,alternateRfIdNumber2] }})
                        .populate('sensorData.assetsLinked.ref')
                        .exec(function(err,result){
                                if(err)
                                {
                                        reject(err)
                                }else{
                                        if(!result){
                                                reject(new Error("No Data Found For This Rf ID Number "+rfIdNumber))
                                        }else{
                                                resolve(result)
                                        }
                                }
                        })
        })

}
function getAssetsDetailsWithRefAssetsLinked(assetId){
        return new Promise(function(resolve,reject){
                assetModel.findOne({_id:assetId})
                        .populate('sensorData.assetsLinked.ref')
                        .exec(function(err,result){
                                if(err)
                                {
                                        reject(err)
                                }else{
                                        resolve(result)
                                }
                        })
        })
}
function updateRfIdDetectionInfo(id,res){
        rfIdDetectionModel.findOneAndUpdate({_id:id},res,function(err,data){
                console.log('updated')
        })
}
function checkAndGetAssetLinked(assetsLinkedArray,type){
        return new Promise(function(resolve,reject){
                var relatedAssets = null;
                for(var i=0;i<assetsLinkedArray.length;i++){
                        var assetsLinkedObj = assetsLinkedArray[i];
                        if(assetsLinkedObj.type==type){
                                relatedAssets = assetsLinkedObj;
                        }
                        if(i==assetsLinkedArray.length-1){
                                resolve(relatedAssets);
                        }
                }
        })
}
var pushToSocket=function(data){
        socketConn.getSocketIoServer().emit('rfIdDetectionDataPusher',data);
        console.log('data pushed')
}
module.exports={
        save:save,
        pushToSocket:pushToSocket
}