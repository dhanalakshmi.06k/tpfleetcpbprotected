/**
 * Created by Suhas on 7/7/2016.
 */
var notificationModel = require('../models/notificationModel');
var socketConn = require('../../config/socketIo');
var constants = require('../../config/constants').constants;
var save=function(notificationData){
    var notificationObj = notificationModel();
    notificationObj.level=notificationData.level;
    notificationObj.message=notificationData.message;
    notificationObj.timeStamp=new Date(notificationData.timeStamp);
    notificationObj.notificationType=notificationData.notificationType;
    pushToSocket(notificationData)
    notificationObj.save(function(err,res){
        if(err){
            console.log(err.stack)
        }else{
            console.log('Notification Data Saved')
        }
    })
}

var pushToSocket=function(msg){
    msg.timeStamp=new Date(msg.timeStamp)
    socketConn.getSocketIoServer().emit(constants.SOCKET_DETAILS.NOTIFICATION_DATA_PUSHER,msg);
}
module.exports={
    save:save,
    pushToSocket:pushToSocket
}