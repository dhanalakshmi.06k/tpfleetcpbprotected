/**
 * Created by Suhas on 9/6/2016.
 */
var moment = require('moment');
var mongoose = require('mongoose');
var assetManagementModule  = require('../asset');
var rfIdDetectionModel = require('../models/rfIdDetection');
var socketConn = require('../../config/socketIo');
var constants = require('../../config/constants').constants;
var beaconModel = mongoose.model('beaconsDetails');
var saveBeacon=function(data){
    var beaconModelObj = new beaconModel();
    beaconModelObj.beaconId=JSON.parse(data).beaconId
    beaconModelObj.rssi=JSON.parse(data).rssi
    beaconModelObj.companyName=JSON.parse(  data).companyName
    beaconModelObj.save(function(err,res) {
        pushToSocket(data)
        if (err) {
            console.log(err.stack)
        }
    })

};


var pushToSocket=function(data){
    socketConn.getSocketIoServer().emit('beaconMessage',data);
    console.log('data pushed')
}
module.exports={
    saveBeacon:saveBeacon,
    pushToSocket:pushToSocket
}