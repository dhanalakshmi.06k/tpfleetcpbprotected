/**
 * Created by dhanalakshmi on 27/8/16.
 */
var mongoose = require('mongoose'),
 routeConfig = require('../../config/assetServiceConfig'),
    gpsDeviceWoxConstant = require('../../config/addDeviceConstants'),
    request = require('request'),
    SensorsModel = mongoose.model('sensors');
var obdDeviceGPSWOXModel = require('../models/gpswoxobddeviceModel');

var saveOdbAsset=function(assetData,callBack){
    obdDeviceGPSWOXModel.find(function(err,obdDeviceLastId){
        var obdDeviceDetails = new obdDeviceGPSWOXModel();
        console.log("*****************assetData*******saveOdbAsset*********")
        console.log(assetData)
        console.log(obdDeviceLastId.length)
        console.log("*****************assetData*******saveOdbAsset*********")
        /* var url=routeConfig.assetsApiBaseUrl+'add_device?lang='+routeConfig.lang+'&user_api_hash='+routeConfig.user_api_hash;*/
        obdDeviceDetails.id = obdDeviceLastId.length+1;
        obdDeviceDetails.name = assetData.uniqueIdentifier;
        obdDeviceDetails.device_data={}
        obdDeviceDetails.device_data.pivot={}
        obdDeviceDetails.device_data.imei = assetData.uniqueIdentifier;
        obdDeviceDetails.device_data.object_owner = gpsDeviceWoxConstant.object_owner;
        obdDeviceDetails.device_data.icon_id = gpsDeviceWoxConstant.icon_id;
        obdDeviceDetails.device_data.fuel_measurement_id = gpsDeviceWoxConstant.fuel_measurement_id;
        obdDeviceDetails.device_data.fuel_quantity = gpsDeviceWoxConstant.fuel_quantity;
        obdDeviceDetails.device_data.min_moving_speed = gpsDeviceWoxConstant.min_moving_speed;
        obdDeviceDetails.device_data.min_fuel_fillings = gpsDeviceWoxConstant.min_fuel_fillings;
        obdDeviceDetails.device_data.min_fuel_thefts = gpsDeviceWoxConstant.min_fuel_thefts;
        obdDeviceDetails.device_data.tail_length = gpsDeviceWoxConstant.tail_length;
        obdDeviceDetails.device_data.sim_number = assetData.gsmId;
        obdDeviceDetails.device_data.device_model = assetData.modelNo;
        obdDeviceDetails.device_data.pivot.group_id = gpsDeviceWoxConstant.group_id;
        obdDeviceDetails.device_data.pivot.timezone_id = gpsDeviceWoxConstant.timezone_id;
        obdDeviceDetails.save(function (err, result) {
            if (err) {
                console.log(err.stack)
            }
            else {
                findLocalOdbDeviceApiRefId(assetData.uniqueIdentifier,callBack)
            }
        })
    })





}


var updateObdDevice=function (assetData,callBack) {
    
    var url=routeConfig.assetsApiBaseUrl+'edit_device?lang='+routeConfig.lang+'&user_api_hash='+routeConfig.user_api_hash;
    var devObj = {};
    devObj.id=assetData.sensorData.apiRefId;
    devObj.name = assetData.sensorData.uniqueIdentifier;
    devObj.imei = assetData.sensorData.uniqueIdentifier;
    devObj.object_owner = gpsDeviceWoxConstant.object_owner;
    devObj.icon_id = gpsDeviceWoxConstant.icon_id;
    devObj.group_id = gpsDeviceWoxConstant.group_id;
    devObj.sim_number = assetData.sensorData.gsmId;
    devObj.device_model = assetData.sensorData.modelNo;
    devObj.fuel_measurement_id = gpsDeviceWoxConstant.fuel_measurement_id;
    devObj.fuel_quantity = gpsDeviceWoxConstant.fuel_quantity;
    devObj.min_moving_speed = gpsDeviceWoxConstant.min_moving_speed;
    devObj.min_fuel_fillings = gpsDeviceWoxConstant.min_fuel_fillings;
    devObj.min_fuel_thefts = gpsDeviceWoxConstant.min_fuel_thefts;
    devObj.tail_length = gpsDeviceWoxConstant.tail_length;
    devObj.timezone_id = gpsDeviceWoxConstant.timezone_id;
    sendEditRequest('post', url,devObj,callBack)

}
function sendPostResponse(method, url,requestBody,imeiNumber,callBack) {
    var options = {
        method: method,
        url: url,
        body: requestBody,
        json: true
    };

    request(options, function (error, response, body) {
        if (error){
            callBack(error);
            throw new Error(error);
        }
        else{
            findOdbDeviceApiRefId(imeiNumber,callBack)
        }
    });
}


function sendEditRequest(method, url,requestBody,callBack) {
    var options = {
        method: method,
        url: url,
        body: requestBody,
        json: true
    };

    request(options, function (error, response, body) {
        if (error) {
            callBack(error);
            throw new Error(error);
        }
        else{
            callBack(null);
            console.log(body)
        }
    });
}
function findLocalOdbDeviceApiRefId(imei,callBack){
    obdDeviceGPSWOXModel.find({},function(err,result){
        if(err)
            res.send(err)
        console.log("findLocalOdbDeviceApiRefIdfindLocalOdbDeviceApiRefId")
     /*  var resultStringArray="[" + result + "]"*/
      /*  var resultArray=JSON.parse("[" + result + "]");*/

        for (var l = 0; l < result.length; l++) {
            console.log(result[l].device_data.imei)
            console.log(result[l].device_data.imei)
            if (imei ==result[l].device_data.imei) {
                updateOdbDeviceApiRefId(result[l].id, imei,callBack)
            }else{
                callBack(new Error("NO Match Found In Database"));
            }
        }

    })

}
function updateOdbDeviceApiRefId(apiRefId,uniqueIdentifierNumber,callBack){
    console.log("**************result****result*********************")
    console.log(apiRefId)
    console.log(uniqueIdentifierNumber)

    SensorsModel.update({'sensorData.uniqueIdentifier': uniqueIdentifierNumber},{ $set: { "sensorData.apiRefId" :apiRefId} },function (err, result) {
        if (err) {
            console.log(err)
            callBack(err);
        } else {
            console.log(result)
            callBack(null);
        }
    })
}

/*
function findOdbDeviceApiRefId(imei,callBack){
    var options = {
        method: 'GET',
            url:routeConfig.assetsApiBaseUrl+'get_devices?lang='+routeConfig.lang+'&user_api_hash='+routeConfig.user_api_hash
    };
    request(options, function (error, response, result) {
        if (error){res.send(error) }
        else {
            console.log("IIIIIIIIIIIIIresultIIIIIIIIIIIIIIII")
            console.log( typeof result)
            var resultArray=JSON.parse("[" + result + "]");

            for (var l = 0; l < resultArray[0][0].items.length; l++) {

                if (imei ==resultArray[0][0].items[l].device_data.imei) {
                    updateOdbDeviceApiRefId(resultArray[0][0].items[l].id, imei,callBack)
                }else{
                    callBack(new Error("NO Match Found In Database"));
                }
            }

        }


    });





}*/


module.exports = {
    saveOdbAsset:saveOdbAsset,
    updateObdDevice:updateObdDevice
}