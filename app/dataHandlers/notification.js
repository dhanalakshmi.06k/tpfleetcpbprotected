/**
 * Created by Suhas on 7/7/2016.
 */

var notificationDataManger = require('../dataManagement').notification;

var handler = function(data){
    notificationDataManger.save(data);
    notificationDataManger.pushToSocket(data);
}
module.exports={
    handler:handler
}