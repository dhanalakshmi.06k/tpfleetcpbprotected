/**
 * Created by Suhas on 7/7/2016.
 */
var notification = require('./notification');
var rfidDetection = require('./rfIdDetection');
var beaconDetection = require('./beaconDetection');

module.exports={
    notificationHandler:notification,
    rfIdDetectionHandler:rfidDetection,
    beaconDetection:beaconDetection
}