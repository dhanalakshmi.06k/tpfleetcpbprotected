/**
 * Created by Suhas on 9/6/2016.
 */

var rfIdDetectionDataManger = require('../dataManagement').rfIdDetection;
var rfIdDetectionRawDataManger = require('../asset').rfIdDetectedRawDataManagement;

var handler = function(data){
        console.log(data)
        try{
                if(data){
                        rfIdDetectionRawDataManger.save(data);
                        var receivedData = data.toString();
                        var rfIdDetectionData = receivedData.split(" ");
                        rfIdDetectionDataManger.save(rfIdDetectionData)
                }else{
                        console.log("RfId data recieved is undefined")
                }
        }catch(e){
                console.warn(e.stack)
        }
}
module.exports={
        saveData:handler
}