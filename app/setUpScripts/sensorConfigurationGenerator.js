/**
 * Created by Suhas on 7/5/2016.
 */
var mongoose = require('mongoose'),
        config = require('../../config/config');
var sensorConfigModel = require('../models/sensorConfiguration');
var argv = process.argv;
if(argv.indexOf('-force')>=0){
    removePreviousData();
}
var fs = require('fs');
var path  =require('path');
var dirPath = fs.realpathSync(__dirname);
var count=0;
//mongoose.connect(config.db);
var sensorConfiguration = require('../data/sensorConfiguration');






function generateSensorConfiguration(callBack){
    /*var sensorConfigJsonPath = path.join(dirPath, "../","data/sensorConfiguration.js")
    var sensorConfiguration = JSON.parse(fs.readFileSync(sensorConfigJsonPath));*/
    for(var i=0;i<sensorConfiguration.configDetails.length;i++){
        var sensorConfigJsonObj = sensorConfiguration.configDetails[i];
        var sensorConfigObj = new sensorConfigModel();
        sensorConfigObj.sensorConfiguration={};
        sensorConfigObj.sensorType=sensorConfigJsonObj.sensorType;
        sensorConfigObj.sensorConfiguration=sensorConfigJsonObj.sensorConfiguration;
        sensorConfigObj.save(function(err,reult){
            if(err){
                console.log(err)
            }else{
                console.log("Data Added For SensorConfiguration")
                count+=1;
                stopExecution(sensorConfiguration,callBack);
            }

        })
    }

};
function stopExecution(arrayLength,callBack){
    if(count==arrayLength.length){
        console.log(" Sensors Configurations of "+ count +" Sensors saved Successfully");
        callBack();
    }
}
function removePreviousData(callBack){
    console.log("Deleting previous Data")
    sensorConfigModel.remove({},function(err,result){
        console.log("Deleting previous Data")
        generateSensorConfiguration(callBack);
    })
}

module.exports ={
    run:removePreviousData
}

