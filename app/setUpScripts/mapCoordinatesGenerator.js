/**
 * Created by zendynamix on 7/11/2016.
 */

var mongoose = require('mongoose'),
    config = require('../../config/config');
SettingsModel = require('../models/mapSettingsModel');
Schema = mongoose.Schema;
//mongoose.connect(config.db);

SettingsModel.remove({}, function(err) {
    console.log("previous data removed")
    //generateMapCoordinates();
});
function generateMapCoordinates(callBack){
    var latitude= 37.442231;
    var longitude=-122.152674;
    var configObj = new SettingsModel();
    configObj.SettingsConfiguration.id=1;
    configObj.SettingsConfiguration.mapSettings.mapCenter.latitude = latitude;
    configObj.SettingsConfiguration.mapSettings.mapCenter.longitude = longitude;
    configObj.SettingsConfiguration.uiThemeName="default"
    configObj.SettingsConfiguration.uiStyleName="default" //arabicUIStyle
    configObj.save(function(res,err) {
        console.log("configObj added!!");
        callBack();
    });

};
module.exports = {
    run: generateMapCoordinates
}