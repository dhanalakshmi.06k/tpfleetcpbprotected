/**
 * Created by dhanalakshmi on 29/8/16.
 */


var mongoose = require('mongoose'),
    config = require('../../config/config');
mapPolygonDetailsModel = require('../models/mapPolygonDetailsModel');
Schema = mongoose.Schema;
fs = require('fs');
//mongoose.connect(config.db);

mapPolygonDetailsModel.remove({}, function(err) {
    console.log("previous data removed")
    //generateMapCoordinates();
})
var polygonData = {
    "parkingLot":{
        "polygon":{
            "coOrdinates":[
                [ 25.277384916082053, 55.41901946067811],
                [25.27942222709375, 55.41605293750764],
                [ 25.279950952217398, 55.4164659976959],
                [25.27904387192114, 55.41941642761231],
                [ 25.277952456158587, 55.41972756385804]

            ],
            "lineColor":"black",
            "fillColor":"#ac7339",
            "fillOpacity":0.3

        },
        "mapCenterCoOrdinates":{
            "lat":25.27852484431615,
            "lng": 55.41843473911286,
            "zoomLevel":18
        }
    },
    "Inspection":{
        "polygon":{
            "coOrdinates":[
                [25.277064764098515, 55.415382385253906],
                [25.277307303557578, 55.41510879993439],
                [25.277642007214865, 55.41541457176209],
                [25.277389766863195, 55.41569888591767]

            ],
            "lineColor":"black",
            "fillColor":"green",
            "fillOpacity":0.3

        },
        "mapCenterCoOrdinates":{
            "lat":25.27728304963347,
            "lng":  55.41540384292603,
            "zoomLevel":18
        }
    },
    "Maintenance":{
        "polygon":{
            "coOrdinates":[
                [ 25.27597333053333, 55.41563987731934],
                [25.276419606777512, 55.4150390625],
                [ 25.277239392557906, 55.41589200496674],
                [ 25.276885284586722, 55.41647136211396]

            ],
            "lineColor":"black",
            "fillColor":"green",
            "fillOpacity":0.3

        },
        "mapCenterCoOrdinates":{
            "lat":25.275478544083075,
            "lng":  55.41788220405579,
            "zoomLevel":17.3
        }
    },
    "Fuelling":{
        "polygon":{
            "coOrdinates":[
                [ 25.27752073788046, 55.41441142559052],
                [ 25.278233799827714, 55.415081977844245],
                [25.27788939628119, 55.41555941104889],
                [25.2771617799403, 55.41491568088532]

            ],
            "lineColor":"black",
            "fillColor":"green",
            "fillOpacity":0.3

        },
        "mapCenterCoOrdinates":{
            "lat":25.277709917989178,
            "lng":  55.41498005390168,
            "zoomLevel":18
        }
    },
    "CarWash":{
        "polygon":{
            "coOrdinates":[
                [  25.276977449774588, 55.4147493839264],
                [ 25.277307303557578, 55.414314866065986],
                [ 25.277394617644134, 55.41440606117249],
                [    25.277074465686194, 55.414813756942756]

            ],
            "lineColor":"black",
            "fillColor":"#ffff80",
            "fillOpacity":0.3

        },
        "mapCenterCoOrdinates":{
            "lat":25.277176332309885,
            "lng":  55.414588451385505,
            "zoomLevel":18
        }
    },
    "MeterCash":{
        "polygon":{
            "coOrdinates":[
                [ 25.278238650574913, 55.41504979133606],
                [ 25.277554693306318, 55.41441142559052],
                [25.277632305672558, 55.41441142559052],
                [25.277671111837087, 55.41443288326264],
                [25.278253202815314, 55.41496396064759],
                [25.27824350132191, 55.41504442691803]

            ],
            "lineColor":"black",
            "fillColor":"#ffff80",
            "fillOpacity":0.3

        },
        "mapCenterCoOrdinates":{
            "lat":25.277874843997125,
            "lng":  55.414695739746094,
            "zoomLevel":18
        }
    }

}



function generatePolygonDetails(callBack){
    var configObj = new mapPolygonDetailsModel();
  /*  fs.readFile(__dirName+'../../config/polygonZonePlots.json', function (err, data) {*/
        /*if (err) throw err;*/
        configObj.polygonDetailsSchemaModelConfiguration = polygonData;
        console.log("**************dd")
        console.log(configObj)
        configObj.save(function(res,err) {
            console.log("mapPolygonDetailsModel added sucessfully");
            callBack();
        });

  /*  });*/



};
module.exports = {
    run: generatePolygonDetails
}
