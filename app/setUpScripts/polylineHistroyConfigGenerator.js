/**
 * Created by zendynamix on 25/8/16.
 */

var mongoose = require('mongoose'),
    config = require('../../config/config');
historyDateModel = require('../models/mapPolylIneHistoryModel');
Schema = mongoose.Schema;


historyDateModel.remove({}, function(err) {
    console.log("previous data removed")
    //generateDefaultDate();
});



function generateDefaultDate(callBack){
    var dateObj = new historyDateModel();
    dateObj.dateConfiguration.id=1;
    dateObj.dateConfiguration.fromDate=new Date();
    dateObj.dateConfiguration.fromTime=12;
    dateObj.dateConfiguration.toDate=new Date();
    dateObj.dateConfiguration.toTime=13;
    dateObj.save(function(res,err) {
        console.log("configObj added sucessfully");
        callBack();
    });

};
module.exports = {
    run: generateDefaultDate
}
