/**
 * Created by Suhas on 8/18/2016.
 */

var assetsLinkConfigModel = require('../models/assetsLinkConfiguration');
var assetLinkConfigMetaData = [
        {'assetType':"car",'assetsTypeLinkable':['obdDevice','rfIdTag']},
        {'assetType':"obdDevice",'assetsTypeLinkable':['gsmSim']},
        {'assetType':"rfIdTag",'assetsTypeLinkable':[]},
        {'assetType':"driver",'assetsTypeLinkable':['rfIdTag','car']},
        {'assetType':"gsmSim",'assetsTypeLinkable':['']},
    {'assetType':"truck",'assetsTypeLinkable':['obdDevice','rfIdTag','Air fuel ratio meter']},
    {'assetType':"scooter",'assetsTypeLinkable':['rfIdTag','Air fuel ratio meter']}
]
function generateAssetsLinkConfig(callBack){
        var count = 0;
        for(var i=0;i<assetLinkConfigMetaData.length;i++){
                var sensorConfigJsonObj = assetLinkConfigMetaData[i];
                var sensorConfigObj = new assetsLinkConfigModel();
                sensorConfigObj.assetType=sensorConfigJsonObj.assetType;
                sensorConfigObj.assetsTypeLinkable=sensorConfigJsonObj.assetsTypeLinkable;
                sensorConfigObj.save(function(err,reult){
                        if(err){
                                console.log(err)
                        }else{
                                console.log("Data Added For Assets link Configuration")
                                count+=1;
                                if(assetLinkConfigMetaData.length==count){
                                        stopExecution(assetLinkConfigMetaData.length,callBack);
                                }
                        }

                })
        }

};
function stopExecution(arrayLength,callBack){
       /* if(count==arrayLength){*/
                console.log(" Assets link Configuration of "+ arrayLength +" Asset type saved Successfully");
                callBack();

       /* }*/
}
function removePreviousData(callBack){
        console.log("Deleting previous Data")
        assetsLinkConfigModel.remove({},function(err,result){
                console.log("Deleting previous Data")
                generateAssetsLinkConfig(callBack);
        })
}

module.exports ={
        run:removePreviousData
}