/**
 * Created by Suhas on 9/6/2016.
 */
var express = require('express'),
        router = express.Router(),
        config = require('../../config/config');
var moment = require('moment');
var rfIdDetectionModel = require('../models/rfIdDetection');

var rfIDetectionAssetManagement  = require('../asset').rfIdDetectedAssetManagement;
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt
var secret = 'this is the secrete password';
var jsonToCSV = require('json-to-csv');
var jsoncsv = require('json-csv')
module.exports = function (app) {
        app.use('/', router);
};


router.use('/api', expressJwt({secret: secret}));
router.use(function(err, req, res, next){
        if (err.constructor.name === 'UnauthorizedError') {
                res.status(401).send('Unauthorized user please login via the application');
        }
});


router.get('/api/rfIdDetection/totalCount',function(req,res){
        rfIDetectionAssetManagement.getTotalCount()
                .then(function(totalCount){
                        res.send(totalCount)
                })
})
router.get('/api/rfIdDetection/:skip/:limit', function (req, res) {
        rfIDetectionAssetManagement.getDataBySkipAndLimit(req.params.skip,req.params.limit)
                .then(function(data){
                        res.send(data)
                })
})
router.get('/api/rfIdDetection/totalCountByDate/date/:date',function(req,res){
        rfIDetectionAssetManagement.rfIdDetectionDataCountByDate(new Date(parseInt(req.params.date)))
                .then(function(totalCount){
                        res.send(totalCount)
                })
})
router.get('/api/rfIdDetectionByDate/:date/:skip/:limit', function (req, res) {
        console.log("Date")
        console.log(parseInt(req.params.date))
        rfIDetectionAssetManagement.rfIdDetectionDataByDate(parseInt(req.params.date),req.params.skip,req.params.limit)
                .then(function(data){
                        res.send(data)
                })
})
router.get('/api/rfIdDetection/totalCountByDate/analytics/:fromDate/:toDate/:fromTime/:toTime',function(req,res){

        rfIDetectionAssetManagement.rfIdHistoryDetectionDataCountByDate(req.params.fromDate,req.params.toDate,req.params.fromTime,req.params.toTime)
            .then(function(totalCount){
                    res.send(totalCount)
            })
})


router.get('/rfIdHistoryDetectionByDateAll/:utcFromDate/:utcToDate', function (req, res) {

    res.set('Content-Type', 'application/octet-stream');
    res.set('Content-Disposition', 'disp-ext-type; filename=historicalData.json;creation-date-parm:'+new Date());

    res.write("[")
    var stream = rfIdDetectionModel.find({created_at: { $gt: req.params.utcToDate, $lt: req.params.utcFromDate }}).stream();
    stream.on('data', function (doc) {
        res.write(JSON.stringify(doc)+',')
    })
    stream.on('error', function (err) {
        console.log(err.stack)
    })
    stream.on('close', function (doc) {
        console.log("done")
        res.write('{}]')
        res.end();
    })

})

/*router.get('/rfIdHistoryDetectionByDateRange/:utcFromDate/:utcToDate', function (req, res) {
console.log(moment(parseInt(req.params.utcFromDate)))
    console.log(moment(parseInt(req.params.utcToDate)))
    res.set('Content-Type', 'application/octet-stream');
    res.set('Content-Disposition', 'disp-ext-type; filename=historicalData.json;creation-date-parm:'+new Date());

    res.write("[")
    var stream = rfIdDetectionModel.find({'created_at': { $gt: moment(parseInt(req.params.utcFromDate)), $lt: moment(parseInt(req.params.utcToDate))}}).stream();
    stream.on('data', function (doc) {
        res.write(JSON.stringify(doc)+',')
    })
    stream.on('error', function (err) {
        console.log(err.stack)
    })
    stream.on('close', function (doc) {
        console.log("done")
        res.write('{}]')
        res.end();
    })

})*/

router.get('/rfIdHistoryDetectionByDateRange/:utcFromDate/:utcToDate', function (req, res) {
    res.set('Content-Type', 'application/octet-stream');
    res.set('Content-Disposition', 'disp-ext-type; filename=carDetectionReport'+new Date()+'.csv;creation-date-parm:'+new Date());
    var stream = require('stream');
    var arrayStream = new stream.Transform({objectMode: true});
    arrayStream._hasWritten = false;
    arrayStream._transform = function (chunk, encoding, callback) {
        if (!this._hasWritten) {
            this._hasWritten = true;
            this.push('[' + JSON.stringify(chunk));

        } else {
            this.push(',' + JSON.stringify(chunk));
        }
        callback();
    };
    arrayStream._flush = function (callback) {
        this.push(']');
        callback();

    };
    var options = {
        fields : [
        {
            name : 'carLinked.carNo',
            label : 'carNo'
        },
        {
            name : 'created_at',
            label : 'entryTime'
        },
        {
            name : 'updated_at',
            label : 'exitTime'
        },
        {
            name : 'direction',
            label : 'direction'
        }
    ]
    }
    rfIdDetectionModel
        .find({'created_at': { $gt: moment(parseInt(req.params.utcFromDate)), $lt: moment(parseInt(req.params.utcToDate))}})
        .stream()
        .pipe(jsoncsv.csv(options))/*
        .pipe(zlib.createGzip())*/
        .pipe(res);
});


router.get('/rfIdHistoryDetectionByDateRangeTable/:utcFromDate/:utcToDate', function (req, res) {
/*    rfIdDetectionModel.find({created_at: { $gt: moment(parseInt(req.params.utcFromDate)), $lt: moment(parseInt(req.params.utcToDate))}},{"_id":0},function(err,result){
        if(err){
            res.send(err.stack)
        }else{
            res.send(result)
        }
    })*/
        console.log("Date")
        console.log(parseInt(req.params.utcFromDate))
        var start  = parseInt(req.params.utcFromDate);
        var end  = parseInt(req.params.utcToDate)
        rfIDetectionAssetManagement.rfIdDetectionAllDataByDateRangeWithDwellingTime(start,end)
        .then(function(data){
                res.send(data)
        }).
        catch(function(err){
                res.status(406).send(err.stack)
        })
 })