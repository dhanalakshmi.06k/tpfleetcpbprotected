/**
 * Created by Suhas on 4/4/2016.
 */
var express = require('express'),
        router = express.Router(),
        mongoose = require('mongoose'),
        config=require('../../config/config'),
        SettingsModel = mongoose.model('settingModel');
        appSensorMappingModel= mongoose.model('sensorAppMapper'),
            busEntryExitModel = mongoose.model('busEntryExitInfo'),
            emitSocketIo = require('../../config/socketIo');

        module.exports = function (app) {
        app.use('/', router);
        };
router.get('/necDashBoard/settings/getSocketUrl',function(req,res){
        var url = config['socketUrl'];
        res.send(url);
})





router.post('/appMapperObj', function(req, res, next) {
    var mapperObj = new appSensorMappingModel();
    //camera
    mapperObj.sensorType="camera";
    mapperObj.save(function(err,result){
        if(err){
            console.log(err)
        }
        else{
            console.log("mapperObj generated")
            res.send("mapperObj generated")
        }

    })


});
router.get('/appSensorMappingConfig',function(req,res){
    appSensorMappingModel.find({},{"_id":0},function(err,result){
        if(err){
            res.send(err.stack)
        }else{
            res.send(result)
        }
    })
})
router.get('/appSettings',function(req,res){
    SettingsModel.findOne({},function(err,result){
        if(err){
            res.send(err.stack)
        }else{
            res.send(result)
        }
    })
})


var taxiEntryExitDataSimulatorId=0;
router.get('/busEntryExitData/startDataPush', function (req, res) {
    if(taxiEntryExitDataSimulatorId==0){
        triggerTaxiEntryExitDataPusher();
        res.send("started")
    }else{
        res.send("Push Already Started No Need To Start Again")
    }
})
router.get('/busEntryExitData/stopDataPush', function (req, res) {
    clearInterval(taxiEntryExitDataSimulatorId);
    taxiEntryExitDataSimulatorId=0;
    res.send("Bus Entry Exit Data Pusher Stopped")
})
function triggerTaxiEntryExitDataPusher(){
    taxiEntryExitDataSimulatorId = setInterval(function(){
        taxiEntryExitDataPusher();
        console.log("Started Bus")
    },10000)
    console.log("started busEntryExit Data Pusher");
}
function taxiEntryExitDataPusher(){
    busEntryExitModel.findOneRandom(function(err,result){
        if(err){
            console.log(err)
        }
        if(result){
            console.log("data pusher bus entry exit")
            console.log(result)
            emitSocketIo.getSocketIoServer().emit('taxiEntryExitData',result);
        }else{
            console.log("No Data In Database (BEE)");
        }
    })
}




