/**
 * Created by Suhas on 8/24/2016.
 */

var express = require('express'),
        router = express.Router(),
        mongoose = require('mongoose'),
        config = require('../../config/config'),
        SensorsModel = mongoose.model('sensors');
var constants = require('../../config/constants').constants;
var sensorsConfiguration =  mongoose.model('sensorsConfiguration');
var sensorNotificationPusher = require('../dataManagement/notification').pushToSocket;
var userDetails = require('../data/loginUserDetails');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt
var secret = 'this is the secrete password';
module.exports = function (app) {
        app.use('/', router);
};

var assetsDataManagementModule = require('../asset');

router.use('/api', expressJwt({secret: secret}));
router.use(function(err, req, res, next){
        if (err.constructor.name === 'UnauthorizedError') {
                res.status(401).send('Unauthorized user please login via the application');
        }
});



router.post('/api/linkAssetsNew',function(req, res){
        var assetDetails = req.body;
        assetsDataManagementModule.linking.linkingAssets(assetDetails.assetOne,assetDetails.assetTwo)
                .then(function(){
                        res.send('linked')
                })
                .catch(function(err){
                        res.send(err.stack)
                })
});
router.post('/api/deLinkAssetsNew',function(req, res){
        var assetDetails = req.body;
        assetsDataManagementModule.deLinking.deLinkingAssets(assetDetails.assetOne,assetDetails.assetTwo)
                .then(function(){
                        res.send('de-linked')
                })
                .catch(function(err){
                        res.send(err.stack)
                })
});
router.get('/api/linkedAssets/:skip/:limit/:assetType', function (req, res) {
        SensorsModel.find({'sensorData.sensorType':req.params.assetType,'sensorData.isLinked':true})
                .sort({"updated_at":-1})
                .skip(parseInt(req.params.skip))
                .limit(parseInt(req.params.limit))
                .populate('sensorData.assetsLinked.ref')
                .exec(function(err,result){
                        /* res.send(result);*/
                        getAssetsLinkedListInRequiredForm(result)
                                .then(function(data){
                                        res.send(data);
                                })
                })
});
router.get('/api/linkedAssetsCounts/:assetType', function (req, res) {
        SensorsModel.count({'sensorData.sensorType':req.params.assetType,"sensorData.isLinked":true}, function (err, count) {
                if (err) {
                        console.log(err.stack)
                } else {
                        var count ={
                                count:count
                        }
                        res.send(count)
                }
        });
})


function getAssetsLinkedListInRequiredForm(assetsLinkedDetailsArray){
        return new Promise(function(resolve,reject){
                if(assetsLinkedDetailsArray && assetsLinkedDetailsArray.length>0){
                        var linkedAssetsArray = new Array();
                        for(var i=0;i<assetsLinkedDetailsArray.length;i++){
                                var arrObj = assetsLinkedDetailsArray[i];
                                if(arrObj.sensorData.assetsLinked && arrObj.sensorData.assetsLinked.length>0){
                                        var assetsLinkedArray = arrObj.sensorData.assetsLinked;
                                        var assetsLinkedObj= {};
                                        for(var j=0;j<assetsLinkedArray.length;j++){
                                                var assetLinkedObjDetails=assetsLinkedArray[j];
                                                if(!assetsLinkedObj[assetLinkedObjDetails.type]){
                                                        assetsLinkedObj[assetLinkedObjDetails.type]=[]
                                                }
                                                var obj =assetLinkedObjDetails.ref;
                                                assetsLinkedObj[assetLinkedObjDetails.type].push(obj);
                                                if(j == assetsLinkedArray.length-1){
                                                        arrObj.sensorData.linkedAssetsDetails = assetsLinkedObj;
                                                        var obj = new Object(arrObj);
                                                        delete  arrObj.sensorData.assetsLinked
                                                        linkedAssetsArray.push(obj)
                                                }
                                                if(i==assetsLinkedDetailsArray.length-1){
                                                        if(!assetsLinkedArray || j == assetsLinkedArray.length-1 ){
                                                                resolve(linkedAssetsArray);
                                                        }
                                                }
                                        }
                                }
                        }
                }else{
                        resolve(new Array());
                }
        })


}