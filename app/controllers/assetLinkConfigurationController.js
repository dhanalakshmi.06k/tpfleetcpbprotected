/**
 * Created by Suhas on 8/24/2016.
 */
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var config = require('../../config/config');
var SensorsModel = mongoose.model('sensors');
var AssetLinkConfigurationModel = mongoose.model('assetsLinkConfigurationModel');
var constants = require('../../config/constants').constants;
var sensorsConfiguration =  mongoose.model('sensorsConfiguration');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt
var secret = 'this is the secrete password';
module.exports = function (app) {
        app.use('/', router);
};


router.use('/api', expressJwt({secret: secret}));


router.get('/api/assetsLinkConfig/type/:type', function (req, res) {
        var assetType = req.params.type;
        if(assetType && assetType != 'undefined') {
          AssetLinkConfigurationModel.findOne({'assetType': req.params.type}, function (err, result) {
            generateAssetsLinkConfig(req.params.type, result.assetsTypeLinkable)
              .then(function (data) {
                res.send(data)
              })
          })
        }else{
                res.send(null);
        }
});

function generateAssetsLinkConfig(rootAssetType,data){

        return new Promise(function(resolve,reject){
                var assetsToBeLinkedArray = data;
                var assetsToBeLinked = [];
                for(var i=0;i<assetsToBeLinkedArray.length;i++){
                        var assetType = assetsToBeLinkedArray[i];
                        var obj = {
                                label:constants.ASSET_DETAILS.typeDetails[assetType].label,
                                type:assetType,
                                index:i+1,
                                data:'',
                                id:'',
                                new:true,
                                isLinked:false
                        }
                        assetsToBeLinked.push(obj)
                        if(i==assetsToBeLinkedArray.length-1){
                                var obj1 = {
                                        label:constants.ASSET_DETAILS.typeDetails[rootAssetType].label,
                                        type:rootAssetType,
                                        index:0,
                                        data:'',
                                        id:'',
                                        new:true,
                                        isLinked:false
                                }
                                assetsToBeLinked.unshift(obj1)
                            console.log("****************generateAssetsLinkConfig********************")
                            console.log(assetsToBeLinked)
                            console.log("****************generateAssetsLinkConfig********************")
                                resolve(assetsToBeLinked)
                        }
                }
        })

}