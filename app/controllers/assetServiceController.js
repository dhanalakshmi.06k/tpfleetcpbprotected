/**
 * Created by zendynamix on 8/11/2016.
 */

var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    config = require('../../config/config'),
    routeConfig = require('../../config/assetServiceConfig'),
    request = require('request'),
    jwt    = require('jsonwebtoken'),
    expressJwt = require('express-jwt'),
     secret = 'this is the secrete password';
     routeConfig = require('../../config/assetServiceConfig');
    module.exports = function (app) {
        app.use('/', router);
    };
var obdDeviceGPSWOXModel = require('../models/gpswoxobddeviceModel');

/*router.use('/sensorServiceApi', expressJwt({secret: secret}));
router.use(function(err, req, res, next){
    if (err.constructor.name === 'UnauthorizedError') {
        res.status(401).send('Unauthorized user please login via the application');
    }
});*/

/*router.get('/sensorServiceApi/!*', function (req, res) {
    var apiUrl=req.url.substring(18,req.url.length);
    var options = {
        method: 'GET',
        url:routeConfig.assetsApiBaseUrl+apiUrl
    };
    request(options, function (error, response, body) {
        if (error){res.send(error) }
        else {
            res.send(body);
        }


    });




})
router.post('/sensorServiceApi/!*', function (req, res,body) {
    var apiUrl=req.url.substring(18,req.url.length);
    console.log("*****************req.body*********************************")
    console.log(req.body)
    console.log("********************req.body******************************")
        var options = {
            method: 'POST',
            url: routeConfig.assetsApiBaseUrl+apiUrl,
            body: req.body,
            json: true
        };

        request(options, function (error, response, body) {
            if (error){
                res.send(error)
            } else{

                res.send(body)
            }

        })


})*/


router.get('/getAllDevices', function (req, res) {
    obdDeviceGPSWOXModel.find({},function(err,result){
        res.send(result);
    })
    });

router.get('/getDeviceHistoryById/:fromDate/:toDate/:fromTime/:toTime/:deviceId', function (req, res) {
    var url=routeConfig.assetsApiBaseUrl+'get_history?lang='+routeConfig.lang+'&user_api_hash='+routeConfig.user_api_hash+'&device_id='+req.params.deviceId+'&from_date='+req.params.fromDate+'&from_time='+req.params.fromTime+'&to_date='+req.params.toDate+'&to_time='+req.params.toTime+'&snap_to_road=false';
    sendResponse('get', url, res)
});
router.post('/addDevice', function (req, res) {
    var url=routeConfig.assetsApiBaseUrl+'add_device?lang='+routeConfig.lang+'&user_api_hash='+routeConfig.user_api_hash;
    sendPostResponse('post', url, res,req.body)
});

router.get('/deleteDevice/:deviceApiRefId', function (req, res) {
    var url=routeConfig.assetsApiBaseUrl+'destroy_device?lang='+routeConfig.lang+'&user_api_hash='+routeConfig.user_api_hash+'&device_id='+req.params.deviceApiRefId;
    sendResponse('get', url, res)
});



function sendPostResponse(method, url, res,requestBody) {
    var options = {
        method: method,
        url: url,
        body: requestBody,
        json: true
    };
    request(options, function (error, response, body) {
        if (error) throw new Error(error);
        res.send(body);
    });
}

function sendResponse(method, url, res) {
    var options = {
        method: method,
        url: url
    };
    request(options, function (error, response, body) {
        if (error) throw new Error(error);
        res.send(body);
    });
}


