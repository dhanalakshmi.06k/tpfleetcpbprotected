/**
 * Created by Suhas on 6/18/2016.
 */
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    config = require('../../config/config'),
    SensorsModel = mongoose.model('sensors');


var assetManagementModule  = require('../asset');

var constants = require('../../config/constants').constants;
var sensorsConfiguration =  mongoose.model('sensorsConfiguration');
var sensorNotificationPusher = require('../dataManagement/notification').pushToSocket;
var userDetails = require('../data/loginUserDetails');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt
var secret = 'this is the secrete password';
module.exports = function (app) {
    app.use('/', router);
};


router.use('/api', expressJwt({secret: secret}));
router.use(function(err, req, res, next){
    if (err.constructor.name === 'UnauthorizedError') {
        res.status(401).send('Unauthorized user please login via the application');
    }
});
router.get('/printSensors', function (req, res) {
    SensorsModel.find({}, function (err, result) {
        if (err) {

        } else {
            res.send(result)
        }
    })
})

router.get('/api/sensors', function (req, res) {
    SensorsModel.find({}, function (err, result) {
        if (err) {

        } else {
            res.send(result)
        }
    })
})
router.post('/api/sensors', function (req, res) {
    assetManagementModule.assetDataManagement.save(req.body)
    .then(function(data){
        res.send(data);
    })
    .catch(function(err){
        res.status(406).send(err);
    });

})
router.put('/api/sensors', function (req, res) {
    var assetObj = req.body;
    updateAssetDetails(assetObj)
    .then(function(updatedAssetObj){
        res.send(updatedAssetObj)
    })


})
router.get('/api/sensors/:id', function (req, res) {
    SensorsModel.findOne({'_id': req.params.id}, function (err, result) {
        if (err) {

        } else {
            res.send(result)
        }
    })
})


router.delete('/api/sensors/:id', function (req, res) {
/*    SensorsModel.findOne({'_id': req.params.id},function(errr,response){
        if(errr){
            console.log(errr.stack)
        }else{
            SensorsModel.remove({'_id': req.params.id}, function (err, result) {
                if (err) {

                } else {
                    var deleteMsg={
                        "level":"WARNING",
                        "message": "Asset Configuration '"+response.sensorData.assetName+"'  Deleted",
                        "timeStamp":new Date(),
                        "notificationType":"Sensor"
                    }
                    pushSensorNotification(deleteMsg);
                    res.send(result)
                }
            })
        }
    })*/
    assetManagementModule.assetDataManagement.remove(req.params.id)
    .then(function(response){
        res.send(response)
    })
    .catch(function(err){
        console.log(err)
        res.status(406).send(err);
    })
})
router.get('/api/sensorsCount', function (req, res) {
    SensorsModel.count({}, function (err,sensorCount) {
        if (err) {
            res.send(err.stack)
        }
        else {
            var count = {
                count:sensorCount
            }
            res.send(count)
        }
    })

})

router.get('/api/sensorsDetails/instanceDataMapper/:type/:sensorName',function(req,res){
    sensorsConfiguration.findOne({"sensorConfiguration.sensorType.typeId":req.params.type},function (err,sensorConfigurationResponse) {
        if (err) {
            res.send(err.stack)
        }
        else {
            var sensorConfig = sensorConfigurationResponse;
            SensorsModel.findOne({"sensorData.sensorName":req.params.sensorName}, function (err, sensorDataResponse) {
                if (err) {
                    res.send(err.stack)
                }
                else {
                    var count=0;
                    var instanceDataMapper={}
                    var sensorDetail = sensorDataResponse;
                    if(sensorDataResponse && sensorConfig.sensorConfiguration.instanceDataMapper){
                        var instanceDataMapperObj= sensorConfig.sensorConfiguration.instanceDataMapper;
                        var k = Object.keys(instanceDataMapperObj);
                        k.forEach(function (objKey, index) {
                            var objPath = instanceDataMapperObj[objKey].split(".");
                            instanceDataMapper[objKey]=sensorDetail[objPath[0]][objPath[1]];
                            count++;
                            if(k.length==count){
                                res.send(instanceDataMapper)
                            }

                        });
                    }else{
                        res.send([])
                    }
                }
            })
        }
    })

})
router.get('/api/sensorsDetails/:type', function (req, res) {
    SensorsModel.aggregate(
        [
         {$match: {"sensorData.sensorType":req.params.type}},
         {$project: {"_id":0,"sensorData.sensorType":1,"sensorData.sensorName":1}},
         {$project: {"sensorType":"$sensorData.sensorType","sensorName":"$sensorData.sensorName"}}
        ]
        ,function (err, sensorDataResponse) {
            if (err) {
                res.send(err.stack)
            }
            else {
                res.send(sensorDataResponse)
            }
        }
    )
})

function dss(){
}
router.get('/api/sensorsDetails/instanceDataMapper/:type/:sensorName/:fieldName',function(req,res){
    sensorsConfiguration.findOne({"sensorConfiguration.sensorType.typeId":req.params.type},function (err,sensorConfigurationResponse) {
        if (err) {
            res.send(err.stack)
        }
        else {
            var sensorConfig = sensorConfigurationResponse;
            SensorsModel.findOne({"sensorData.sensorName":req.params.sensorName}, function (err, sensorDataResponse) {
                if (err) {
                    res.send(err.stack)
                }
                else {
                    var instanceDataMapper={}
                    var sensorDetail = sensorDataResponse;
                    if(sensorDataResponse && sensorConfig.sensorConfiguration.instanceDataMapper){
                        var instanceDataMapperObj= sensorConfig.sensorConfiguration.instanceDataMapper;
                        if(instanceDataMapperObj[req.params.fieldName]){
                            var objPath = instanceDataMapperObj[req.params.fieldName].split(".");
                            instanceDataMapper[req.params.fieldName]=sensorDetail[objPath[0]][objPath[1]];
                            res.send(instanceDataMapper)

                        }else{
                            res.send({})
                        }
                    }else{
                        res.send([])
                    }
                }
            })
        }
    })

})
router.get('/api/sensorsDetails/isUnique/:sensorName',function(req,res){
    SensorsModel.find({"sensorData.sensorName":req.params.sensorName},function (err,result) {
        if (err) {
            res.send(err.stack)
        }
        else {
            if(result.length>0){
                res.send(false)
            }else{
                res.send(true)
            }
        }
    })

})
function pushSensorNotification(data){
    var userName =userDetails.getUserNameOfLoginUser();
    if(userName!=''){
        data.message = data.message+" By "+userName;
    }
    sensorNotificationPusher(data)
}
/*function notificationSocketTest(){
    setInterval(function(){
        var testMsg={
            "level":"WARNING",
            "message": "Sensor Configuration 'Test11'  Deleted",
            "timeStamp":new Date(),
            "notificationType":"Sensor"
        }
        pushSensorNotification(testMsg)
    },2000)
}*/


/*For Pagination*/
router.get('/api/sensors/:skip/:limit/:type', function (req, res) {
    SensorsModel.find({"sensorData.sensorType":req.params.type}, function (err, result) {
        if (err) {

        } else {
            res.send(result)
        }
    }).sort({"updated_at":-1}).skip(parseInt(req.params.skip)).limit(parseInt(req.params.limit));
});
router.get('/api/sensorsCount/:type', function (req, res) {
    SensorsModel.count({"sensorData.sensorType":req.params.type}, function (err, count) {
        if (err) {
            console.log(err.stack)
        } else {
            var count ={
                count:count
            }
            res.send(count)
        }
    });
})
router.get('/api/sensors/:skip/:limit', function (req, res) {
    SensorsModel.find({}, function (err, result) {
        if (err) {

        } else {
            res.send(result)
        }
    }).sort({"_id":-1}).skip(parseInt(req.params.skip)).limit(parseInt(req.params.limit));
});



router.get('/api/sensorsData/:type', function (req, res) {
    SensorsModel.find({"sensorData.sensorType":req.params.type}, function (err,result) {
        if (err) {
            console.log(err.stack)
        } else {
            res.send(result)
        }
    });
})




function updateAssetDetails(assetDetails){
    return new Promise(function(resolve,reject){
       /* SensorsModel.findOneAndUpdate({_id: assetDetails._id},assetDetails, function (err, response){
            if (err) {
                reject(err.stack)
            }
            else {
                var updateMsg={
                    "level":"INFO",
                    "message": "Asset Configuration "+assetDetails.sensorData.assetName+" Updated",
                    "timeStamp":new Date(),
                    "notificationType":"Asset"
                }
                pushSensorNotification(updateMsg);
                resolve(assetDetails)
            }
        })*/
        assetManagementModule.assetDataManagement.update(assetDetails)
        .then(function(result){
            resolve(result)
        })
        .catch(function(err){
            reject(err);
        })
    })
}



router.get('/getSensorByUniqueIdentifierNumber/:uniqueIdentifierNumber', function (req, res) {
    SensorsModel.findOne({'sensorData.uniqueIdentifier': req.params.uniqueIdentifierNumber}, function (err, result) {
        if (err) {

        } else {
            res.send(result)
        }
    })
})
router.get('/updatesensorDeviceBYuniqueIdentifierNumber/:uniqueIdentifierNumber/:apiRefId', function (req, res) {
    SensorsModel.update({'sensorData.uniqueIdentifier': req.params.uniqueIdentifierNumber},{ $set: { "sensorData.apiRefId" : req.params.apiRefId} },function (err, result) {
        if (err) {
            console.log("******************")
            console.log(err)
        } else {
            console.log(result)
            res.send(result)
        }
    })

})


router.get('/findSensorDeviceBYUniqueIdentifierNumber/:uniqueId', function (req, res) {
    SensorsModel.findOne({'sensorData.uniqueIdentifier': req.params.uniqueId}, function (err, result) {
        if (err) {
            console.log("******************")
            console.log(err)
        } else {
            console.log(result)
            res.send(result)
        }
    })
})

router.get('/api/assetDetails/searchByNameAndType/:type/:assetName',function(req,res){
    console.log("************type********"+req.params.assetName)
    console.log("************assetName********"+req.params.assetName)
    SensorsModel.aggregate([
                {$match: {
                    "sensorData.assetName":{$regex:req.params.assetName, $options: 'i'},
                    "sensorData.sensorType":req.params.type
                }},
                {$skip: 0},
                {$limit: 10},
                {$sort: {"updated_at":-1}},
                {$project: {"value":{"id":"$_id","assetName":"$sensorData.assetName"},"_id":0}
                }
            ],
            function(err,result){
                if(err){
                    console.log(err)
                }else{
                    res.send(result)
                }
            });

})

router.get('/assetDetails/searchByName/All/:assetName',function(req,res){
    SensorsModel.aggregate([
            {$match: {
                "sensorData.assetName":{$regex:req.params.assetName, $options: 'i'},
               /* "sensorData.sensorType":req.params.type*/
            }},
            {$skip: 0},
            {$limit: 10},
            {$sort: {"updated_at":-1}},
            {$project: {"value":{"id":"$_id","assetName":"$sensorData.assetName"},"_id":0}
            }
        ],
        function(err,result){
            if(err){
                console.log(err)
            }else{
                res.send(result)
            }
        });

})
/*router.get('/existingAsset/assetDetails/:type/:assetName',function(req,res){
    console.log(req.params.assetName);
    console.log(req.params.type);
    SensorsModel.aggregate(

        // Pipeline
        [
            // Stage 1
            {
                $match: {
                    "sensorData.assetName":{$regex:req.params.assetName, $options: 'i'},
                    /!*"sensorData.isLinked":true,*!/
                    "sensorData.sensorType":req.params.type
                }
            },


            // Stage 2
            {
                $skip: 0
            },

            // Stage 3
            {
                $limit: 10
            },

            // Stage 4
            {
                $sort: {
                    "updated_at":-1
                }
            },

            // Stage 5
            {
                $project: {
                    "value":{"id":"$_id","assetName":"$sensorData.assetName"},
                    "_id":0
                }
            }

        ],function(err,result){
            if(err){
                console.log(err)
            }else{
                console.log(result);
                res.send(result)
            }
        });
})*/



router.get('/assetDetails/byRootAssetTypesAndAssetsLinked/:rootAssetType/:assetLinked',function(req,res){
assetManagementModule.assetDataManagement
.getAssetsLinkedToAssetTypeProvided(req.params.rootAssetType,req.params.assetLinked)
.then(function(data){
    res.send(data)
})
})


