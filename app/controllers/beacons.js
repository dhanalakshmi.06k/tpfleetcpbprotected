/**
 * Created by Suhas on 9/6/2016.
 */
var express = require('express'),
    router = express.Router(),
    config = require('../../config/config');
var moment = require('moment');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt
var secret = 'this is the secrete password';
module.exports = function (app) {
    app.use('/', router);
};
var mongoose = require('mongoose');
beacons =  mongoose.model('beaconsDetails');

router.use('/api', expressJwt({secret: secret}));
router.use(function(err, req, res, next){
    if (err.constructor.name === 'UnauthorizedError') {
        res.status(401).send('Unauthorized user please login via the application');
    }
});

router.get('/beacons/count', function (req, res){
    beacons.count(function(err,beaconCount){
        if(err)
            res.send(err);
        var count = {beaconCount: beaconCount};
        res.send(count);
    });
})

router.get('/beacons/get/:start/:range', function (req, res) {
    beacons.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }
    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))
})

router.post('/beacons', function(req, res, next) {
            var newBeacons = new beacons();
            // set the user's local credentials
    newBeacons.beaconId = req.body.beaconId;
    newBeacons.rssi =req.body.rssi;
    newBeacons.companyName =req.body.companyName;
            // save the user
    newBeacons.save(function(err) {
                if (err){
                    console.log('Error in Saving user: '+err);
                }

                res.send("newBeacons added sucessfully");
            });


});






