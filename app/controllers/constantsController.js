/**
 * Created by zendynamix on 7/4/2016.
 */

var express = require('express'),
    router = express.Router(),
    gpsDeviceWoxConstant = require('../../config/addDeviceConstants'),
    mongoose = require('mongoose'),
    routeConfig = require('../../config/assetServiceConfig'),
    constantconfig = require('../../config/constants');
      PolygonDetailsModel = mongoose.model('polygonDetailsSchemaModel');


module.exports = function (app) {
    app.use('/', router);

};

router.route('/constants')
    .get(function (req, res) {
        res.send(constantconfig);
    })


router.route('/assetConfig').get(function (req, res) {
    res.send(routeConfig);
})

router.route('/gpsDeviceWoxConstant').get(function (req, res) {
    res.send(gpsDeviceWoxConstant);
})




router.get('/zonePolygonDetails',function(req,res){
    PolygonDetailsModel.find({},function(err,result){
        if(err){
            res.send(err.stack)
        }else{
            res.send(result)
        }
    })
})
