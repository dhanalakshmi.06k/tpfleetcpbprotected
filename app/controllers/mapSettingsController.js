/**
 * Created by zendynamix on 7/11/2016.
 */

var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    config=require('../../config/config'),
    SettingsModel = mongoose.model('settingModel');
        historyDateSettingSchema = mongoose.model('historyDateSettingModel')
      

    module.exports = function (app) {
        app.use('/', router);
    };

router.get('/configMapDetails',function(req,res){
    SettingsModel.find({},function(err,result){
        if(err){
            res.send(err.stack)
        }else{
            res.send(result)
        }
    })
})



router.post('/config', function(req, res, next) {
    SettingsModel.find({"SettingsConfiguration.id":1},function(err,result){
        console.log( req.body)
        /*console.log(result[0].SettingsConfiguration.mapSettings.mapCenter.latitude)*/
        result[0].SettingsConfiguration.mapSettings.mapCenter.latitude = req.body.centerLatitude;
        result[0].SettingsConfiguration.mapSettings.mapCenter.longitude = req.body.centerLongitude;
        result[0].save(function(err) {
            if (err){
                console.log('Error in Saving configObj: '+err);
            }

            res.send("configObj added sucessfully");
        });
    })



});


router.post('/historyRange', function(req, res, next) {
    historyDateSettingSchema.find({"dateConfiguration.id":1},function(err,result){
        console.log( req.body)
        result[0].dateConfiguration.id = 1;
        result[0].dateConfiguration.fromDate = req.body.fromDate;
        result[0].dateConfiguration.fromTime = req.body.fromTime;
        result[0].dateConfiguration.toDate = req.body.toDate;
        result[0].dateConfiguration.toTime = req.body.toTime;
        result[0].save(function(err) {
            if (err){
                console.log('Error in Saving configObj: '+err);
            }

            res.send("configObj added sucessfully");
        });
    })



});

router.get('/configHistoryRange',function(req,res){
    
    historyDateSettingSchema.find({},function(err,result){
        if(err){
            res.send(err.stack)
        }else{
            res.send(result)
        }
    })
})

