var path = require('path'),
  rootPath = path.normalize(__dirname + '/..'),
  env = process.env.NODE_ENV || 'development';

  dbHost=process.env.DB_HOST_NAME;
  dbPort=process.env.DB_PORT;
  apiBaseUrl = process.env.API_BASE_URL;
  notificationRecHost = process.env.NOTIFICATION_RECEIVE_HOST;
  notificationRecPort = process.env.NOTIFICATION_RECEIVE_PORT;

var config = {
  beaconURL:"tcp://localhost:5211",
  development: {
    root: rootPath,
    app: {
      name: 'taxiFleetControlPanel'
    },
    port: 5200,
     db: 'mongodb://127.0.0.1:27017/taxiFleetCP-development-newDBprojetcted',
    //db: 'mongodb://163.172.131.83:28018/Nec_ConlPanel_DataBase',
    socketUrl:"http://212.47.249.75:5000",
    baseUrl:"http://is-ext1.nec-labs.com:3000",
    hostName:"is-ext1.nec-labs.com",
    zmq:{
      sendHost:"0.0.0.0",
      recHost:"*",
      port:'5211',
      queue:[
        {name:'rfIdDetection',portNo:'52001',type:'pull'},
          {name:'busEntryExitInfoData',portNo:'52004',type:'pull'},
      ]
    }
  },
  integration: {
    root: rootPath,
    app: {
      name: 'taxiFleetControlPanel'
    },
    db: 'mongodb://163.172.131.83:28018/taxiFleetCP-development-newDBprojetcted',
    port: 5200,
    socketUrl:"http://212.47.249.75:5000",
    baseUrl:"http://is-ext1.nec-labs.com:3000",
    zmq:{
      sendHost:"localhsot",
      recHost:"*",
      port:'5201',
      queue:[
        {name:'rfIdDetection',portNo:'52001',type:'pull'}
      ]
    }
  },
  docker: {
    root: rootPath,
    app: {
      name: 'taxiFleetControlPanel'
    },
    db: 'mongodb://'+dbHost+':'+dbPort+'/taxiFleetCP-Docker',
    port: 5200,
    baseUrl:apiBaseUrl,
    zmq:{
      sendHost:"212.47.249.75",
      recHost:"*",
      port:'5201',
      queue:[
        {name:'rfIdDetection',portNo:'52001',type:'pull'}
      ]
    }
  }

};

module.exports = config[env];
