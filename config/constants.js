/**
 * Created by zendynamix on 7/4/2016.
 */

var constants = {
    ASSET_DETAILS:{
        DEFAULT_ASSET_NAME:'car',
        ASSET_NAME_CAR:'car',
        ASSET_NAME_OBD_DEVICE:'obdDevice',
        ASSET_NAME_RFID:'rfIdTag',
        ASSET_NAME_DRIVER:'driver',
        ASSET_NAME_GSM_SIM:'gsmSim',
        typeDetails:{
            car:{label:'Taxi'},
            obdDevice:{label:'Obd Device'},
            rfIdTag:{label:'RFID Tag'},
            driver:{label:'Driver'},
            beacon:{label:'Beacon'},
            gsmSim:{label:'GSM Sim'}
        }
    },
    RFID_DETECTION:{
        DIRECTION:{
            ENTRY:'entry',
            EXIT:'exit'
        },
        RANGE_FOR_EXIT:-80

    }
    ,SOCKET_DETAILS:{
        NOTIFICATION_DATA_PUSHER:'notificationNotifier',
        RFID_DETECTION_DATA_PUSHER:'rfIdDetectionDataPusher'
    }
}

module.exports={constants:constants};