/**
 * Created by zendynamix on 9/11/2016.
 */
var carDetails=[
        {
                cardName:'parkingLot',
                min:1470,
                max:1500
        },
        {
                cardName:'inspection',
                min:530,
                max:550
        },
        {
                cardName:'maintainence',
                min:600,
                max:613
        },
        {
                cardName:'fuelling',
                min:100,
                max:120
        },{
                cardName:'carWash',
                min:60,
                max:80
        },{
                cardName:'meterAndWash',
                min:50,
                max:65
        }
]

module.exports={carDetails:carDetails}