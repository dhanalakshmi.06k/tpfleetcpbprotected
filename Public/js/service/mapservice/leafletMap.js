/**
 * Created by zendynamix on 20/8/16.
 */
taxiFleetControlPanel.factory('leafLetMapService', function(assetApiService,$rootScope) {

    var mymap,
        dir,
        layers,
        BING_KEY = 'AuhiCJHlGzhg93IqUH_oCpl_-ZUrIE6SPftlyGYUvr9Amx5nzA-WqGcPquyFZl4L',
        deviceDetails={},
         polygon ;



    function initLeafLetMap(lat,lng,zoomLevel) {

         mymap = L.map('mapId').setView([lng,lat], zoomLevel);
        /*types of map Aerial Road*/
        var tileLayer = new L.BingLayer(BING_KEY, {type: 'AerialWithLabels'});
            mymap.addLayer(tileLayer);

    }
    function addMarkers(lat, lng,markerId) {
        var markerIcon = L.icon({
            iconUrl: '../../directives/images/square-xxl.png',
            iconSize: [20, 20]
        });
        var marker = L.marker([lat, lng], {icon: markerIcon}).addTo(mymap).on('click', onClick);
        marker._leaflet_id = markerId;
        mymap.panTo(new L.LatLng(lat, lng));
        /*mymap.panTo(new L.LatLng(lat, lng), {animate: true, duration: 1.0});*/
    }
    function onClick(e) {
        console.log("inisde marker alwer");
        console.log(this.getLatLng());
        console.log(this._leaflet_id);
        var dataBox= $(".mapContainer .rightDetails");
        dataBox.fadeIn(0, function () {
            dataBox.removeClass("bounceOutRightCustom").addClass("bounceInRightCustom");
        });
        showDeviceData(this,this._leaflet_id)
    }
    function showDeviceData(element,deviceId) {
        $(".deviceIndicatorMain").removeClass("active");

        $(element).parent().addClass("active");

        var dataBox= $(".mapContainer .rightDetails");

        dataBox.fadeIn(0, function () {
            dataBox.removeClass("bounceOutRightCustom").addClass("bounceInRightCustom");
        });

    assetApiService.getDevicesDetails().then(function (result) {
                if(result){
                     for(i=0;i<result.length;i++){
                     var deviceObj = result[i];
                     if(deviceObj.id===deviceId){
                         var deviceDetailsObj={}
                         deviceDetailsObj.lat=deviceObj.lat
                         deviceDetailsObj.lng = deviceObj.lng;
                         deviceDetailsObj.name = deviceObj.name;
                         deviceDetailsObj.online = deviceObj.online;
                         deviceDetailsObj.updatedDate = new Date(deviceObj.updatedDate);
                         deviceDetailsObj.createdDate = new Date(deviceObj.createdDate);
                         $rootScope.$emit('deviceDetails', { devDetails: deviceDetailsObj });
                     }

                     }
                }

            }, function error(errResponse) {
                console.log(errResponse);
            });



    };
    function drawPolylineMap(directionArray) {
        console.log("inside drawPolylineMap")
            var directionObjArray=[]
        for(var j=0;j<directionArray.length;j++){
            var duumyArr=new Array();
            duumyArr.push(parseFloat(directionArray[j].lat))
            duumyArr.push(parseFloat(directionArray[j].lng))
            directionObjArray.push(duumyArr)

        }
        mymap.setView([directionArray[directionArray.length-1].lat, directionArray[directionArray.length-1].lng], 15)
        plotDirection(directionObjArray)
    }
    function plotDirection(points) {
        var polyline = L.polyline(points,
            {
                color: 'red',
                weight: 3,
                opacity: 0.5,
                smoothFactor: 1
            }
        ).addTo(mymap);
        var label = new L.Label({
            offset: [-40,-7]
        });
      
        startMarkerMoving(points)

    }
    function startMarkerMoving(points){
        console.log(points)
        var line = L.polyline(points)
        var myIcon = L.icon({
            iconUrl: '../../directives/images/square-xxl.png',
            iconSize: [10, 10]
        });

        var animatedMarker = L.animatedMarker(line.getLatLngs(), {
            icon: myIcon,
            interval: 100
        });
        animatedMarker.start();
      /*  setTimeout(function() {
            // Stop the animation
            animatedMarker.start();
        }, 1000);*/

        mymap.addLayer(animatedMarker);

    }
   /* function plotDirection(points) {
        L.Routing.control({
            waypoints:points,
            routeWhileDragging: true
        }).addTo(mymap);

    }*/

    function setCenterLocationOfMap(lat,lng,zoomLevel) {
        /*mymap.panTo(new L.LatLng(lat, lng));*/
        map.setView([lat, lng], zoomLevel)
    }



    function drawPolygon(polyarray,lineColor,fillColor,fillOpacity,matCenterLat,mapCenterLang,zoomLevel,ZoneName){
         polygon = L.polygon(polyarray,{
            color: lineColor,
            fillColor: fillColor,
            fillOpacity: fillOpacity
        }).addTo(mymap);

        polygon.bindTooltip(ZoneName, {
            permanent: true,
            direction:"auto"/*,
            offset:[150,150]*/
        }).openTooltip()



        mymap.setView([ matCenterLat,mapCenterLang], zoomLevel);
    /*    var label = new L.Label();
        label.setContent("MultiPolygon static label");
        label.setLatLng(polygon.getBounds().getCenter());
        mymap.showLabel(label);*/

    }

    function removeExistingPolygon(){
        mymap.removeLayer(polygon);
    }

    function    addCameraMarkers(lat, lng,markerId) {
        var markerIcon = L.icon({
            iconUrl: '../../directives/images/camera.png',
            iconSize: [20, 20]
        });

        var marker = L.marker([lat, lng], {icon: markerIcon}).addTo(mymap).bindPopup('<video src="../../directives/videos/lane1.mp4" controls style="width:267px;height:267px "></video>');
        marker._leaflet_id = markerId;
        mymap.panTo(new L.LatLng(lat, lng));

    }



    return {
        initMap: initLeafLetMap,
        addMarkers:addMarkers,
        drawPolylineMap:drawPolylineMap,
        setCenterLocationOfMap:setCenterLocationOfMap,
        drawPolygon:drawPolygon,
        removeExistingPolygon:removeExistingPolygon,
        addCameraMarkers:addCameraMarkers


    };
});