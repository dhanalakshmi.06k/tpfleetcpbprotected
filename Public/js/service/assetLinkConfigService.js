/**
 * Created by Suhas on 8/24/2016.
 */


taxiFleetControlPanel.factory('assetLinkConfigService',function($http){
        var getAssetLinkConfigBYType = function(type){
                return $http.get('/api/assetsLinkConfig/type/'+type)
        }
  var getBeaconData = function(){
                console.log("CLient request to get beacon data::::");
                return $http.get('/app/beaconData');
  }

        return{
                getAssetLinkConfigBYType:getAssetLinkConfigBYType,
          getBeaconData:getBeaconData
        }
})