/**
 * Created by zendynamix on 7/8/2016.
 */
taxiFleetControlPanel.service('settingService',['$http', function($http) {
    var mapObj={};
    var dateObj={}
    var appSettings=null;
    var simulatorFlag=false;
    var settingService = {
        addMapConfigDetails: function (mapConfigObject) {
            return $http.post('/config',mapConfigObject);
        },
        getMapConfig:function(){
            return $http.get('/configMapDetails');
        },

        init:function(){
            var p =$http.get('/appSettings')
            p.then(function(res) {
                appSettings= res.data;
            })
            return p;
        },
        getAppSettings:function(){
            return appSettings
        },
        setMapConfigObj:function(mapConfigObj){
            mapObj=mapConfigObj;

        },
        getMapConfigObj:function(){
            return mapObj;
        },
        addHistoryDateConfigDetails: function (dateConfigObject) {
            return $http.post('/historyRange',dateConfigObject);
        },
        getHistoryDateConfigDetails:function(){
            return $http.get('/configHistoryRange');
        },
        setHistoryDateConfigObj:function(dateConfigObj){
            dateObj=dateConfigObj;

        },
        getHistoryDateConfigObj:function(){
            return dateObj;
        },
         startTaxiEntryExitSimulator :function(){
           return $http.get('/busEntryExitData/startDataPush');
        },
         stopTaxiEntryExitSimulator : function(){
          return  $http.get('/busEntryExitData/stopDataPush');
        },
        setTaxiEntryExitSimulatorFlag:function(flagValue){
            simulatorFlag=flagValue;

        },
        getTaxiEntryExitSimulatorFlag:function(){
            return simulatorFlag;
        },


    }

    return settingService;



}]);
