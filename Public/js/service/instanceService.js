/**
 * Created by zendynamix on 07-06-2016.
 */

taxiFleetControlPanel.service('instanceService',['$http','appService', function($http,appService) {
    var routeConfig={};
    var fileNameFromView="";
    var fileObjectArrayDetails=[];
/*    var pulseApplicationUrl=appService.getPulseApiConstants();*/
   /* var pulseApplicationUrl=appService.getPulseApiConstants();*/
    var pulseAppBaseUrl="/pulseWatchList";
    var pulseAppPostBaseUrl="/pulseWatchListPostFile";
   /* console.log("&&&&&&&&&&&&&&&&&&appService.getPulseApiConstants*&&&&&&&&&&&&&&&&&&&&&&&&");
    console.log(pulseApplicationUrl)
    console.log("&&&&&&&&&&&&&&&&&&appService.getPulseApiConstants*&&&&&&&&&&&&&&&&&&&&&&&&");*/
    var instanceConfigObj={
        "appId": "pulse-watchlist",
        "nodes": [
            "string"
        ],
        "configuration": {
            "cameraInputAddress": "http://138.15.120.50/mjpg/1/video.mjpg",
            "alertsOutputAddress": "",
            "faceOutputAddress": "",
            "alertsLogFile": "",
            "videoOutputAddress": "tcp://127.0.0.1:11000",
            "alertsPictureLogPath": "",
            "faceDetectorProcesses": 3,
            "trackingWindowSize": 1,
            "maxFaces": 10,
            "featureMatcherProcesses": 1,
            "matchScoreThreshold": 0.5,
            "seenCacheSize": 100,
            "seenCacheExpirationTimeout": 1,
            "longitude": -74.595284,
            "latitude": 40.359734,
            "neofaceConfiguration": {
                "eyes_max_width": 800,
                "eyes_min_width": 20,
                "reliability_threshold": 0.5,
                "max_face_count": 15,
                "max_face_roll": 30
            }
        }
    };
    var appConfiguration={};
    var configObj={};
    var instanceService = {
        getInstanceDetails:function(){
            var pulseApplicationUrl=appService.getPulseApiConstants();
            return $http.get(pulseAppBaseUrl+pulseApplicationUrl.instanceApiUrl);
        },
        getInstanceObj:function(){
            return instanceConfigObj;
        },
        saveInstance:function(instanceObj){
            var pulseApplicationUrl=appService.getPulseApiConstants();
            return $http.post(pulseAppBaseUrl+pulseApplicationUrl.instanceApiUrl,instanceObj);
        },
        deleteInstance:function(instanceID){
        return  $http.delete(pulseAppBaseUrl+pulseApplicationUrl.instanceApiUrl+'/'+instanceID);
        },


    getInstanceConfigurationById:function(instanceId){
            return  $http.get(pulseAppBaseUrl+pulseApplicationUrl.instanceApiUrl+'/'+instanceId+"/configuration");
        },
        getAllInstances:function(instanceIdArrayFromApi){
            return $http.post("/InstanceDetails",instanceIdArrayFromApi);
        },
        getInstanceDetailsFromApi:function(instanceId){
            return $http.get(pulseAppBaseUrl+pulseApplicationUrl.instanceApiUrl+'/'+instanceId);
        },
        updateInstanceStatus:function(instanceId,bodyParameter){
            return $http.post( pulseAppBaseUrl+pulseApplicationUrl.instanceApiUrl+'/'+instanceId+"/status",bodyParameter);

        },
        getAppconfigurationByInstance: function () {
            return appConfiguration;
        },

        setAppconfigurationByInstance: function (appInstanceConfiguration) {
            appConfiguration=appInstanceConfiguration;

        },
         setAppConfig:function(appConfig){
            configObj=appConfig;

        },
         getAppConfig:function(){
            return configObj;
        },
       updateInstanceConfigById:function(instanceId,InstanceConfig){
           var instanceObj={}
           instanceObj.instanceConfig=InstanceConfig
            return $http.post(pulseAppBaseUrl+pulseApplicationUrl.instanceApiUrl+'/'+instanceId+"/configuration",instanceObj);
        },


       saveFileForInstance: function (fileObj,instanceId) {
            var fd = new FormData();
            fd.append('file', fileObj);
            return $http.post(pulseAppPostBaseUrl+pulseApplicationUrl.instanceApiUrl+'/'+instanceId+"/files",fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
        },
        getFilesBasedOnInstanceId:function(instanceId){
            return $http.get(pulseAppBaseUrl+pulseApplicationUrl.instanceApiUrl+'/'+instanceId+"/files");
        },
        deleteInstanceFileByID:function(instanceId,fileName){
            return  $http.delete(pulseAppBaseUrl+pulseApplicationUrl.instanceApiUrl+'/'+instanceId+"/files/"+fileName);
        },
        setFileName:function(fileName){
            fileNameFromView=fileName;

        },
        getFileName:function(){
            return fileNameFromView;
        },
        updateInstanceNodeDetails:function(instanceId,bodyParameter){

            return $http.post(pulseAppBaseUrl+pulseApplicationUrl.nodeApiUrl+'/'+instanceId+"/nodes",bodyParameter);

        },
        setInstanceFileObjectArray:function(objectArray){
            fileObjectArrayDetails=objectArray;
        },
        getInstanceFileObjectArray:function(){
            return fileObjectArrayDetails;
        }

    }
    return instanceService;


}]);
