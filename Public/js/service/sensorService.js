/**
 * Created by Suhas on 6/18/2016.
 */
taxiFleetControlPanel.service('sensorService',['$http','$q', function($http,$q) {
    var sensordetails = '';
    var sensorMapObject='';
    var sensorName = '';
    var getSensors = function(){
        return $http.get('/api/sensors');
    }
    var addSensors= function(sensorObj){
        return $http.post('/api/sensors',sensorObj);
    }
    var setSensorDetails = function(sensorObj){
        sensordetails=sensorObj;
    }
    var getSensorDetails = function(){
        return sensordetails
    }
    var updateSensorDetails = function(obj){
        return $http.put('/api/sensors',obj);
    }
    var getSensorDetailsById = function(id){
        return $http.get('/api/sensors/'+id);
    }
    var deleteSensorDetailsById = function(id){
        return $http.delete('/api/sensors/'+id);
    }
    var getAssetTypeConfigByAssetType =function(assetType){
        return $http.get('sensorConfiguration/'+assetType)
    }
    var getAllSensorTypes =function(){
        return $http.get('sensorConfiguration/sensorTypes/all')
    }
    var generateHtmlFromJson1 = function(data,callBack) {
        var label = [];
            var extractedData = [];
            var k = Object.keys(data);
            k.forEach(function (objKey, index) {
                if (data[k[index]].type) {
                    label.push(k[index]);
                    var obj = {
                        "name": data[k[index]].label,
                        "realName": objKey,
                        "type": data[k[index]].type,
                        "description": data[k[index]].description,
                        "model": label.join('.'),
                        required:data[k[index]].required,
                        autoComplete:data[k[index]].autoComplete
                    };
                    if(data[k[index]].value){
                        obj.dropDownDetails={}
                        obj.dropDownDetails=data[k[index]].value;
                    }
                    if(data[k[index]].assetName){
                        obj.assetName=data[k[index]].assetName;
                    }
                    (extractedData).push(obj);
                    label.pop();
                }
                else {
                    label.push(k[index]);
                    generateHtmlFromJson1(data[k[index]], label);
                    label.pop();
                }
                if(k.length==extractedData.length){
                    callBack(extractedData)
                }

            });
        }
    var getSensorCount = function(){
        return $http.get('/api/sensorsCount')
    }
    var getSensorDetailsListByType = function(type){
        return $http.get('/api/sensorsDetails/'+type)
    }
    var getSensorDetailsInstanceMapperByTypeAndSensorName = function(sensorType,sensorName,fieldName){
        return $http.get('/api/sensorsDetails/instanceDataMapper/'+sensorType+'/'+sensorName+'/'+fieldName)
    }
    var getSensorDetailsInstanceMapperByTypeAndSensorNameAndField = function(sensorType,sensorName,fieldname){
        return $http.get('/api/sensorsDetails/instanceDataMapper/'+sensorType+'/'+sensorName+'/'+fieldname)
    }
    var setSensorMapObject = function(sensorObj){
        sensorMapObject=sensorObj;
    }
    var getSensorMapObject = function(){
        return sensordetails
    }
    var setSensorNameOfSensorInSensorDetails = function(sensorObjName){
        sensorName=sensorObjName;
    }
    var getSensorNameOfSensorInSensorDetails = function(){
        return sensorName
    }
    var checkSensorNameForUniqueness = function(sensorname){
        return $http.get('/api/sensorsDetails/isUnique/'+sensorname);
    }
    var getSensorCountByType = function(assetType){
        return $http.get('/api/sensorsCount/'+assetType)
    }
    var getAssetDetailsByPageNo = function(skip,limit){
        return $http.get('/api/sensors/'+skip+'/'+limit)
    }
    var getAssetDetailsByPageNoAndAssetType = function(skip,limit,assetType){
        return $http.get('/api/sensors/'+skip+'/'+limit+'/'+assetType)
    }
    var updateApiReferenceByID = function(apiRefId,uniqueIdentifierNumber){
        return $http.get('/updatesensorDeviceBYuniqueIdentifierNumber/'+uniqueIdentifierNumber+'/'+apiRefId)
    }
    var getFormDetailsByAssetType =function(assetType,callBack){
        return $q(function(resolve,reject){
            $http.get('sensorConfiguration/'+assetType).then(function(result){
                var data = result.data.sensorConfiguration.configuration;
                var label = [];
                var extractedData = [];
                var k = Object.keys(data);
                k.forEach(function (objKey, index) {
                    if (data[k[index]].type) {
                        label.push(k[index]);
                        var obj = {
                            "name": data[k[index]].label,
                            "realName": objKey,
                            "type": data[k[index]].type,
                            "description": data[k[index]].description,
                            "model": label.join('.'),
                            required:data[k[index]].required,
                            autoComplete:data[k[index]].autoComplete
                        };
                        if(data[k[index]].value){
                            obj.dropDownDetails={}
                            obj.dropDownDetails=data[k[index]].value;
                        }
                        if(data[k[index]].assetName){
                            obj.assetName=data[k[index]].assetName;
                        }
                        (extractedData).push(obj);
                        label.pop();
                    }
                    else {
                        label.push(k[index]);
                        generateHtmlFromJson1(data[k[index]], label);
                        label.pop();
                    }
                    if(k.length==extractedData.length){
                        resolve(extractedData);
                    }
                });
            })
        })

    }
    var deviceBYUnqiueId=function(uniqueId){
        return $http.get('/findSensorDeviceBYUniqueIdentifierNumber/'+uniqueId)
    }
    var getAllAssetsDetailsByType=function(type){
        return $http.get('/api/sensorsData/'+type)
    }
    var getAssetNameForAssetsByTypeId=function(assetType){
        return $http.get('/api/sensorConfiguration/assetNameByType/'+assetType);
    }
     var getAssetNameArr = function(type,assetName){
         return $http.get('/api/assetDetails/searchByNameAndType/'+type+'/'+assetName);
     }
    var getAssetNameArrForAll = function(assetName){
        return $http.get('/assetDetails/searchByName/All/'+assetName);
    }
    return{
        getSensors:getSensors,
        addSensors:addSensors,
        setSensorDetails:setSensorDetails,
        getSensorDetails:getSensorDetails,
        updateSensorDetails:updateSensorDetails,
        getSensorDetailsById:getSensorDetailsById,
        deleteSensorDetailsById:deleteSensorDetailsById,
        getAssetTypeConfigByAssetType:getAssetTypeConfigByAssetType,
        getAllSensorTypes:getAllSensorTypes,
        generateHtmlFromJson1:generateHtmlFromJson1,
        getSensorCount:getSensorCount,
        getSensorDetailsListByType:getSensorDetailsListByType,
        getSensorDetailsInstanceMapperByTypeAndSensorName:getSensorDetailsInstanceMapperByTypeAndSensorName,
        getSensorDetailsInstanceMapperByTypeAndSensorNameAndField:getSensorDetailsInstanceMapperByTypeAndSensorNameAndField,
        setSensorMapObject:setSensorMapObject,
        getSensorMapObject:getSensorMapObject,
        checkSensorNameForUniqueness:checkSensorNameForUniqueness,
        setSensorNameOfSensorInSensorDetails:setSensorNameOfSensorInSensorDetails,
        getSensorNameOfSensorInSensorDetails:getSensorNameOfSensorInSensorDetails,
        getSensorCountByType:getSensorCountByType,
        getAssetDetailsByPageNo:getAssetDetailsByPageNo,
        getAssetDetailsByPageNoAndAssetType:getAssetDetailsByPageNoAndAssetType,
        getFormDetailsByAssetType:getFormDetailsByAssetType,
        updateApiReferenceByID:updateApiReferenceByID,
        deviceBYUnqiueId:deviceBYUnqiueId,
        getAllAssetsDetailsByType:getAllAssetsDetailsByType,
        getAssetNameForAssetsByTypeId:getAssetNameForAssetsByTypeId,
        getAssetNameArr:getAssetNameArr,
        getAssetNameArrForAll:getAssetNameArrForAll
    }
}]);
