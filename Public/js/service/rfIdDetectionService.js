/**
 * Created by Suhas on 9/6/2016.
 */
taxiFleetControlPanel.factory('rfIdDetectionService',function($http,$rootScope){
        var rfIdDetectionDetails={
                constants:{
                        RFID_DETECTION_NOTIFIER:'rfIdDetectionDataPusher'
                },
                rfIdDetectionData:{}
        }
        socket.on('rfIdDetectionDataPusher',function(data){
                rfIdDetectionDetails.rfIdDetectionData = data;
                triggerRfIdDetection(data);
        })
        function triggerRfIdDetection(data){
                $rootScope.$emit(rfIdDetectionDetails.constants.RFID_DETECTION_NOTIFIER,data)
        }
        var getRfIdDetectionData = function(){
                return rfIdDetectionDetails.rfIdDetectionData;
        }
        var getRfIdTotalCount = function(){
                return $http.get('/api/rfIdDetection/totalCount')
        }
        var getRfIdDataBySkipAndLimit= function(skip,limit){
                return $http.get('/api/rfIdDetection/'+skip+'/'+limit)
        }
        var getRfIdTotalCountByDate = function(date){
                var utcDate = moment.utc(date).valueOf();
                return $http.get('/api/rfIdDetection/totalCountByDate/date/'+utcDate)
        }
        var getRfIdDataByDateSkipAndLimit= function(date,skip,limit){
                var utcDate = moment.utc(date).valueOf();
                return $http.get('/api/rfIdDetectionByDate/'+utcDate+'/'+skip+'/'+limit)
        }
        return{
                rfIdDetectionDetails:rfIdDetectionDetails,
                getRfIdDetectionData:getRfIdDetectionData,
                getRfIdTotalCount:getRfIdTotalCount,
                getRfIdDataBySkipAndLimit:getRfIdDataBySkipAndLimit,
                getRfIdTotalCountByDate:getRfIdTotalCountByDate,
                getRfIdDataByDateSkipAndLimit:getRfIdDataByDateSkipAndLimit
        }
})