/**
 * Created by zendynamix on 18/8/16.
 */

taxiFleetControlPanel.service('assetApiUrlService',['$http','settingService', function($http,settingService) {
    var asset = {
        //get baseUrl lang and hash of gpswox
        getAssetsConfig:function(){
            return $http.get('/assetConfig');
        },
        //list all the devices
        getDevices: function () {
            return $http.get('/getAllDevices');
        },
        getDeviceHistory:function(deviceId){
            /*var fromDate=moment().format('YYYY-MM-DD')
             var toDate=moment().format('YYYY-MM-DD')
             var fromTime=moment().format('HH:mm');
             var toTime=moment().subtract(1,'hour').format('HH:mm');*/
          

            var fromDate='2016-08-15'
            var toDate='2016-08-15'
            var fromTime='00:00';
            var toTime='24:00';


            return $http.get('/getDeviceHistoryById/'+fromDate+'/'+toDate+'/'+fromTime+'/'+toTime+'/'+ deviceId)
        },
        addDevice:function(deviceObj){
            return $http.post('/addDevice',deviceObj);
        },
        deleteDevice:function(deviceApiRefId){
            return $http.get('/deleteDevice/'+deviceApiRefId);
        },
        editDevice:function(){

        }


    }
    return asset;

}]);
