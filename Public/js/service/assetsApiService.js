
/**
 * Created by zendynamix on 8/11/2016.
 */


taxiFleetControlPanel.factory('assetApiService',function($http,assetApiUrlService,sensorService,settingService){
    var getDevicesDetails= function(){
        return assetApiUrlService.getDevices().then(function (result) {
            console.log("*********getDevices************");
            console.log(result)
            var deviceDetailsArray=[];
            for(var l=0;l<result.data.length;l++){
                var detailsObj={};
                detailsObj.id=result.data[l].id
                detailsObj.name=result.data[l].name
                detailsObj.online=result.data[l].online
                detailsObj.lat=result.data[l].lat
                detailsObj.lng=result.data[l].lng
                detailsObj.createdDate=result.data[l].device_data.created_at
                detailsObj.updatedDate=result.data[l].device_data.updated_at
                deviceDetailsArray.push(detailsObj);
                if(result.data.length-1==l) {
                    return deviceDetailsArray;
                }
            }
             }, function error(errResponse) {
                     return errResponse;
                 });
    }
    var getDeviceHistory = function(fromDate,toDate,fromTime,toTime,deviceId){
        /*var fromDate=moment().format('YYYY-MM-DD')
         var toDate=moment().format('YYYY-MM-DD')
         var fromTime=moment().format('HH:mm');
         var toTime=moment().subtract(1,'hour').format('HH:mm');*/


       /* var fromDate='2016-8-15'
        var toDate='2016-8-15'
        var fromTime='00:00';
        var toTime='24:00';*/
        
        console.log(fromDate)
        console.log(toDate)
        console.log(fromTime)
        console.log(toTime)
        console.log('/getDeviceHistoryById/'+fromDate+'/'+toDate+'/'+fromTime+'/'+toTime+'/'+ deviceId)

        return $http.get('/getDeviceHistoryById/'+fromDate+'/'+toDate+'/'+fromTime+'/'+toTime+'/'+ deviceId)


    }


    var addDevice = function(deviceObj){
        return $http.post('/addDevice',deviceObj);
    }
    var deleteDevice = function(deviceApiRefId){
        return $http.get('/deleteDevice/'+deviceApiRefId);
    }
    var editDevice = function(){

    }
    var getMapPlotsFromApi=function(){
     return getDevicesFromApiAndLocal();
    }
    function getDevicesFromApiAndLocal(){
        return  assetApiUrlService.getDevices().then(function (result) {
            return   sensorService.getAllAssetsDetailsByType('obdDevice').then(function (response) {
                console.log("**************************assetApiUrlService************")
                console.log(result)
                console.log(response)
                console.log("******************response********************")
                var deviceDetailsArray=[];
                for(var  i=0;i<response.data.length;i++){
                    for(var  j=0;j<result.data.length;j++){

                        if(response.data[i].sensorData.uniqueIdentifier==result.data[j].device_data.imei){
                           
                            var detailsObj={};
                            detailsObj.id=result.data[j].id
                            detailsObj.name=result.data[j].name
                            detailsObj.online=result.data[j].online
                            detailsObj.lat=result.data[j].lat
                            detailsObj.lng=result.data[j].lng
                            detailsObj.tail=result.data[j].tail
                            deviceDetailsArray.push(detailsObj);
                        }
                    }


                    if(response.data.length-1===i) {

                        return (deviceDetailsArray.length>0?deviceDetailsArray:[]);
                    }

                }


            }, function error(errResponse) {
                return errResponse;
            });

        }, function error(errResponse) {
            return errResponse;
        });

    }
    return{
        getDevicesDetails:getDevicesDetails,
        getDeviceHistory:getDeviceHistory,
        addDevice:addDevice,
        deleteDevice:deleteDevice,
        editDevice:editDevice,
        getMapPlotsFromApi:getMapPlotsFromApi


    }
})



