/**
 * Created by MohammedSaleem on 24/06/16.
 */

taxiFleetControlPanel.factory('jsonFormService',function($http){
    var sensorConfig={};
    var getJsonDesc = function(){
        return $http.get('data/watchListConfiguration.json')
    }
    var getJsonSample = function(){
        return $http.get('data/watchlistSampel.json')
    }
    var setSensorConfig=function(sensorConfigDetails){
            sensorConfig=sensorConfigDetails;

    }
   var  getSensorConfig=function(){
        return sensorConfig;
    }



    return{
        getJsonDesc:getJsonDesc,
        getJsonSample:getJsonSample,
        setSensorConfig:setSensorConfig,
        getSensorConfig:getSensorConfig


    }
});
