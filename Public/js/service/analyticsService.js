/**
 * Created by dhanalakshmi on 16/9/16.
 */


taxiFleetControlPanel.service('analyticsService',['$http', function($http) {
    var analyticsService = {


        getHistoryBasedOnDateRange: function (fromDate,toDate) {

            var utcFromDate = moment.utc(fromDate).valueOf();
            var utcToDate=moment.utc(toDate).valueOf();

            var url=''
            url='http://'+location.hostname+':'+location.port+'/rfIdHistoryDetectionByDateRange/'+utcFromDate+"/"+utcToDate;
            var win = window.open(url, '_blank');
            win.focus();
            return("sent")

        },
        getHistoryBasedOnDateRangeTabel: function (fromDate,toDate) {
            console.log("************ inside servcie")
            var utcFromDate = moment.utc(fromDate).valueOf();
            var utcToDate=moment.utc(toDate).valueOf();
            return $http.get('/rfIdHistoryDetectionByDateRangeTable/'+utcFromDate+'/'+utcToDate)

        },
        getPrintAsset: function () {
            var url=''
            url='http://'+location.hostname+':'+location.port+'/printSensors';
            var win = window.open(url, '_blank');
            win.focus();
            return("sent")

        }

    }
    return analyticsService;

}])
