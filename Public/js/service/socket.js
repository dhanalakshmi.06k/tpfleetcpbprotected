/**
 * Created by Suhas on 4/4/2016.
 */
taxiFleetControlPanel.factory('socket', function ($rootScope) {
        var url = 'http://212.47.249.75:5000';
        var socket = io.connect(url);
        var setSocketUrl = function(urlVal){
                url = urlVal;
        }
        return {
                on: function (eventName, callback) {
                        socket.on(eventName, function () {
                                var args = arguments;
                                $rootScope.$apply(function () {
                                        callback.apply(socket, args);
                                });
                        });
                },
                emit: function (eventName, data, callback) {
                        socket.emit(eventName, data, function () {
                                var args = arguments;
                                $rootScope.$apply(function () {
                                        if (callback) {
                                                callback.apply(socket, args);
                                        }
                                });
                        })
                },
                setSocketUrl:setSocketUrl
        };
})