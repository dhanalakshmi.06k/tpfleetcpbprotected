/**
 * Created by Suhas on 8/11/2016.
 */

taxiFleetControlPanel.factory("linkAssetsService", function ($http) {
        var linkAssets = function(linkAssetsDetails){
                return $http.put('/api/linkAssets',linkAssetsDetails);
        }
        var linkAssetsNew = function(linkAssetsDetails){
                return $http.post('/api/linkAssetsNew',linkAssetsDetails);
        }
        var deLinkAssets = function(linkAssetsDetails){
                return $http.put('/api/deLinkAssets',linkAssetsDetails);
        }
        var deLinkAssetsNew = function(linkAssetsDetails){
                return $http.post('/api/deLinkAssetsNew',linkAssetsDetails);
        }
        var getLinkedAssetsCount = function(assetType){
                return $http.get('/api/linkedAssetsCounts/'+assetType);
        }
        var getLinkedAssetsLists = function(skip,limit,assetType){
                return $http.get('/api/linkedAssets/'+skip+"/"+limit+'/'+assetType);
        }
        var getBeaconDataSet = function(){
                return $http.get('/app/beaconData');
        }
        /*var getAssetByType =function(type,assetName){
                return $http.get("/assetDetails/searchByNameAndType/:type/:assetName"+type+"/"+assetName);
        }*/
        return {
                linkAssets:linkAssets,
                getLinkedAssetsCount:getLinkedAssetsCount,
                getLinkedAssetsLists:getLinkedAssetsLists,
                deLinkAssets:deLinkAssets,
                linkAssetsNew:linkAssetsNew,
                deLinkAssetsNew:deLinkAssetsNew,
          getBeaconDataSet:getBeaconDataSet
              /*  getAssetByType:getAssetByType*/
        }
})