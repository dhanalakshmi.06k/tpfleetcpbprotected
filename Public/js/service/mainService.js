/**
 * Created by Suhas on 4/4/2016.
 */
taxiFleetControlPanel.factory('mainService',function($http){
        var getSocketUrl = function(){
                return $http.get('/necDashBoard/settings/getSocketUrl')
        }

        var getHelpDetails=function(){
                return $http.get('/helpDetails')
        }
        return{
                getSocketUrl:getSocketUrl,
                getHelpDetails:getHelpDetails
        }
})