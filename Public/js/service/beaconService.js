/**
 * Created by Suhas on 3/8/2016.
 */
taxiFleetControlPanel.factory("beaconService", function ($http) {
    var createBeacon = function(beaconData){
        return $http.post('/beacons',beaconData)
    }

    var getBeaconsCount = function(){
        return $http.get('/necControlPanel/userDetails/count');
    }
    var getBeaconsByRange = function(skip,limit){
        return $http.get('/necControlPanel/userDetails/get/'+skip+'/'+limit)
    }

    return{
        createBeacon:createBeacon,
        getBeaconsCount:getBeaconsCount,
        getBeaconsByRange:getBeaconsByRange
    }
})