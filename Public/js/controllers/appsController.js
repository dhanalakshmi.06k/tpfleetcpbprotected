/**
 * Created by zendynamix on 07-06-2016.
 *
 */
taxiFleetControlPanel.controller("appsController", function ($scope, appService, $http, $stateParams, $state, $timeout, instanceService,$showdown) {

    $scope.appConfigDetials = {
        showAppLoader: false,
        showSaveAppLoader: false,
        showDeleteAppLoader: false,
        appDetails: "",
        showAppInstanceLoader: false,
        showSaveInstanceLoader: false,
        appConstant: ""
    }

    $scope.getApplicationConfig = function () {
        $scope.appConfigDetials.appConstant = appService.getApplicationConstants();
    }

    $scope.closeAppUpload = function () {
        $scope.validationError = " ";
        $scope.appInstallErrorMessage = " ";
    }
    $scope.showApplicationName = function () {
        if ($scope.myFile) {
            $scope.fileName = $scope.myFile;
        }
    }
    $scope.emptyErrorMessage = function () {
        $scope.validationError = " ";
        $scope.appInstallErrorMessage = " ";

    }
    $scope.saveApp = function () {
        if ($scope.myFile) {
            $scope.appConfigDetials.showSaveAppLoader = true
            var file = $scope.myFile;
            appService.appApplication(file).then(function (response) {
                $scope.appConfigDetials.showSaveAppLoader = false;
                getAllAppDetails();
                $scope.closePopup('addNewApp');
            }, function error(errResponse) {
                $scope.appConfigDetials.showSaveAppLoader = false;
                $scope.appInstallErrorMessage = errResponse.data.message
            });
        }
        else {
            $scope.validationError = "Please upload the file";
        }


    }
    function getAllAppDetails() {
        $scope.appConfigDetials.showAppLoader = true;
        appService.getAppDetails().then(function (result) {

                $scope.appDetails = result.data;
                $scope.appConfigDetials.showAppLoader = false;


        }, function error(errResponse) {
            $scope.appDetailsError = errResponse.data.message
            $scope.appConfigDetials.showAppLoader = false;
        });
    }

    if ($stateParams.applicationId) {
        $scope.applicationId = $stateParams.applicationId;
        getInstanceByAppId($stateParams.applicationId)
        getAppDocumentationDetailsByID($stateParams.applicationId)

    }
    function getInstanceByAppId(applicationId) {
        $scope.appConfigDetials.showAppInstanceLoader = true;
        instanceService.getInstanceDetails().then(function (result) {
            var instanceDetailsArrayFromApi = result.data;
            getApplicationSpecificInstance(applicationId, instanceDetailsArrayFromApi);

        });
    }
    function getApplicationSpecificInstance(applicationId, instanceDetailsArrayFromApi) {
        var resInstanceArrayWithName = [];
        for (j = 0; j < instanceDetailsArrayFromApi.length; j++) {
            if (instanceDetailsArrayFromApi[j].parameters.appId === applicationId) {
                instanceDetailsArrayFromApi[j].creationTimestamp = new Date(instanceDetailsArrayFromApi[j].creationTimestamp);
                instanceDetailsArrayFromApi[j].statusUpdateTimestamp = new Date(instanceDetailsArrayFromApi[j].statusUpdateTimestamp);
                resInstanceArrayWithName.push(instanceDetailsArrayFromApi[j]);
            }
        }
        $scope.instanceDetailsArrayFromApi = resInstanceArrayWithName;
        $scope.appConfigDetials.showAppInstanceLoader = false;


    }
    $scope.getAllAppNames = function () {
        appService.getAppDetails().then(function (result) {
            $scope.appDropDownName = result.data;
        });
    }

    $scope.emptyfromObject = function () {
        $scope.instanceObjDetails = {}
        $scope.instanceObjDetails.userInstanceName = "";
        $scope.configData = "";
        $scope.sampleData = "";
        $(".instancesSelected .headTitle").val("");
    }
        // ....... mark down service api  call .......
    function getAppDocumentationDetailsByID(appId){
        appService.getAppDocumentationDetailById(appId).then(function (result) {
            $scope.appDocumentationDetails=result.data;

        }, function error(errResponse) {
            $scope.getAppDocumentationDetailsError = errResponse.data.message
        });
    }
    $scope.unInstallApplication = function (appDetailsById) {
        $scope.appConfigDetials.showDeleteAppLoader = true;
        appService.deleteAppById(appDetailsById).then(function (response) {
            $scope.appConfigDetials.showDeleteAppLoader = false;
            $scope.closePopup('uninstallApp');
            $(".messagePopup.appUnsInstalled").fadeIn(300);
            $timeout(function () {
                $state.go('necApp.appsMain.installedApps');
            }, 3000);

        }, function error(errResponse) {
            $scope.appConfigDetials.showDeleteAppLoader = false;
            $scope.appUnInstallErrorMessage = errResponse.data.message
        });

    }


    function init() {
        getAllAppDetails();
        $scope.getApplicationConfig();

    }
    init();

})
