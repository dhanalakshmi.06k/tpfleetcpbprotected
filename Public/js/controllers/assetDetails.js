/**
 * Created by MohammedSaleem on 26/04/16.
 */

taxiFleetControlPanel.controller("sensorDetails", function ($scope,$location,watchListService,
                                                            sensorService,$stateParams,
                                $state,settingService,$timeout,$rootScope,assetApiService,
                                assetApiUrlService,leafLetMapService) {
    //...... Adding devices List.......
    $scope.sensorDetails={
        showPolylineLoader:false
    }
    $scope.isSensorDetailsNameUnique=true;
    function getSelectedSensorDetails(){
        $scope.sensordetails = sensorService.getSensorDetails();
    }
    // getSelectedSensorDetails();
    $scope.setProtocolType = function(type){
        $scope.sensordetails.sensorData.rfIdProtocol=type;
    }
    $scope.setFrequencyType = function(type){
        $scope.sensordetails.sensorData.rfIdFrequency=type;
    }
    $scope.setRfIdtype = function(type){
        $scope.sensordetails.sensorData.rfIdType=type;
    }
    $scope.update = function(){
       console.log("*******************")
        var type = $scope.sensordetails.sensorData.sensorType;
        sensorService.getAssetNameForAssetsByTypeId(type).then(function(result){
            if(result.data && $scope.sensordetails.sensorData[result.data]){
                $scope.sensordetails.sensorData.assetName=$scope.sensordetails.sensorData[result.data];
            }
            sensorService.updateSensorDetails($scope.sensordetails)
            .then(function(err,result){
                getSensorDetailsForEdit($stateParams.id);
            })
        })
    }
    $scope.deleteSensor = function(refId){
        console.log("*********")
        console.log(refId)
        var id= $scope.sensordetails._id;
        sensorService.deleteSensorDetailsById(id).then(function(result){
            assetApiUrlService.deleteDevice(refId).then(function(response){

                    $(".messagePopup.sensorDeleted").fadeIn(100);
                    $timeout(function () {
                        $(".messagePopup.sensorDeleted").fadeOut(100);
                        console.log("Sensor Object Deleted");
                        $state.go('necApp.sensorMain.sensors');
                    }, 1000);

            }, function error(errResponse) {
                console.log(errResponse)
            })

        })
    }
    function getSensorDetailsForEdit(id){
        sensorService.getSensorDetailsById(id).then(function(response){
            if(response && response.data){
                $scope.sensordetails = response.data;
                getSensorConfiguration($scope.sensordetails.sensorData.sensorType)
                $scope.pageLoader=false;
                if($scope.sensordetails.sensorData.latitude && $scope.sensordetails.sensorData.latitude){
                    var lat = parseFloat($scope.sensordetails.sensorData.latitude);
                    var lng = parseFloat($scope.sensordetails.sensorData.longitude);
                    var type = $scope.sensordetails.sensorData.sensorType;
                    var sensorName = $scope.sensordetails.sensorData.sensorName;

                   /* initMap(lat,lng,type,sensorName);*/
                }
                sensorService.setSensorNameOfSensorInSensorDetails(sensorName);
            }
        })

    }
    $scope.assetTypeConfiguration = {};
    function getSensorConfiguration(sensorType){
        sensorService.getAssetTypeConfigByAssetType(sensorType).then(function(result){
            if(result.data && result.data.sensorConfiguration){
                $scope.assetTypeConfiguration.json = result.data.sensorConfiguration.configuration;
                $scope.assetTypeConfiguration.configAvailable = true
                getFormForSensorType(result.data.sensorConfiguration.configuration)
            }

        })
    }
    function getFormForSensorType(jsonData){
        sensorService.generateHtmlFromJson1(jsonData,function(data){
            $scope.form=data;
        })
    }
    $scope.$watch('sensordetails.sensorData.sensorName',checkSensorDetailsUniqueName);
    function checkSensorDetailsUniqueName(){
        var currentSensorName = sensorService.getSensorNameOfSensorInSensorDetails();
        if($scope.sensordetails
           && $scope.sensordetails.sensorData
           && $scope.sensordetails.sensorData.sensorName){
            sensorService.checkSensorNameForUniqueness($scope.sensordetails.sensorData.sensorName).then(function(result){
                if($scope.sensordetails.sensorData.sensorName === currentSensorName){
                    $scope.isSensorDetailsNameUnique = true;
                }else{
                    $scope.isSensorDetailsNameUnique=result.data;
                }
                console.log($scope.isSensorDetailsNameUnique)
            })
        }
    }

    function getDeviceNavigationDetailsFromApi(apiRefId){
        $scope.sensorDetails.showPolylineLoader=true;
        var fromDate,toDate;
        var fromTime,toTime;



        settingService.getHistoryDateConfigDetails().then(function(response){
            var fromDateFromSettings=new Date(response.data[0].dateConfiguration.fromDate)
            var fromDateMonth = fromDateFromSettings.getUTCMonth() + 1; //months from 1-12
            var fromDateDay = fromDateFromSettings.getUTCDate();
            var fromDateYear = fromDateFromSettings.getUTCFullYear();

            var toDateFromSettings=new Date(response.data[0].dateConfiguration.toDate)
            var toDateMonth = toDateFromSettings.getUTCMonth() + 1; //months from 1-12
            var toDateDay = toDateFromSettings.getUTCDate();
            var toDateYear = toDateFromSettings.getUTCFullYear();
            fromDate=fromDateYear+'-'+fromDateMonth+'-'+fromDateDay
            toDate=toDateYear+'-'+toDateMonth+'-'+toDateDay
            fromTime='00:'+response.data[0].dateConfiguration.fromTime,
                toTime=response.data[0].dateConfiguration.toTime+':00'
            getHistoryBasedONDate(fromDate,toDate,fromTime,toTime,apiRefId)

        })


    }


    function getHistoryBasedONDate(fromDate,toDate,fromTime,toTime,apiRefId){

        assetApiService.getDeviceHistory(fromDate,toDate,fromTime,toTime,apiRefId).then(function (result) {
            var deviceDetailsArray=[];
            console.log(result.data.items)
            if(typeof result.data.items!= 'undefined'&& result &&result.data&&result.data.items.length!==0){
                console.log("inside iffffff")
                for(var p=0;p<result.data.items.length;p++){
                    for(var b=0;b<result.data.items[p].items.length;b++){
                        var obj={};
                        obj.lat=result.data.items[p].items[b].latitude;
                        obj.lng=result.data.items[p].items[b].longitude;
                        deviceDetailsArray.push(obj)

                    }

                }
                leafLetMapService.drawPolylineMap(deviceDetailsArray);
                $scope.sensorDetails.showPolylineLoader=false;

            }
            else{
                assetApiUrlService.getDevices().then(function (result) {
                    $scope.sensorDetails.showPolylineLoader=false;
                    for(var l=0;l<result.data[0].items.length;l++){
                        if(result.data[0].items[l].id===parseInt(apiRefId)){
                            leafLetMapService.addMarkers(parseFloat(result.data[0].items[l].lat),parseFloat(result.data[0].items[l].lng),result.data[0].items[l].id);

                        }
                    }

                }, function error(errResponse) {
                    console.log(errResponse);
                });

            }

        }, function error(errResponse) {
            console.log(errResponse);
        });

    }







    if($stateParams.id){
        if($stateParams.apiRefId){
            getDeviceNavigationDetailsFromApi($stateParams.apiRefId);
            getSensorDetailsForEdit($stateParams.id);

        }
        else{
            getSensorDetailsForEdit($stateParams.id);
        }

    }
});