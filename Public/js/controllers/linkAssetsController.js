/**
 * Created by Suhas on 8/11/2016.
 */
taxiFleetControlPanel.controller('linkAssetsCtrl', function ($scope,constantService,
                             sensorService,linkAssetsService,$q,assetLinkConfigService, $stateParams,beaconService) {
        $scope.value={
                assets:{}
        }
        $scope.assetDataToBeLinked={};
    $scope.overviewRelated ={
        paginationDetails:{
            maxDocPerPage:5,
            maxNoOfPages:10,
            totalDocs:0,
            pageSelected:1,
            noOfPagesToBeDisplayed:10,
            totalNoOfPages:0
        },
        dateTimeForTaxi:'',
        paginationType:'all',
        dateSelected:''
    }

        console.log($stateParams.iotListType)
        console.log(`console.log($stateParams.linktype) = ${$stateParams.iotListType}`)


        $scope.linkAssetsVariables = {
                mainTab:{
                        selected:'car',
                        assetLabel:'Car'
                },
                subTab:{
                        labels:['New Car','Existing Car']
                },
                carId:0,
                assetsFormDetails:[],
                assetLinkedDetails:[],
                assets:{},
                constants:'',
                linkedAssetsLists:[],
                paginationDetails:{
                        maxDocsPerPage:10,
                        maxNoOfPages:10,
                        selectedPage:1,
                        totalNoOfPages:0,
                        noOfPagesToBeDisplaced:10
                },
                rootAssetDetails:{},
                rootAssetType:$stateParams.iotListType,
                assetsDetails:[],
                deLinkPopUpClass:'deleteLink'
        }
        function getConstants(){
                constantService.getConstant()
                .then(function(result){
                        $scope.linkAssetsVariables.constants = result.data.constants.ASSET_DETAILS;
                })
        }
        function getSensorConfiguration(sensorType){
                sensorService.getFormDetailsByAssetType(sensorType)
                .then(function(result){
                        if(result){
                                $scope.form = result;
                                $scope.linkAssetsVariables.assetsFormDetails=result;
                        }
                })
        }
        $scope.selectAssetsTab = function (assetType, assetLabel,linkDetails) {
                addVariableToLink (assetType, assetLabel);
                $scope.linkAssetsVariables.mainTab.selected = assetType;
                $scope.assetType = assetType;
                $scope.linkAssetsVariables.mainTab.assetLabel = assetLabel;
                $scope.sensordetails='';
                $scope.linkAssetsVariables.rootAssetDetails = linkDetails;
                $scope.linkAssetsVariables.mainTab.selected=assetType;
                getSensorConfiguration(assetType);
                if(linkDetails.sensorData.sensorType===assetType){
                        $scope.sensordetails = linkDetails;
                }else{
                   getLinkedAssetDetailsByType($scope.assetType,linkDetails)
                   .then(function(data){
                           $scope.sensordetails=data;
                   })
                }
        }
        function getLinkedAssetDetailsByType(type,assetData){
                return $q(function(resolve,reject){
                        if(assetData.sensorData.linkedAssetsDetails){
                                var assetObject = assetData.sensorData.linkedAssetsDetails;
                                var typeData = {};
                                if(assetObject[type]){
                                        typeData=assetObject[type];
                                }
                                /*if(type==="driver"){
                                        resolve(typeData[0])}
                                else{*/
                                        resolve(typeData)}
                      /*  }*/
                })
        }
        function addVariableToLink (assetType, assetLabel, carId) {
                $scope.assetTypeLink=assetType;
                $scope.assetLabelLink=assetLabel;
                $scope.carIdLink=carId;
        }
        $scope.saveAssetDetailsAndLink = function(assetType,assetLabel){
                $scope.addSensorLoader=true;
                $scope.assetDataToBeLinked.sensorType=assetType;
                $scope.assetDataToBeLinked.sensorLabel=assetLabel;
                var assetNameVariable= new Object($scope.assetDataToBeLinked.assetName);
                $scope.assetDataToBeLinked.assetName = $scope.assetDataToBeLinked[assetNameVariable];
                $scope.assetDataToBeLinked.islinked = true;
                sensorService.addSensors($scope.assetDataToBeLinked)
                .then(function(result){
                        var linkAssetsObject = {
                                assetOne:{
                                        id:$scope.linkAssetsVariables.rootAssetDetails._id,
                                        type:$scope.linkAssetsVariables.rootAssetDetails.sensorData.sensorType
                                },
                                assetTwo:{
                                        id:result.data._id,
                                        type:result.data.sensorData.sensorType
                                }
                        };
                        linkAssetsService.linkAssetsNew(linkAssetsObject)
                        .then(function(response){
                                $scope.addSensorLoader=false;
                                getLinkedAssetCount();
                                $scope.closePopup('addSensor');

                        })
                })

        }
        $scope.deLinkAsset = function(assetIdOne,assetIdTwo){
                $scope.delinkAssetData={"assetOne":assetIdOne,"assetTwo":assetIdTwo}
                $scope.openPopup($scope.linkAssetsVariables.deLinkPopUpClass)

        }
        $scope.deleteLink = function(){
                linkAssetsService.deLinkAssetsNew($scope.delinkAssetData)
                .then(function(data){
                        getLinkedAssetCount()
                        $scope.closePopup($scope.linkAssetsVariables.deLinkPopUpClass)
                })
        }



        /******* Pagination *****/
        function getLinkedAssetList(){
                var maxAssetPerPage = $scope.linkAssetsVariables.paginationDetails.maxDocsPerPage;
                var selectedPageNo = $scope.linkAssetsVariables.paginationDetails.selectedPage;
                var skip =((selectedPageNo-1)*maxAssetPerPage)
                var limit=maxAssetPerPage;
                linkAssetsService.getLinkedAssetsLists(skip,limit,$scope.linkAssetsVariables.rootAssetType)
                .then(function(result){
                        $scope.linkAssetsVariables.linkedAssetsLists = result.data;
                })
        }
        function getLinkedAssetCount(){
                linkAssetsService.getLinkedAssetsCount($scope.linkAssetsVariables.rootAssetType)
                .then(function(result){
                        var totalPageNoToBeDisplayed = parseInt(result.data.count)/$scope.linkAssetsVariables.paginationDetails.maxDocsPerPage;
                        var roundedTotalPageNo = Math.round(parseInt(result.data.count)/$scope.linkAssetsVariables.paginationDetails.maxDocsPerPage);
                        if(totalPageNoToBeDisplayed>roundedTotalPageNo){
                                totalPageNoToBeDisplayed =roundedTotalPageNo+1;
                        }else if(totalPageNoToBeDisplayed < 1){
                                totalPageNoToBeDisplayed=1;
                        }else if(totalPageNoToBeDisplayed<roundedTotalPageNo){
                                totalPageNoToBeDisplayed=roundedTotalPageNo;
                        }
                        $scope.linkAssetsVariables.paginationDetails.totalNoOfPages=roundedTotalPageNo;
                        if(totalPageNoToBeDisplayed<=$scope.linkAssetsVariables.paginationDetails.maxNoOfPages){
                                $scope.linkAssetsVariables.paginationDetails.noOfPagesToBeDisplaced = range(totalPageNoToBeDisplayed);
                        }else{
                                $scope.linkAssetsVariables.paginationDetails.noOfPagesToBeDisplaced = range($scope.linkAssetsVariables.paginationDetails.maxNoOfPages);
                        }
                        getLinkedAssetList();
                })
        }
        function range(n) {
                return new Array(n);
        };
        $scope.getAssetsLinkedByPageNo = function(pageNo){
                $scope.linkAssetsVariables.paginationDetails.selectedPage=pageNo;
                getLinkedAssetList();
        }
        $scope.incrementPageNo = function(){
                $scope.linkAssetsVariables.paginationDetails.selectedPage=$scope.linkAssetsVariables.paginationDetails.selectedPage+1;
                getLinkedAssetList();
        }
        $scope.decrementPageNo = function(){
                $scope.linkAssetsVariables.paginationDetails.selectedPage=$scope.linkAssetsVariables.paginationDetails.selectedPage-1;
                getLinkedAssetList();
        }
        $scope.beaconList = [];
        $scope.beaconPaused = false;

  $scope.clearBeaconData = function () {
    $scope.beaconList.length = 0;
  }

  $scope.pauseBeaconData = function () {
    $scope.beaconPaused = !$scope.beaconPaused;
  }
        var beaconListener = socket.on('beaconMessage', function (msg) {
          let data = JSON.parse(msg);
          if($scope.beaconPaused)return;
          console.log($scope.beaconList.length);
            $scope.beaconList.unshift(data);

        /*  if(!$scope.beaconList){
            $scope.beaconList = [];
            $scope.beaconList.length = 0;
          }
          if ($scope.beaconList.length >= 10) {
            $scope.beaconList.pop()
            $scope.beaconList.unshift(data);
          } else {if($scope.beaconList)
            $scope.beaconList.unshift(data);
          }*/
          $scope.$apply();
        });

        function getBeaconData() {

           linkAssetsService.getBeaconDataSet();
        }

        /***** Init Function *******/
        function linkAssetsCtrlInit(){
                $scope.linkAssetsVariables.assetsDetails=[];
                getConstants();
                getLinkedAssetCount();
                getBeaconData();
                getAssetLinkConfig($scope.linkAssetsVariables.rootAssetType)
                /*getAllSensorTypes();*/
        }
        /*Asset Link Template*/
        function getAssetLinkConfig(type){
                assetLinkConfigService.getAssetLinkConfigBYType(type)
                .then(function(result){
                    console.log("****************resultresult**********************")
                    console.log(result)
                        $scope.linkAssetsVariables.assetsDetails = result.data;
                })
        }
        /*getAssetLinkConfig()*/


        /*DeLink Popup*/
        $scope.openPopup= function (popup) {
                $(".background").fadeIn(250, function () {
                        $("."+popup).fadeIn(250);
                });
        };
        $scope.closePopup= function (popup) {
                $("."+popup).fadeOut(250, function () {
                        $(".background").fadeOut(250);
                });
        };


        function getAllAssetTypes(){
                var array  = []
                var filterArray = new Array();
                sensorService.getAllSensorTypes()
                .then(function(result){
                        if(result && result.data){
                                var sensorConfigArray = result.data;
                                for(var i=0;i<sensorConfigArray.length;i++){
                                        var sensorConfigObj = sensorConfigArray[i];
                                        array.unshift(sensorConfigObj.linkAssetsVariables.sensorType);
                                        filterArray.unshift(sensorConfigObj.linkAssetsVariables.sensorType);
                                        if(array.length==sensorConfigArray.length){
                                                $scope.linkAssetsVariables.sensorTypes=array;
                                                var allData = {'typeId':'all','label':'All'}
                                                filterArray.unshift(allData)
                                                $scope.sensorControllerObjects.linkAssetsVariables=filterArray;
                                        }
                                }
                                /*$scope.sensorControllerObjects.sensorTypes=result.data;*/
                        }
                })
        }
        $scope.setTypeToGetAssetLinkList = function(rootType){
                $scope.linkAssetsVariables.rootAssetType = rootType;
                linkAssetsCtrlInit();
        }
  //
  $scope.$on('destroy',function(){
    beaconListener();
  });

       /* if($stateParams.iotListType){
                $scope.linkAssetsVariables.rootAssetType = $stateParams.iotListType;*/
                linkAssetsCtrlInit();
       /* }*/

})