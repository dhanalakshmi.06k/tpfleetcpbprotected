
taxiFleetControlPanel.controller("mainController", function ($scope,$location,mainService,helpWidgetService,$sce,$state,$timeout,$rootScope) {


    $scope.projectDetails={
        socketUrl:"http://localhost:5000"
    };
    $scope.currentRout= function (path) {
        var loc=$location.path();
        return loc.includes(path)
    };

    $scope.tabsType="";
    $scope.tabsControl= function (currentTab) {
        $scope.tabsType=currentTab;
    };

    $scope.showMessagePopup= function (popup) {
        $("."+popup).fadeIn(300);
        $timeout(function () {
            $("."+popup).fadeOut(300);
        },3000);

    };
    $scope.openPopup= function (popup) {
        $(".background").fadeIn(250, function () {
            $("."+popup).fadeIn(250);
        });
    };
    $scope.closePopup= function (popup) {
        $("."+popup).fadeOut(250, function () {
            $(".background").fadeOut(250);
        });
    };

    /*function getSocketUrl(){
        mainService.getSocketUrl().then(function(res){
            var socketUrl = res.data;
            console.log(socketUrl)
            socket.setSocketUrl(socketUrl);
        })
    }
    function init(){
        getSocketUrl();
    }*/
   /* init();*/
    $scope.getAppHelpDetails=function(){
        mainService.getHelpDetails().then(function (result) {
            $scope.appHelpDetails=result.data;

        });
    }
    $scope.setPageCode = function(pageCode){
        $scope.htmlValue = '';
        helpWidgetService.setPageCodeForHelpDescription(pageCode);
        setHtmlValForHelp(pageCode)
    }
    function setHtmlValForHelp(pageCode){
        helpWidgetService.getHelpWidgetContentByPageCode(pageCode).then(function(result){
            if(result && result.data){
                $scope.htmlValue = $sce.trustAsHtml(result.data.widgetDescription)
            }
        })
    }
    function getHelpContentByPageCode(){
        if($state.includes('necApp.overview')){
            setHtmlValForHelp('Overview')
        }else if($state.includes('necApp.appsMain.installedApps') || $state.includes('necApp.appsMain.apps')){
            setHtmlValForHelp('Apps')
        }else if($state.includes('necApp.instanceMain.instance') || $state.includes('necApp.appsMain.apps')){
            setHtmlValForHelp('Instance')
        }else if($state.includes('necApp.serversMain.servers')){
            setHtmlValForHelp('Servers')
        }else if($state.includes('necApp.dataMain')){
            setHtmlValForHelp('data')
        }else if($state.includes('necApp.sensorMain.sensors') || $state.includes('necApp.sensorMain.sensorDetails')){
            setHtmlValForHelp('Sensors')
        }
    }
    function init(){
        getHelpContentByPageCode();
    }
    init();
   /* $scope.getAppHelpDetails();*/


// ........... default values for google maps ......
    // sample devices
    $scope.devices = [
        {
            "type": "sensor",
            "name": "sensor1",
            "sensors": 23,
            "up": 21,
            "down": 2,
            "lat": 37.456175,
            "lng": -122.182729
        },
        {
            "type": "sensor",
            "name": "sensor2",
            "sensors": 20,
            "up": 19,
            "down": 1,
            "lat": 37.454037,
            "lng": -122.246990
        },
        {
            "type": "camera",
            "name": "server1",
            "sensors": 25,
            "up": 19,
            "down": 6,
            "lat": 37.449526,
            "lng": -122.113673
        },
        {
            "type": "camera",
            "name": "server2",
            "sensors": 22,
            "up": 20,
            "down": 3,
            "lat": 37.421065,
            "lng": -122.066173
        }
    ];


    // Create an array of styles.
    $scope.mapStyles = [

        {
            "featureType": "all",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#838f9d"
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "visibility": "simplified"
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry",
            "stylers": [
                {
                    "weight": 0.6
                },
                {
                    "color": "#1a3541"
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#1d212f"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#1e2230"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#1e2230"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#353b50"
                },
                {
                    "lightness": 10
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#1a1e2a"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#616c8d"
                }
            ]
        }
    ];
    $scope.showNotification=false;
    $scope.notificationHideShow= function () {
        $scope.showNotification=!$scope.showNotification;
        $rootScope.$emit('mapResize')
    }

});
