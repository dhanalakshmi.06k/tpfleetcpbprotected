/*
 * Created by MohammedSaleem on 29/03/16.
 */

taxiFleetControlPanel.controller("overviewController", function ($scope,$location,sensorService,
             appService,nodeService,settingService,$timeout,$rootScope,
             instanceService,assetApiService,leafLetMapService,constantService,
             rfIdDetectionService,paginationService,carDataService,$filter) {/*paginationService*/

    var map;
    $scope.overviewRelated ={
        servers:{
            count:'-',
            up:'-',
            down:'-'
        },
        instance:{
            count:'-',
            up:'-',
            down:'-'
        },
        app:{
            count:'-'
        },
        sensors:{
            count:'-'
        },
        deviceInfo:{
            data:[],
            selectedDeviceType:'',
            selectedDeviceName:''

        },
        deviceInfoNew:{},
        constants:[],
        appConstant:"",
        taxiEntryExitDetails:[],
        paginationDetails:{
            maxDocPerPage:5,
            maxNoOfPages:10,
            totalDocs:0,
            pageSelected:1,
            noOfPagesToBeDisplayed:10,
            totalNoOfPages:0
        },
        dateTimeForTaxi:'',
        paginationType:'all',
        dateSelected:''
    }

    function getSimulatorDataForDepo() {
        carDataService.getSimulatorData();
    }

    /*push data from simulator*/
    socket.on('busEntryExitData', function (data) {
        console.log("busEntryExitDatabusEntryExitDatabusEntryExitData")
        console.log(data)
        console.log("************************************************")
        $scope.showTxiEntryExitData = true;
        if ($scope.overviewRelated.taxiEntryExitDetails.length >= 10) {
            $scope.overviewRelated.taxiEntryExitDetails.pop()
            $scope.overviewRelated.taxiEntryExitDetails.unshift(data);
        } else {
            $scope.overviewRelated.taxiEntryExitDetails.unshift(data);
        }
        $scope.$apply();

    })
    function getAssetsCount(){
        sensorService.getSensorCount().then(function(res){
            $scope.overviewRelated.sensors.count=res.data.count;
        })
    }
    function getAssetsDetails(){
        assetApiService.getDevicesDetails().then(function (result) {
            if(result){
                $scope.overviewRelated.deviceInfo.data = result;
            }

        }, function error(errResponse) {
            console.log(errResponse);
        });

    }
    function getAllDeviceFromApi() {

        assetApiService.getMapPlotsFromApi().then(function (result) {
            $scope.plotPolygonBasedOnZone('parkingLot')
            /*if(result){
                plotMarkersFromApi(result)
            }*/
        }, function error(errResponse) {
            console.log(errResponse);
        });
    }
    const DATE_FORMAT = {
        ALERT_MANAGER: 'YYYY-MM-DD[T]HH:mm:ss[Z]',
        ALERT_FACEMATCH_FORMAT: 'MMM Do, YYYY HH:mm A',
    };

    $scope.$watch('overviewRelated.dateTimeForTaxi', function (newValue) {
        $scope.overviewRelated.dateTimeForTaxi = $filter('date')(newValue,DATE_FORMAT.ALERT_FACEMATCH_FORMAT);
    });

    function plotMarkersFromApi(deviceDetailsArray){
        for(var k=0;k<deviceDetailsArray.length;k++){
            if(deviceDetailsArray[k].lat!=0&&deviceDetailsArray[k].lng!=0){
                leafLetMapService.addMarkers(deviceDetailsArray[k].lat,deviceDetailsArray[k].lng,deviceDetailsArray[k].id)
            }


        }

    }
    $rootScope.$on('deviceDetails', function (event, args) {
        $scope.overviewRelated.deviceInfoNew = args.devDetails;
       
    });
    $scope.plotPolygonBasedOnZone=function (ZoneName) {
        var mapConfig=constantService.getPolygonZonePlotsFromConfig()
        if(ZoneName==="parkingLot"){
            leafLetMapService.addCameraMarkers(25.279390, 55.415800,'12411')
        }
        leafLetMapService.drawPolygon(mapConfig[ZoneName].polygon.coOrdinates,
            mapConfig[ZoneName].polygon.lineColor,
            mapConfig[ZoneName].polygon.fillColor,
            mapConfig[ZoneName].polygon.fillOpacity,
            mapConfig[ZoneName].mapCenterCoOrdinates.lat,
            mapConfig[ZoneName].mapCenterCoOrdinates.lng,
            mapConfig[ZoneName].mapCenterCoOrdinates.zoomLevel,ZoneName)


        }
    $scope.removePolygon=function () {
        leafLetMapService.removeExistingPolygon()

    }
  /*  socket.on('taxiEntryExitData',function(data){
        if($scope.overviewRelated.taxiEntryExitDetails.length>=10){
            $scope.overviewRelated.taxiEntryExitDetails.pop()
            $scope.overviewRelated.taxiEntryExitDetails.unshift(data);
        }else{
            $scope.overviewRelated.taxiEntryExitDetails.unshift(data);
        }
        $scope.$apply();

    })*/


        /*TAXi RF ID Detection Data Functionality*/

    function setInitialPaginationDetails(){
        var maDocsPerPage = $scope.overviewRelated.paginationDetails.maxDocPerPage;
        var maxNoOfPagesToBeDisplayed = $scope.overviewRelated.paginationDetails.maxNoOfPages;
        paginationService.setPaginationInitDetails(maDocsPerPage,maxNoOfPagesToBeDisplayed)
    }
    function getRfIdDetectionTotalCount(type){
        if(type=='all'){
            rfIdDetectionService.getRfIdTotalCount()
                    .then(function(rfIdDetectionTotalCount){
                        paginationService.setTotalNoOfDocs(rfIdDetectionTotalCount.data.count);
                        $scope.overviewRelated.paginationDetails = paginationService.getPaginationDetails();
                    })
        }else{
            var date = moment($scope.overviewRelated.dateTimeForTaxi);
            rfIdDetectionService.getRfIdTotalCountByDate(date)
                    .then(function(rfIdDetectionTotalCount){
                        paginationService.setTotalNoOfDocs(rfIdDetectionTotalCount.data.count);
                        $scope.overviewRelated.paginationDetails = paginationService.getPaginationDetails();
                    })
        }
    }

    function getRfIdDetectionDataByPageNo(pageNo){
        var skip = ((pageNo - 1) * $scope.overviewRelated.paginationDetails.maxDocPerPage)
        var limit = $scope.overviewRelated.paginationDetails.maxDocPerPage;
        if($scope.overviewRelated.paginationType==='all'){
            rfIdDetectionService.getRfIdDataBySkipAndLimit(skip,limit)
                    .then(function(rfIdDetectionData){
                        $scope.overviewRelated.taxiEntryExitDetails=[];
                        $scope.overviewRelated.taxiEntryExitDetails=rfIdDetectionData.data;
                    })
        }else{
            var date = moment($scope.overviewRelated.dateTimeForTaxi);
            rfIdDetectionService.getRfIdDataByDateSkipAndLimit(date,skip,limit)
                    .then(function(rfIdDetectionData){
                        $scope.overviewRelated.taxiEntryExitDetails=[];
                        $scope.overviewRelated.taxiEntryExitDetails=rfIdDetectionData.data;
                    })
        }
    }
    $scope.incrementPageGetRfIDDetectionData=function(){
        $scope.overviewRelated.paginationDetails.pageSelected = $scope.overviewRelated.paginationDetails.pageSelected +1;
        getRfIdDetectionDataByPageNo($scope.overviewRelated.paginationDetails.pageSelected)
    }
    $scope.decrementPageGetRfIDDetectionData = function(){
        $scope.overviewRelated.paginationDetails.pageSelected = $scope.overviewRelated.paginationDetails.pageSelected -1;
        getRfIdDetectionDataByPageNo($scope.overviewRelated.paginationDetails.pageSelected)
    }
    $scope.getDataByPageNo = function(pageNo){
        $scope.overviewRelated.paginationDetails.pageSelected = pageNo;
        getRfIdDetectionDataByPageNo(pageNo)
    }

    $scope.$watch('overviewRelated.dateTimeForTaxi',function(){
        if($scope.overviewRelated.dateTimeForTaxi!=''){
            var data = moment($scope.overviewRelated.dateTimeForTaxi).valueOf();
            console.log(moment($scope.overviewRelated.dateTimeForTaxi).valueOf())
            $scope.overviewRelated.paginationType = 'date';
            $scope.overviewRelated.paginationDetails.pageSelected = 1;
            updateRfIdDataDetectionList();
        }
    })

    $scope.clearFilter = function(){
        $scope.overviewRelated.dateTimeForTaxi='';
        $scope.overviewRelated.paginationType = 'all';
        $scope.overviewRelated.paginationDetails.pageSelected = 1;
        updateRfIdDataDetectionList()
    }
    function updateRfIdDataDetectionList(){
        getRfIdDetectionTotalCount($scope.overviewRelated.paginationType);
        getRfIdDetectionDataByPageNo($scope.overviewRelated.paginationDetails.pageSelected)
    }

    $rootScope.$on(rfIdDetectionService.rfIdDetectionDetails.constants.RFID_DETECTION_NOTIFIER,function(data){
        updateTaxiEntryExitTable();
        $scope.$apply();
    });
    function updateTaxiEntryExitTable(){
        var dayOfSelectedTime=null;
        if($scope.overviewRelated.dateTimeForTaxi!=''){
            dayOfSelectedTime = $scope.overviewRelated.dateTimeForTaxi.getDate();
        }
        var currentDay  = new Date().getDate();
        if($scope.overviewRelated.paginationDetails.pageSelected==1){
            if($scope.overviewRelated.dateTimeForTaxi==='' || dayOfSelectedTime===currentDay){
                var data = rfIdDetectionService.rfIdDetectionDetails.rfIdDetectionData;
                if($scope.overviewRelated.taxiEntryExitDetails.length>=$scope.overviewRelated.paginationDetails.maxDocPerPage){
                    $scope.overviewRelated.taxiEntryExitDetails.pop();
                    $scope.overviewRelated.taxiEntryExitDetails.unshift(data);
                    getRfIdDetectionTotalCount($scope.overviewRelated.paginationType);
                }else{
                    $scope.overviewRelated.taxiEntryExitDetails.unshift(data);
                }
            }
        }
    }
    function init(){
        getAssetsDetails();
        getAssetsCount();
        getAllDeviceFromApi();
        setInitialPaginationDetails();
        updateRfIdDataDetectionList();
        getSimulatorDataForDepo();
    }
    init();



    /*updating Card details*/
    $scope.cardDetails={
        parkingLot:0,
        inspection:0,
        maintainence:0,
        fuelling:0,
        carWash:0,
        meterAndWash:0,
    }
    var rootScopeFunctionOne = $rootScope.$on('cardDetails', function (data,args) {
        updateCardDetails();
        $scope.$apply();
    });
    function updateCardDetails(){
        var cardDetailArray = carDataService.getCardDetails();
        for(var i=0;i<cardDetailArray.length;i++){
            var cardObj = cardDetailArray[i];
            $scope.cardDetails[cardObj.cardName]=cardObj.value;
        }
    }
    $scope.$on('destroy',function(){
        rootScopeFunctionOne();
    })
});



