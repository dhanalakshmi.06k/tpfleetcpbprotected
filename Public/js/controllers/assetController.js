/**
 * Created by MohammedSaleem on 01/04/16.
 */

taxiFleetControlPanel.controller("sensorController", function ($scope, $location, watchListService,
                                                               sensorService,settingService,$rootScope,
                                                               $timeout,constantService,assetApiService,
                                                               assetApiUrlService,leafLetMapService,$state,analyticsService) {

    var map;
    $scope.flag = 1;
    $scope.sensorControllerObjects = {
        /*$scope.sensorControllerObjects.paginationDetails.maxAssetsPerPage*/
        addingSensor: {
            value: {
                type: 'Car',
            },
            type: 'car',
            label: "Car",
            form: [],
            sensorTypes: [],
            isSensorNameUnique: true
        },
        markerSelected: {
            sensorName: '',
            sensorType: '',
            sensorLat: '',
            sensorLng: '',
        },
        assetConstants: '',
        paginationDetails: {
            assetPageSelected: 1,
            assetTotalPageCount: 0,
            maxPageNo: 10,
            maxAssetsPerPage: 10,
            assetTypeSelected: "all",
            "totalNoOfAssets": 10
        },
        searchData: {
            assetByName: ''
        }
    }

    //GET ASSETS TYPE
    function getConstant() {
        constantService.getConstant().then(function (result) {
            $scope.sensorControllerObjects.assetConstants = result.data.constants
            sensorDetailsInit();
        })
    }

    function getInitConfig() {
        getConstant();
    }

    getInitConfig();
    //...... assets managed from the application ........

    $scope.addSensors = function (value) {
        $scope.addSensorLoader = true;
        var sensorNameVariable = new Object($scope.sensor.assetName);
        $scope.sensor.sensorType = $scope.sensorControllerObjects.addingSensor.type;
        $scope.sensor.sensorLabel = $scope.sensorControllerObjects.addingSensor.label;
        $scope.sensor.assetName = $scope.sensor[sensorNameVariable];
        sensorService.addSensors($scope.sensor)
                .then(function (result) {
                  /*  pushAddedAssetToListToDisplay(result.data)*/
                    $scope.addSensorLoader = false;
                    $scope.closePopup('addSensor');
                    $scope.sensor = {};
                    getAssetCountByType();


                })
                .catch(function (err) {
                    console.log(err)
                })
    }

    $scope.downloadDateBasedOnRange=function () {
        console.log("***********************downloadDateBasedOnRange****************************")
        analyticsService.getPrintAsset()


    }

    $scope.changeSensorType = function (type, label) {
        /*$scope.sensor.sensorType=$scope.sensorControllerObjects.addingSensor.type;*/
        $scope.sensorControllerObjects.addingSensor.type = type;
        $scope.sensorControllerObjects.addingSensor.label = label;
        /*$scope.sensorControllerObjects.addingSensor.form=$scope.sensorControllerObjects.addingSensor[type].formRequired;*/
        /*if(type==='rfIdReader'){
         setRfIdValues();
         }*/
        getSensorConfiguration(type);
    }
    $scope.changeSensorType($scope.sensorControllerObjects.addingSensor.type, $scope.sensorControllerObjects.addingSensor.label);
    function getSensorsDetails() {
        sensorService.getSensors().then(function (result) {
            $scope.pageLoader = false;
            $scope.sensorDataForList = result.data;

        })
    }

    $scope.setSensorDetails = function (obj) {
        sensorService.setSensorDetails(obj);
    }
    function getSensorConfiguration(sensorType) {
        sensorService.getAssetTypeConfigByAssetType(sensorType).then(function (result) {
            if (result.data && result.data.sensorConfiguration) {
                $scope.assetTypeConfiguration = {};
                $scope.assetTypeConfiguration = {
                    json: result.data.sensorConfiguration.configuration
                };
                console.log($scope.assetTypeConfiguration)
                getFormForSensorType(result.data.sensorConfiguration.configuration)
                $scope.showForm = true;
            }

        })
    }

    function getFormForSensorType(jsonData) {
        sensorService.generateHtmlFromJson1(jsonData, function (data) {
            $scope.sensorControllerObjects.addingSensor.form = /*$compile($sce.trustAsHtml(*/data/*))*/;
        })
    }

    function getAllSensorTypes() {
        var array = []
        var filterArray = new Array();
        sensorService.getAllSensorTypes().then(function (result) {
            if (result && result.data) {
                var sensorConfigArray = result.data;
                for (var i = 0; i < sensorConfigArray.length; i++) {
                    var sensorConfigObj = sensorConfigArray[i];
                    array.unshift(sensorConfigObj.sensorConfiguration.sensorType);
                    filterArray.unshift(sensorConfigObj.sensorConfiguration.sensorType);
                    if (array.length == sensorConfigArray.length) {
                        $scope.sensorControllerObjects.sensorTypes = array;
                        var allData = {'typeId': 'all', 'label': 'All'}
                        filterArray.unshift(allData)
                        $scope.sensorControllerObjects.sensorTypesForFilter = filterArray;
                    }
                }
                /*$scope.sensorControllerObjects.sensorTypes=result.data;*/
            }
        })
    }


    /*pagination*/
    $scope.getAssetDetailsByPageNo = function (pageNo) {
        $scope.sensorControllerObjects.paginationDetails.assetPageSelected = pageNo;
        getSensorsDetailsByPageNoAndType();
    }
    $scope.range = function (n) {
        return new Array(n);
    };
    function range(n) {
        return new Array(n);
    };
    function getAssetCountByType() {
        var assetTypeSelectedForPagination = $scope.sensorControllerObjects.paginationDetails.assetTypeSelected;
        if (assetTypeSelectedForPagination != 'all') {
            sensorService.getSensorCountByType(assetTypeSelectedForPagination).then(function (result) {
                var totalPageNo = parseInt(result.data.count) / $scope.sensorControllerObjects.paginationDetails.maxAssetsPerPage;
                var roundedTotalPageNo = Math.round(parseInt(result.data.count) / $scope.sensorControllerObjects.paginationDetails.maxAssetsPerPage);
                if (totalPageNo > roundedTotalPageNo) {
                    totalPageNo = roundedTotalPageNo + 1;
                } else if (totalPageNo < 1) {
                    totalPageNo = 1;
                } else if (totalPageNo < roundedTotalPageNo) {
                    totalPageNo = roundedTotalPageNo;
                }
                $scope.sensorControllerObjects.paginationDetails.totalNoOfAssets = totalPageNo;
                if (totalPageNo <= $scope.sensorControllerObjects.paginationDetails.maxPageNo) {
                    $scope.sensorControllerObjects.paginationDetails.assetTotalPageCount = range(totalPageNo);
                } else {
                    $scope.sensorControllerObjects.paginationDetails.assetTotalPageCount = range($scope.sensorControllerObjects.paginationDetails.maxPageNo);
                }
                getSensorsDetailsByPageNoAndType(1, $scope.sensorControllerObjects.assetConstants.ASSET_DETAILS.DEFAULT_ASSET_NAME);
            })
        } else {
            sensorService.getSensorCount().then(function (result) {
                var totalPageNo = parseInt(result.data.count) / $scope.sensorControllerObjects.paginationDetails.maxAssetsPerPage;
                var roundedTotalPageNo = Math.round(parseInt(result.data.count) / $scope.sensorControllerObjects.paginationDetails.maxAssetsPerPage);
                if (totalPageNo > roundedTotalPageNo) {
                    totalPageNo = roundedTotalPageNo + 1;
                } else if (totalPageNo < 1) {
                    totalPageNo = 1;
                } else if (totalPageNo < roundedTotalPageNo) {
                    totalPageNo = roundedTotalPageNo;
                }
                $scope.sensorControllerObjects.paginationDetails.totalNoOfAssets = totalPageNo;
                if (totalPageNo <= $scope.sensorControllerObjects.paginationDetails.maxPageNo) {
                    $scope.sensorControllerObjects.paginationDetails.assetTotalPageCount = range(totalPageNo);
                } else {
                    $scope.sensorControllerObjects.paginationDetails.assetTotalPageCount = range($scope.sensorControllerObjects.paginationDetails.maxPageNo);
                }
                getSensorsDetailsByPageNoAndType();
            })
        }
    }

    function   getSensorsDetailsByPageNoAndType() {
        $scope.pageLoader = true;
        var assetTypeSelectedForPagination = $scope.sensorControllerObjects.paginationDetails.assetTypeSelected;
        var maxAssetPerPage = $scope.sensorControllerObjects.paginationDetails.maxAssetsPerPage;
        var selectedPageNo = $scope.sensorControllerObjects.paginationDetails.assetPageSelected;
        var skip = ((selectedPageNo - 1) * maxAssetPerPage)
        var limit = maxAssetPerPage;
        assetApiService.getDevicesDetails().then(function (result) {
            if (result) {
                $scope.sensorData = result;
            }
        }, function error(errResponse) {
            console.log(errResponse);
        });
        if (assetTypeSelectedForPagination != 'all') {
            sensorService.getAssetDetailsByPageNoAndAssetType(skip, limit, assetTypeSelectedForPagination).then(function (result) {
                $scope.sensorDataForList = result.data;
                $scope.pageLoader = false;
            })
        } else {
            sensorService.getAssetDetailsByPageNo(skip, limit).then(function (result) {
                $scope.sensorDataForList = result.data;
                $scope.pageLoader = false;
            })
        }

    }

    function pushAddedAssetToListToDisplay(assetObj) {
        if ($scope.sensorDataForList.length >= $scope.sensorControllerObjects.paginationDetails.maxAssetsPerPage) {
            $scope.sensorDataForList.pop()
        }
        $scope.sensorDataForList.unshift(assetObj)
    };

    /* drop down */
    $scope.setTypeToGetAssetList = function (type) {
        $scope.sensorControllerObjects.paginationDetails.assetPageSelected = 1;
        $scope.sensorControllerObjects.paginationDetails.assetTypeSelected = type;
        getAssetCountByType();
    }
    $scope.decrementPageNo = function () {
        $scope.sensorControllerObjects.paginationDetails.assetPageSelected = $scope.sensorControllerObjects.paginationDetails.assetPageSelected - 1;
        getSensorsDetailsByPageNoAndType();
    }
    $scope.incrementPageNo = function () {
        $scope.sensorControllerObjects.paginationDetails.assetPageSelected = $scope.sensorControllerObjects.paginationDetails.assetPageSelected + 1;
        getSensorsDetailsByPageNoAndType();
    }
    $scope.getAssetNamesForDropdown = function (assetName) {
        var type = $scope.sensorControllerObjects.paginationDetails.assetTypeSelected;
        /*console.log($scope.type);*/
        if (type === "all") {
            sensorService.getAssetNameArrForAll(assetName).then(function (result) {
                console.log(result.data);
                $scope.sensorControllerObjects.searchData.assetByName = result.data;
            })
        } else {
            sensorService.getAssetNameArr(type, assetName).then(function (result) {
                console.log(result.data);
                $scope.sensorControllerObjects.searchData.assetByName = result.data;
            })
        }
    }


    // ................  map .......

    function getAllDeviceFromApi() {
        assetApiService.getMapPlotsFromApi().then(function (result) {
            if (result) {
                plotMarkersFromApi(result)
            }
        }, function error(errResponse) {
            console.log(errResponse);
        });
    }

    function plotMarkersFromApi(deviceDetailsArray) {
        for (var k = 0; k < deviceDetailsArray.length; k++) {
            if (deviceDetailsArray[k].lat != 0 && deviceDetailsArray[k].lng != 0) {
                leafLetMapService.addMarkers(deviceDetailsArray[k].lat, deviceDetailsArray[k].lng, deviceDetailsArray[k].id)
            }
        }
    };
    $rootScope.$on('deviceDetails', function (event, args) {
        $scope.sensorControllerObjects.markerSelected = args.devDetails;

    });
    function sensorDetailsInit() {
        getAssetCountByType();
        $scope.showForm = false;
        getSensorConfiguration($scope.sensorControllerObjects.assetConstants.ASSET_DETAILS.DEFAULT_ASSET_NAME);
        getAllSensorTypes();
        getAllDeviceFromApi();
        getSensorsDetails();
    }


    $scope.getSensorDetails = function(id,apiRefId){
        $state.go('necApp.sensorMain.sensorDetails',{'id':id,'apiRefId':apiRefId});
    }
})