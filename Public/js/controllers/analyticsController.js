/**
 * Created by dhanalakshmi on 15/9/16.
 */
taxiFleetControlPanel.controller("analyticsController", function ($scope,$rootScope,rfIdDetectionService,analyticsService) {
    /*$scope.analyticsController={
        showHistoryLoading:false,
        dateFromTimeForTaxi:moment.utc(new Date()).add(-1, 'days').format('YYYY/MM/DD'),
        dateToTimeForTaxi:moment.utc(new Date()).format('YYYY/MM/DD'),
        taxiEntryExitDetails:[]
    }*/



    $scope.downloadDateBasedOnRange=function () {
        console.log("***************************************************")
        console.log($scope.analyticsController.dateFromTimeForTaxi)
        console.log($scope.analyticsController.dateToTimeForTaxi)
        $scope.updateTaxiEntryExitTable()
        analyticsService.getHistoryBasedOnDateRange($scope.analyticsController.dateFromTimeForTaxi,$scope.analyticsController.dateToTimeForTaxi)

        
    }
    $scope.updateTaxiEntryExitTable=function () {
        $scope.analyticsController.showHistoryLoading=true
        analyticsService.getHistoryBasedOnDateRangeTabel($scope.analyticsController.dateFromTimeForTaxi,$scope.analyticsController.dateToTimeForTaxi).then(function (result) {

            var data = result.data;
            var taxiArray=[];
            if(data){
                for(var j=0;j<data.length;j++){

                    var obj={};
                    obj.carNo=data[j].carNo;
                    /*obj.direction=data[j].direction;*/
                    obj.ingress=data[j].ingress;
                    obj.outgress=data[j].outgress;
                    obj.dwellingTime=data[j].dwellingTime;
                    if(data && data[j].driverName){
                        obj.driverName=data[j].driverName;
                    }else{
                        obj.driverName='-';
                    }

                    taxiArray.push(obj);
                }
                $scope.analyticsController.taxiEntryExitDetails = taxiArray;
                $scope.analyticsController.showHistoryLoading=false

            }else{
                $scope.showFlag=true
                $scope.analyticsController.showHistoryLoading=false
                $scope.analyticsMessage="no data avaliable";
            }


        })
    }


    // function init(){
    //     $scope.updateTaxiEntryExitTable()
    // }

    // init();

})