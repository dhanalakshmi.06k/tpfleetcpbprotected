/**
 * Created by zendynamix on 7/8/2016.
 */
taxiFleetControlPanel.controller('settingsController', function ($scope,$http,$state,settingService,appService) {
$scope.mapSettingsDetails={
    taxiEntryExitStatus:settingService.getTaxiEntryExitSimulatorFlag()

}
    $scope.saveMapConfiguration=function(mapSettingsDetails,dateSettings){
        settingService.addMapConfigDetails(mapSettingsDetails).then(function(err,res){
            settingService.getMapConfig().then(function(err,result){
                addDateConfigToDb(dateSettings)
            })



        })
    }



    function addDateConfigToDb(dateSettings){
        settingService.addHistoryDateConfigDetails(dateSettings).then(function(err,res){

              settingService.getHistoryDateConfigDetails().then(function(response){
                  $scope.date={
                      fromDate:response.data[0].dateConfiguration.fromDate,
                      fromTime:response.data[0].dateConfiguration.fromTime,
                      toDate:response.data[0].dateConfiguration.toDate,
                      toTime:response.data[0].dateConfiguration.toTime
                  }
             })
        })
    }
    $scope.getMapConfigData=function(){
        settingService.getMapConfig().then(function(response){
            if(response && response.data){
                var mapDetailsObj =response.data[0];
                $scope.map={
                    centerLatitude:mapDetailsObj.SettingsConfiguration.mapSettings.mapCenter.latitude,
                    centerLongitude:mapDetailsObj.SettingsConfiguration.mapSettings.mapCenter.longitude
                }
            }
        })
    }

    $scope.getDateConfig=function(){

        settingService.getHistoryDateConfigDetails().then(function(response){
            $scope.date={
                fromDate:response.data[0].dateConfiguration.fromDate,
                fromTime:response.data[0].dateConfiguration.fromTime,
                toDate:response.data[0].dateConfiguration.toDate,
                toTime:response.data[0].dateConfiguration.toTime
            }
        })
    }

    $scope.setSimulatorForTaxiEntryExit = function(flag){
        console.log(flag)
        if(!flag){
            settingService.stopTaxiEntryExitSimulator();
            settingService.setTaxiEntryExitSimulatorFlag(false)
            $scope.mapSettingsDetails.taxiEntryExitStatus =  settingService.getTaxiEntryExitSimulatorFlag();
        }else{
            settingService.startTaxiEntryExitSimulator();
            settingService.setTaxiEntryExitSimulatorFlag(true)
            $scope.mapSettingsDetails.taxiEntryExitStatus =  settingService.getTaxiEntryExitSimulatorFlag();
        }
        
    }
    
    function init(){
        $scope.getMapConfigData();
        $scope.getDateConfig()
    }
    init();

})