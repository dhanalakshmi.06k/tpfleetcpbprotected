/**
 * Created by Suhas on 8/18/2016.
 */
taxiFleetControlPanel.controller('newLinkAssetsCtrl',
        function ($scope,constantService,sensorService,linkAssetsService,$q,$state,
                  assetLinkConfigService,$stateParams){
        /*$scope.newLinkAssetsDetails.rootAssetType*/
        $scope.newLinkAssetsDetails={
                assetsDetails:[
                        /*{label:'Car',type:"car",index:0,data:'',id:''},
                        {label:'Obd Device',type:"obdDevice",index:1,data:'',id:''},
                        {label:'RFID Tag',type:"rfIdTag",index:2,data:'',id:''},
                        {label:'Driver',type:"driver",index:3,data:'',id:''}*/
                ],
                popupClass:'nextAssetDetailsPopup',
                activeTab:0,
                formDetails:{
                        name:'newLinkAssetForm',
                        objects:'',
                        edit:false,
                        value:{}
                },
                direction:['previous','next'],
                rootAssetIndex:0,
                rootAssetType:'driver',
                selectionType:'btn',
                slideDirection:'next',
                selectedIndex:0,
                searchData:{
                        assetByName:""
                }
        };
        $scope.validateForm = function(direction){
                $scope.newLinkAssetsDetails.searchData.assetByName=[];
                var formName =$scope.newLinkAssetsDetails.formDetails.name;
                var index =$scope.newLinkAssetsDetails.activeTab;
                var directionToBeMoved = $scope.newLinkAssetsDetails.slideDirection;
                if(direction){
                        $scope.newLinkAssetsDetails.slideDirection = direction;
                        directionToBeMoved = direction;
                }else{
                        $scope[formName].$setPristine();
                }
                if(isFormPristine(formName)){
                        slideForm(directionToBeMoved,null);
                }
                else{
                        if(isFormValid(formName)){
                                checkForAssetData(index)
                                .then(function(isDataPresent){
                                        if(!isDataPresent){
                                                saveAssetData()
                                                .then(function(){
                                                        slideForm(directionToBeMoved,null);
                                                });
                                        }else{
                                                updateAssetDetails()
                                                .then(function(){
                                                        slideForm(directionToBeMoved,null);
                                                });
                                        }
                                })
                        }else{
                                $scope.openPopup($scope.newLinkAssetsDetails.popupClass);
                                $scope.newLinkAssetsDetails.selectionType = 'btn'
                        }
                }
        };
        $scope.validateTab = function(index){
                $scope.newLinkAssetsDetails.searchData.assetByName=[];
                var formName =$scope.newLinkAssetsDetails.formDetails.name;
                $scope.newLinkAssetsDetails.selectionType = 'tab';
                var activeTabIndex = $scope.newLinkAssetsDetails.activeTab;
                var indexToBeMoved = 0;
                if(index || index==0){
                        indexToBeMoved = index;
                        $scope.newLinkAssetsDetails.selectedIndex = index;
                }else{
                        indexToBeMoved = $scope.newLinkAssetsDetails.selectedIndex;
                        $scope[formName].$setPristine();
                }
                if(isFormPristine(formName)){
                        slideForm(null,indexToBeMoved)
                }
                else{
                        if(isFormValid(formName)){
                                checkForAssetData(activeTabIndex)
                                .then(function(isDataPresent){
                                        if(!isDataPresent){
                                                saveAssetData()
                                                .then(function(){
                                                        slideForm(null,indexToBeMoved);
                                                });
                                        }else{
                                                updateAssetDetails()
                                                .then(function(){
                                                                slideForm(null,indexToBeMoved);
                                                        });
                                        }
                                })
                        }else{
                                $scope.openPopup($scope.newLinkAssetsDetails.popupClass);
                                $scope.newLinkAssetsDetails.selectionType = 'tab'
                        }
                }
        };
        $scope.save = function(){
                var formName =$scope.newLinkAssetsDetails.formDetails.name;
                var activeTabIndex = $scope.newLinkAssetsDetails.activeTab;
                if(isFormPristine(formName)){
                        $state.go('necApp.linksMain.assetLinks.car');
                }
                else{
                        if(isFormValid(formName)){
                                checkForAssetData(activeTabIndex)
                                .then(function(isDataPresent){
                                        if(!isDataPresent){
                                                saveAssetData()
                                                .then(function(){
                                                        $state.go('necApp.linksMain.assetLinks');
                                                });
                                        }else{
                                                updateAssetDetails()
                                                .then(function(){
                                                        $state.go('necApp.linksMain.assetLinks');
                                                });
                                        }
                                })
                        }
                }
        };
        function saveAssetData(){
                return $q(function(resolve,reject){
                        var activeTabIndex = $scope.newLinkAssetsDetails.activeTab;
                        $scope.newLinkAssetsDetails.formDetails.value.sensorType=$scope.newLinkAssetsDetails.assetsDetails[activeTabIndex].type;
                        $scope.newLinkAssetsDetails.formDetails.value.sensorLabel=$scope.newLinkAssetsDetails.assetsDetails[activeTabIndex].label;
                        sensorService.getAssetNameForAssetsByTypeId($scope.newLinkAssetsDetails.formDetails.value.sensorType)
                        .then(function(assetUniqueDetails){
                                $scope.newLinkAssetsDetails.formDetails.value.assetName = $scope.newLinkAssetsDetails.formDetails.value[assetUniqueDetails.data]
                                sensorService.addSensors($scope.newLinkAssetsDetails.formDetails.value)
                                        .then(function(result){
                                                /*var index = $scope.newLinkAssetsDetails.activeTab;*/
                                                $scope.newLinkAssetsDetails.assetsDetails[activeTabIndex].data = result.data.sensorData;
                                                $scope.newLinkAssetsDetails.assetsDetails[activeTabIndex].id = result.data._id;
                                                linkAssets();
                                                resolve()
                                        })
                        })
                })
        }
        function updateAssetDetails(){
                return $q(function(resolve,reject){
                        var activeTabIndex = $scope.newLinkAssetsDetails.activeTab;
                        sensorService.getAssetNameForAssetsByTypeId($scope.newLinkAssetsDetails.formDetails.value.sensorType)
                        .then(function(assetUniqueDetails){
                                $scope.newLinkAssetsDetails.formDetails.value.assetName =
                                $scope.newLinkAssetsDetails.formDetails.value[assetUniqueDetails.data];
                                var assetData = {
                                        sensorData:$scope.newLinkAssetsDetails.formDetails.value,
                                        _id:$scope.newLinkAssetsDetails.assetsDetails[activeTabIndex].id
                                }
                                sensorService.getSensorDetailsById($scope.newLinkAssetsDetails.assetsDetails[activeTabIndex].id)
                                .then(function(assetsUpdatedDetails){
                                        var assetsOldData = assetsUpdatedDetails.data.sensorData;
                                        if(assetsOldData.assetsLinked){
                                                $scope.newLinkAssetsDetails.formDetails.value.assetsLinked = assetsOldData.assetsLinked;
                                        }
                                        if(assetsOldData.isLinked){
                                                $scope.newLinkAssetsDetails.formDetails.value.isLinked = true;
                                        }else{
                                                $scope.newLinkAssetsDetails.formDetails.value.isLinked = false;
                                        }
                                        sensorService.updateSensorDetails(assetData)
                                                .then(function(result){
                                                        $scope.newLinkAssetsDetails.assetsDetails[activeTabIndex].data = result.data.sensorData;
                                                        $scope.newLinkAssetsDetails.assetsDetails[activeTabIndex].id = result.data._id;
                                                        if(!$scope.newLinkAssetsDetails.assetsDetails[activeTabIndex].isLinked){
                                                                linkAssets();
                                                        }
                                                        resolve();
                                                })
                                })
                        })
                })
        }
        function linkAssets(){
                var activeTabIndex =$scope.newLinkAssetsDetails.activeTab;
                var rootAssetIndex =$scope.newLinkAssetsDetails.rootAssetIndex;
                if(rootAssetIndex!=activeTabIndex){
                        if($scope.newLinkAssetsDetails.assetsDetails
                        [rootAssetIndex].id!=''){
                                var data={
                                        assetOne:{
                                                type:$scope.newLinkAssetsDetails.assetsDetails[rootAssetIndex].type,
                                                id:$scope.newLinkAssetsDetails.assetsDetails[rootAssetIndex].id
                                        },
                                        assetTwo:{
                                                type:$scope.newLinkAssetsDetails.assetsDetails[activeTabIndex].type,
                                                id:$scope.newLinkAssetsDetails.assetsDetails[activeTabIndex].id
                                        }
                                }
                                linkAssetsService.linkAssetsNew(data)
                                .then(function(result){
                                        $scope.newLinkAssetsDetails.assetsDetails[activeTabIndex].isLinked = true;
                                        console.log("asset car linked with "+$scope.newLinkAssetsDetails.assetsDetails[activeTabIndex].type)
                                })
                        }
                }
        }
        function getAssetConfigByType(type){
                sensorService.getFormDetailsByAssetType(type).then(function(formData){
                    console.log("getFormDetailsByAssetType****************getAssetConfigByType");
                    console.log(formData)
                        $scope.newLinkAssetsDetails.formDetails.objects = formData;
                })
        };
        function checkForAssetData(index){
                return $q(function(resolve,reject){
                        if($scope.newLinkAssetsDetails.assetsDetails[index].data!='' ||
                        $scope.newLinkAssetsDetails.assetsDetails[index].id!=''){
                                resolve(true)
                        }
                        else{
                                resolve(false)
                        }
                })
        };
        function isFormValid(formName){
                if($scope[formName].$invalid){
                        return false;
                }else{
                        return true;
                }
        };
        function isFormPristine(formName){
                if($scope[formName].$pristine){
                        return true;
                }else{
                        return false;
                }
        };
        function slideForm(direction,index){
                if(direction){
                        if(direction==='next'){
                                $scope.newLinkAssetsDetails.activeTab = $scope.newLinkAssetsDetails.activeTab+1;
                        }else if(direction==='previous'){
                                $scope.newLinkAssetsDetails.activeTab = $scope.newLinkAssetsDetails.activeTab-1;
                        }
                }else{
                        $scope.newLinkAssetsDetails.activeTab=index;
                }
                checkForAssetData($scope.newLinkAssetsDetails.activeTab)
                .then(function(isDataPresent){
                        $scope.newLinkAssetsDetails.formDetails.edit=isDataPresent;
                        $scope.newLinkAssetsDetails.formDetails.value=$scope.newLinkAssetsDetails.assetsDetails[$scope.newLinkAssetsDetails.activeTab].data;
                        var type = $scope.newLinkAssetsDetails.assetsDetails[$scope.newLinkAssetsDetails.activeTab].type;
                        $scope[$scope.newLinkAssetsDetails.formDetails.name].$setPristine();
                        getAssetConfigByType(type)
                })
        };
        $scope.openPopup= function (popup) {
                $(".background").fadeIn(250, function () {
                        $("."+popup).fadeIn(250);
                });
        };
        $scope.closePopup= function (popup) {
                $("."+popup).fadeOut(250, function () {
                        $(".background").fadeOut(250);
                });
        };
        $scope.getExistingAsset = function(assetName){
                var type = $scope.newLinkAssetsDetails.assetsDetails[$scope.newLinkAssetsDetails.activeTab].type;
                if(assetName === ""){ console.log("Name Is Empty")}
                else{
                        sensorService.getAssetNameArr(type,assetName)
                        .then(function(result){
                                $scope.newLinkAssetsDetails.searchData.assetByName = result.data;

                        })
                }
        }

        function getAssetLinkConfig(type){
                assetLinkConfigService.getAssetLinkConfigBYType(type)
                .then(function(result){
                                        $scope.newLinkAssetsDetails.assetsDetails = result.data;
                                })
        }
        $scope.getExistingAssetData = function(id){
                sensorService.getSensorDetailsById(id)
                .then(function(result){
                        var assetId = result.data._id;
                        var activeTabIndex = $scope.newLinkAssetsDetails.activeTab;
                        /*$scope.newLinkAssetsDetails.assetsDetails[activeTabIndex].data = result.data;*/
                        $scope.newLinkAssetsDetails.assetsDetails[activeTabIndex].id = assetId;
                        var formName =$scope.newLinkAssetsDetails.formDetails.name;
                        $scope.newLinkAssetsDetails.formDetails.value = result.data.sensorData;
                        $scope[formName].$setDirty();
                })
        }
        if($stateParams.rootAssetType){
                $scope.newLinkAssetsDetails.rootAssetType = $stateParams.rootAssetType
                getAssetLinkConfig($stateParams.rootAssetType);
                getAssetConfigByType($stateParams.rootAssetType)
        }
})