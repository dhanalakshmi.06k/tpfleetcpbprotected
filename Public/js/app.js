/**
 * Created by MohammedSaleem on 11/11/15.
 */

var dependencies = ['ui.router','flashGraphUI','flashSliderUI','flashSlider2UI','jsonFormUI','circularLoaderUI','hrLoaderUI',
    'ngSanitize','ng-showdown','jsonToHtmlFormModule'];
var taxiFleetControlPanel = angular.module("taxiFleetControlPanel", dependencies);

taxiFleetControlPanel.run(function(appService,settingService,$rootScope,constantService){
    // $rootScope.mapApiServiceProvider = 'googleMapService';
    settingService.init().then(function (resData) {
        var defaultTheme = "goldPlatedTheme";

        var htmlBody = angular.element( document.querySelector( 'body'));

        var uiStyle = resData.data.SettingsConfiguration.uiStyleName;
        var uiTheme = resData.data.SettingsConfiguration.uiThemeName;

        if(uiStyle){
            htmlBody.addClass(uiStyle);
        }

        if(uiTheme){
            htmlBody.addClass(uiTheme);
        }else{
            htmlBody.addClass(defaultTheme);
        }
    });

    function getConstantDetails(){
        appService.getConstantDetails().then(function (resultDetails) {
            appService. setApplicationConstants(resultDetails.data)
        }, function error(errResponse) {
            console.log("cannot get assets types configuration")
        })
    }
    function getAssetConfigDetails(){
        constantService.getAssetsConfig().then(function (responseDetails) {
            constantService.setAssetConfig(responseDetails.data);
        }, function error(errResponse) {
            console.log("cannot get ApplicationConstants")
        })
    }
    function getGpsWoxAddDeviceConstant(){
        constantService.getGpsDeviceWoxConstantFromApi().then(function (responseDetails) {
            constantService.setGpsDeviceWoxConstant(responseDetails.data);
        }, function error(errResponse) {
            console.log("cannot get ApplicationConstants")
        })

    }
    function getZonePolgonPlots(){
        constantService.getPolygonZonePlots().then(function (result) {
            constantService.setPolygonZonePlotsFromConfig(result.data[0].polygonDetailsSchemaModelConfiguration)

        }, function error(errResponse) {
            console.log("cannot get ApplicationConstants")
        })

    }

    //get the assets types from constants configuration
    function initConfig(){
        getConstantDetails();
        getAssetConfigDetails();
        getGpsWoxAddDeviceConstant();
        getZonePolgonPlots()

    }
    initConfig();

})


taxiFleetControlPanel.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider.
        state('admin', {
            url: "/admin",
            templateUrl: 'credentials/admin.html'
        }).
        state('admin.signIn', {
            url: "/signIn",
            templateUrl: 'credentials/signIn.html',
            controller: 'loginCtrl'
        }).
        state('admin.signUp', {
            url: "/signUp",
            templateUrl: 'credentials/signUp.html',
        controller: 'loginCtrl'
        }).
        state('necApp', {
            url: "/necApp",
            templateUrl: 'templates/necApp.html',
            controller: 'mainController'
        }).
        state('necApp.overview', {
            url: "/overview",
            templateUrl: 'templates/overview.html',
            controller: 'overviewController'
        }).
        state('necApp.appsMain', {
            url: "/appsMain",
            templateUrl: 'templates/appsMain.html'
        }).
        state('necApp.appsMain.apps', {
            url: "/apps",
            templateUrl: 'templates/apps.html'
        }).
        state('necApp.appsMain.appDetails', {
            url: "/appDetails",
            templateUrl: 'templates/appDetails.html'
        }).
        state('necApp.appsMain.installedApps', {
            url: "/installedApps",
            templateUrl: 'templates/installedApps.html',
            controller:'appsController'
        }).
        state('necApp.appsMain.installedAppDetails', {
            url: "/installedAppDetails/:appId",
            templateUrl: 'templates/installedAppDetails.html',
            controller:'appsController'
        }).
        state('necApp.appsMain.queueAnalytics', {
            url: "/queueAnalytics/:applicationId",
            templateUrl: 'templates/installedApps/queueAnalytics.html',
            controller:'appsController'
        }).
        state('necApp.appsMain.newWatchList', {
            url: "/newWatchList",
            templateUrl: 'templates/newWatchList.html',
            controller: 'watchListDetailsController'
        }).
        state('necApp.appsMain.personDetails', {
            url: "/personDetails",
            templateUrl: 'templates/personDetails.html',
            controller: 'personDetailsController'
        }).
        state('necApp.appsMain.personDetailsEdit', {
            url: "/personDetailsEdit",
            templateUrl: 'templates/personDetailsEdit.html'
        }).
        state('necApp.appsMain.watchListDetails', {
            url: "/watchListDetails",
            templateUrl: 'templates/watchListDetails.html',
            controller: 'watchListDetailsController'
        }).
        state('necApp.appsMain.watchListEdit', {
            url: "/watchListEdit",
            templateUrl: 'templates/watchListEdit.html',
            controller: 'watchListDetailsController'
        }).
        state('necApp.sensorMain', {
            url: "/sensorMain",
            templateUrl: 'templates/sensorMain.html'
        }).
        state('necApp.sensorMain.sensors', {
            url: "/sensors",
            templateUrl: 'templates/sensors.html',
            controller: 'sensorController'
        }).
        state('necApp.sensorMain.sensorDetails', {
            url: "/sensorDetails/:id/:apiRefId",
            templateUrl: 'templates/sensorDetails.html',
            controller: 'sensorDetails'
        }).

        state('necApp.dataMain', {
            url: "/dataMain",
            templateUrl: 'templates/dataMain.html'
        }).

        state('necApp.instanceMain', {
            url: "/instanceMain",
            templateUrl: 'templates/instanceMain.html'
           /* controller: 'sensorController'*/
        }).
        state('necApp.instanceMain.instance', {
            url: "/instance",
            templateUrl: 'templates/instances.html',
           controller: 'instancesController'
        }).
        state('necApp.instanceMain.instanceDetails', {
            url: "/instanceDetails/:instanceId",
            templateUrl: 'templates/instanceDetails.html',
           controller: 'instancesController'
        }).

        state('necApp.serversMain', {
            url: "/serversMain",
            templateUrl: 'templates/serversMain.html',
            controller: 'analyticsController'
        }).
        state('necApp.serversMain.servers', {
            url: "/servers",
            templateUrl: 'templates/servers.html'
        }).

        state('necApp.dashboard', {
            url: "/dashboard",
            templateUrl: 'templates/dashboard.html'
        }).
        state('necApp.dashboard.appDashboard', {
            url: "/appDashboard",
            templateUrl: 'templates/appDashboard.html'
        }).
        state('necApp.dashboard.customApisDashboard', {
            url: "/customApisDashboard",
            templateUrl: 'templates/customApisDashboard.html'
        }).

        state('necApp.settings', {
            url: "/settings",
            templateUrl: 'templates/settings.html'
        }).
        state('necApp.settings.licensing', {
            url: "/licensing",
            templateUrl: 'templates/settings/licensing.html'
        }).
        state('necApp.settings.generalSettings', {
            url: "/generalSettings",
            controller:"userCtrl",
            templateUrl: 'templates/settings/generalSettings.html'
        }).

        state('necApp.settings.mapSettings', {
        url: "/mapSettings",
        templateUrl: 'templates/settings/mapSettings.html',
        controller:"settingsController"
         }).

        state('necApp.settings.helpSettings', {
            url: "/helpSettings",
            templateUrl: 'templates/settings/helpSettings.html',
            controller:'helpWidgetController'
        })
        .state('necApp.linksMain', {
            url: "/links",
            templateUrl: 'templates/linksMain.html'
        })
        .state('necApp.linksMain.assetLinks', {
            url: "/assetLinks/:iotListType",
            templateUrl: 'templates/assetLinks.html',
            controller:'linkAssetsCtrl'
        })
        .state('necApp.linksMain.newAsset', {
            url: "/newAsset/:rootAssetType",
            templateUrl: 'templates/newAsset.html',
            controller:'newLinkAssetsCtrl'
        })
        .state('necApp.linksMain.beacons', {
            url: "/beacons",
            templateUrl: 'templates/beacons.html',
          controller:'linkAssetsCtrl'
        })
        .state('necApp.driverLinks', {
            url: "/driverLinks",
            templateUrl: 'templates/driverIotMain.html'
        })
        .state('necApp.driverLinks.driverAssetLinks', {
            url: "/driverAssetLinks/:iotListType",
            templateUrl: 'templates/driverAssetLinks.html',
            controller:'linkAssetsCtrl'
        })
        .state('necApp.driverLinks.newDriverAsset', {
            url: "/newDriverAsset/:rootAssetType",
            templateUrl: 'templates/newDriverAsset.html',
            controller:'newLinkAssetsCtrl'
        })
        .state('necApp.linksMain.driverLinks', {
            url: "/newAsset/driver",
            templateUrl: 'templates/newAsset.html',
            controller:'newLinkAssetsCtrl'
        })
        .state('necApp.analyticsMain', {
        url: "/analyticsMain",
        templateUrl: 'templates/analyticsMain.html'
        /* controller: 'sensorController'*/
        })
        .state('necApp.analyticsMain.taxiInOut', {
        url: "/taxiInOut",
        templateUrl: 'templates/settings/taxiInOut.html'
         })

        .state('necApp.queryMain', {
            url: "/queryMain",
            templateUrl: 'templates/queryMain.html'
        }).
        state('necApp.queryMain.sensors', {
            url: "/report",
            templateUrl: 'templates/report.html'/*,
            controller: 'sensorController'*/
        })



    $urlRouterProvider.otherwise("/admin/signIn");
});
taxiFleetControlPanel.directive('repeatDone', function() {
    return function(scope, element, attrs) {
        if (scope.$last) { // all are rendered
            scope.$eval(attrs.repeatDone);
        }
    }
});

taxiFleetControlPanel.filter('secondsToDateTime', function() {
    return function(seconds) {
        if(!seconds){
            return "-";
        }
        else if(seconds>60){
            var d = new Date(0,0,0,0,0,0,0);
            d.setSeconds(seconds);
            var minutes = d.getMinutes();
            var seconds = d.getSeconds();
            return minutes+" min "+seconds+" sec";
        }else if(seconds==='-'){
            return '-'
        }
    };
});
/*
taxiFleetControlPanel.directive('exportTable', function () {
    var link = function ($scope, element, attr) {
        $scope.$on('export-pdf',function(e,d){
            element.exportDetails({ type:'pdf', escape: false });
        })

        $scope.$on('export-excel', function(e,d){
            element.exportDetails({ type:'excel', escape: false });
        });
        $scope.$on('export-doc',function(e,d){
            element. exportDetails({ type:'doc', escape: false});
        });
        $scope.$on('export-csv', function(e,d){
            element. exportDetails({ type:'csv', escape:false });
        });
    }
    return {
        restrict:'A',
        link:link
    };
});*/
