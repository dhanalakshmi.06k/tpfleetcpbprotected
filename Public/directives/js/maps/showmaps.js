/**
 * Created by zendynamix on 20/8/16.
 */


taxiFleetControlPanel.controller("mapsController", function($scope,$http,leafLetMapService,$rootScope,$attrs,settingService) {
  /*  var map;*/

    var serviceProvider=$rootScope.mapApiServiceProvider
   
    $scope.mapId = $attrs.mapId;
    console.log($scope.mapId)
    function defaultSetMap(){
        settingService.getMapConfig().then(function(response){
            if(response && response.data){
                var mapDetailsObj =response.data[0];
                googleMapService.initMap({lat:mapDetailsObj.SettingsConfiguration.mapSettings.mapCenter.latitude, lng: mapDetailsObj.SettingsConfiguration.mapSettings.mapCenter.longitude});

            }
        })

    }
    
   /* defaultSetMap();*/
    leafLetMapService.initMap(55.4127418,25.2779791,12);
})


taxiFleetControlPanel.directive("showMap", function() {
    return {
        restrict: 'E',
        templateUrl: '../../directives/templates/maps/map.html',
        controller:"mapsController",

        
    }
});

