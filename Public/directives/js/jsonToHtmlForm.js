var jsonToHtmlFormModule;

jsonToHtmlFormModule = angular.module('jsonToHtmlFormModule', []);

jsonToHtmlFormModule.directive('jsonToHtmlForm', function($log, $timeout,$q) {
  return {
    restrict: 'E',
    scope: {
      formData: '=',
      edit: "=",
      json: "=json",
      formName:"=",
      getTypes: "&"
    },
    link: function(scope, elem, attrs) {
      var data={}
      scope.formEdit = false;

      if(scope.json){
        data = /*JSON.parse(*/scope.json/*)*/;
      }
      if(attrs.formName){
        scope.formName = attrs.formName;
      }
      /**/
      if(attrs.formData){
        scope.formValue = attrs.formData;
      }
      if(attrs.formEdit){
        scope.formEdit = true;
      }else{
        scope.formEdit = false;
      }
      scope.$watch('json',function(newValue, oldValue){
        generateHtmlFromJson(newValue).then(function(data){
          scope.formDetails=data;
        })
      })
      generateHtmlFromJson(data).then(function(data){
        scope.formDetails=data;
      })
    },
    templateUrl: 'directives/templates/jsonToHtmlForm.html'
  };
  function generateHtmlFromJson(data) {
    return $q(function(resolve,reject){
      var label = [];
      var extractedData = [];
      var k = Object.keys(data);
      k.forEach(function (objKey, index) {
        if (data[k[index]].type) {
          label.push(k[index]);
          var obj = {
            "name": data[k[index]].label,
            "realName": objKey,
            "type": data[k[index]].type,
            "description": data[k[index]].description,
            "model": label.join('.'),
            required:data[k[index]].required,
            autoComplete:data[k[index]].autoComplete
          };
          if(data[k[index]].value){
            obj.dropDownDetails={}
            obj.dropDownDetails=data[k[index]].value;
          }
          if(data[k[index]].assetName){
            obj.assetName=data[k[index]].assetName;
          }
          (extractedData).push(obj);
          label.pop();
        }
        else {
          label.push(k[index]);
          generateHtmlFromJson(data[k[index]], label);
          label.pop();
        }
        if(k.length==extractedData.length){
          resolve(extractedData)
        }

      });
    })

  }
});

