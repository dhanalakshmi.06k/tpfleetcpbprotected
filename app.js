var express = require('express'),
  config = require('./config/config'),
  glob = require('glob'),
  mongoose = require('mongoose'),
  dataSeeder = require('data-seeder');

mongoose.connect(config.db);


var db = mongoose.connection;
/*var zmqConnector = require('zmqConnector');*/
var dataSeederPromise = dataSeeder.run({scriptDirPath:config.root+'/app/setUpScripts/',mongo:true});
dataSeederPromise.then(function(){
  db.on('error', function () {
    throw new Error('unable to connect to database at ' + config.db);
  });

  var models = glob.sync(config.root + '/app/models/*.js');
  models.forEach(function (model) {
    require(model);
  });
  var app = express();

  var server = require('http').Server(app);
  server.listen(config.port, function () {
    console.log('Express server listening on port ' + config.port);
  });
   /* https.stop (options, app).listen(config.port, function(){
        console.log("Express server listening on port " + config.port);
    });*/
  require('./config/express')(app,config);
  /*require('./config/zmqPushPull').zmqPushPull(config);*/
  require('./config/socketIo').init(server )
  var beaconDetection= require('./app/dataHandlers').beaconDetection
 /* require('zmqConnector').zmqConnector(__dirname+'/app/dataHandlers/',config.zmq);*/

    var zmq = require('zmq')
        , sock = zmq.socket('pull');
    sock.bindSync("tcp://0.0.0.0:5211");
    var beaconSimulator = 0;
    var socketConn = require('./config/socketIo');


    sock.on('message', function (msg) {
       /* console.log("incoming messages:::"+msg.toString());*/
       /* socketConn.getSocketIoServer().emit('beaconMessage', msg.toString());*/
        beaconDetection.saveData(msg.toString())
    });
}).catch(function(error){
  console.warn(error.stack)
});
